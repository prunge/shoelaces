open module au.net.causal.shoelaces.web
{
    exports au.net.causal.shoelaces.web;

    requires spring.core;
    requires spring.beans;
    requires spring.boot;
    requires spring.context;
    requires spring.web;
    requires spring.webmvc;
    requires spring.boot.autoconfigure;

    requires static org.jsoup;
    requires static jakarta.servlet;
}
