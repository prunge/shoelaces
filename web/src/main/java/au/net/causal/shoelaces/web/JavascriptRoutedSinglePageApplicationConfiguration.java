package au.net.causal.shoelaces.web;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportAware;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

/**
 * Configures Spring to handle a typical Javascript-routed single-page application where
 * typical page paths are forwarded to a base page (e.g. /index.html) and a {@literal <base>} tag is injected into
 * the base page's content or updated if it already exists with an href pointing to the webapp's base context path.
 *
 * @since 2.2
 */
@Configuration(proxyBeanMethods = false)
public class JavascriptRoutedSinglePageApplicationConfiguration implements ImportAware
{
    private String basePage = EnableJavascriptRoutedSinglePageApplication.DEFAULT_BASE_PAGE;

    @Override
    public void setImportMetadata(AnnotationMetadata importMetadata)
    {
        AnnotationAttributes attributes = AnnotationAttributes.fromMap(importMetadata.getAnnotationAttributes(EnableJavascriptRoutedSinglePageApplication.class.getName()));
        if (attributes != null)
            this.basePage = attributes.getString("basePage");
    }

    /**
     * @return the base page.  This is configured typically from the {@linkplain EnableJavascriptRoutedSinglePageApplication annotation} where this configuration is used from
     * but may be overridden by subclasses if more sophisticated behaviour is required.
     */
    protected String getBasePage()
    {
        return basePage;
    }

    /**
     * Configures resource forwarding so that paths that should be routed by Javascript (not ending with '.') are forwarded to the base page.
     */
    @Configuration
    public class WebConfiguration implements WebMvcConfigurer
    {
        @Override
        public void addViewControllers(ViewControllerRegistry registry)
        {
            registry.addViewController("/{spring:[\\w\\-]+}").setViewName("forward:" + getBasePage());
            registry.addViewController("/**/{spring:[\\w\\-]+}").setViewName("forward:" + getBasePage());
        }
    }

    /**
     * Configures a HTTP servlet filter that modified the base page content and introduces a {@literal <base>} element with a reference to the webapp's context path.
     */
    @Bean
    public FilterRegistrationBean<HtmlBaseElementFilter> indexFilter()
    {
        FilterRegistrationBean<HtmlBaseElementFilter> b = new FilterRegistrationBean<>(new HtmlBaseElementFilter());
        b.setUrlPatterns(List.of(getBasePage()));
        return b;
    }
}
