package au.net.causal.shoelaces.web;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.HtmlTreeBuilder;
import org.jsoup.parser.ParseSettings;
import org.jsoup.parser.Parser;
import org.jsoup.select.Elements;
import org.jsoup.select.Evaluator;
import org.jsoup.select.QueryParser;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.ContentCachingResponseWrapper;

import java.io.IOException;

/**
 * An HTTP filter that adds a {@literal <base>} element to HTML pages with the webapp's context path.
 *
 * <ul>
 *     <li>
 *         The {@literal <base>} element is added as the first element of the head, unless there is a {@literal <meta charset...>} tag in which case
 *         it is added after that.  If the head element does not exist it is added.
 *     </li>
 *     <li>
 *         If a {@literal <base>} element already exists it is not modified if {@code replaceExistingBaseTag} mode is off, otherwise its href is rewritten.
 *     </li>
 * </ul>
 *
 * The filter itself does not check whether the content is HTML and just assumes it is.  An appropriate match pattern should be set up when configuring it.
 *
 * @since 2.2
 */
public class HtmlBaseElementFilter extends OncePerRequestFilter
{
    private static final Evaluator selectMetaCharsetElement = QueryParser.parse("meta[charset]");

    private final boolean replaceExistingBaseTag;

    /**
     * Creates a base element filter that always replaces a {@literal <base>} tag if it exists.
     */
    public HtmlBaseElementFilter()
    {
        this(true);
    }

    /**
     * Creates a base element filter.
     *
     * @param replaceExistingBaseTag if true, any existing {@literal <base>} tag will be rewritten with a new calculated {@code href}.  If false, the filter will not change
     *                               pages that already have {@literal <base>} tags.
     */
    public HtmlBaseElementFilter(boolean replaceExistingBaseTag)
    {
        this.replaceExistingBaseTag = replaceExistingBaseTag;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
    throws ServletException, IOException
    {
        ContentCachingResponseWrapper responseWrapper = new ContentCachingResponseWrapper(response);

        filterChain.doFilter(request, responseWrapper);

        String htmlContent = new String(responseWrapper.getContentAsByteArray(), responseWrapper.getCharacterEncoding());

        Parser htmlParser = new Parser(new HtmlTreeBuilder()).settings(ParseSettings.preserveCase);
        Document htmlDoc = htmlParser.parseInput(htmlContent, "");
        htmlDoc.outputSettings().prettyPrint(false);

        Element head = htmlDoc.head();
        if (head == null)
            head = htmlDoc.prependElement("head");

        //Only define base if there is no existing definition or we're allowed to clobber the existing one
        Elements baseElements = head.getElementsByTag("base");
        if (!replaceExistingBaseTag && !baseElements.isEmpty())
        {
            responseWrapper.copyBodyToResponse();
            return;
        }

        Element baseElement = baseElements.first();
        if (baseElement == null)
        {
            baseElement = htmlDoc.createElement("base");

            //Try to put our changes after <meta charset=...> since that should go as early as possible
            Element metaCharsetElement = head.selectFirst(selectMetaCharsetElement);
            if (metaCharsetElement != null)
                metaCharsetElement.after(baseElement);
            else
                head.prependChild(baseElement);
        }

        baseElement.attr("href", request.getContextPath() + "/");

        //Can't really stream using html(Appendable) because need to calculate content length
        //...could use counting output stream but it's another dependency and don't want to write my own either
        //HTML should be pretty small and we've already loaded entire DOM into memory anyway
        byte[] replacementHtmlBytes = htmlDoc.html().getBytes(responseWrapper.getCharacterEncoding());
        response.setContentLength(replacementHtmlBytes.length);
        response.getOutputStream().write(replacementHtmlBytes);
        response.getOutputStream().flush();
        response.flushBuffer();
    }
}
