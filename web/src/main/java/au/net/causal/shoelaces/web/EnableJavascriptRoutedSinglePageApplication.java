package au.net.causal.shoelaces.web;

import org.springframework.context.annotation.Import;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * When activated, configures the webapp for a typical single-page application with routing handled by Javascript.
 *
 * <ul>
 *     <li>
 *         Single page app target paths (e.g. /my/path/ but not including resources with file extensions) will be forwarded back to the base page (typically /index.html)
 *     </li>
 *     <li>
 *         The base page will be filtered and modified to add/update an HTML {@literal <base>} element pointing to the webapp's context path.  This allows
 *         the webapp to be deployed as a WAR to any context path (or run standalone with a configured context path) and still allow the Javascript routing to work.
 *     </li>
 * </ul>
 *
 * @since 2.2
 */
@Inherited
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import(JavascriptRoutedSinglePageApplicationConfiguration.class)
public @interface EnableJavascriptRoutedSinglePageApplication
{
    /**
     * The default base page.
     */
    String DEFAULT_BASE_PAGE = "/index.html";

    /**
     * The base page of the application.  All single page app target paths will be forwarded to this one and its HTML content will be updated with
     * a {@literal <base>} tag containing the webapp's context path.
     */
    String basePage() default DEFAULT_BASE_PAGE;
}
