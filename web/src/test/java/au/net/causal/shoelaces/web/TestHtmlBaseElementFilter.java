package au.net.causal.shoelaces.web;

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockFilterChain;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.io.IOError;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;

import static org.assertj.core.api.Assertions.*;

class TestHtmlBaseElementFilter
{
    /**
     * Runs the HTML base element filter.
     *
     * @param htmlResource the classpath resource for the base HTML content that the filter is expected to modify.
     * @param context servlet context.
     * @param replaceExistingBaseTag configuration option for filter.
     *
     * @return the HTML response after filtering, parsed into an HTML document.
     *
     * @throws IOException if an error occurs.
     * @throws ServletException if an error occurs.
     */
    private Document runFilter(String htmlResource, ServletContext context, boolean replaceExistingBaseTag)
    throws IOException, ServletException
    {
        MockHttpServletRequest request = MockMvcRequestBuilders.get(URI.create("http://localhost:8080" + context.getContextPath() + "/index.html"))
                                                               .contextPath(context.getContextPath())
                                                               .buildRequest(context);
        MockHttpServletResponse response = new MockHttpServletResponse();

        HttpServlet servlet = new GetResourceServlet(htmlResource);

        MockFilterChain chain = new MockFilterChain(servlet, new HtmlBaseElementFilter(replaceExistingBaseTag));
        chain.doFilter(request, response);

        String htmlOutput = response.getContentAsString();
        return Jsoup.parse(htmlOutput);
    }

    /**
     * Tests with an index.html from a React app created with create-react-app.
     */
    @Nested
    class ReactAppIndex
    {
        /**
         * Root servlet context should create a base with href '/'.
         */
        @Test
        void rootContext()
        throws IOException, ServletException
        {
            MockServletContext context = new MockServletContext();
            Document result = runFilter("react-index.html", context, false);
            Element base = result.head().getElementsByTag("base").first();

            assertThat(base.attr("href")).isEqualTo("/");

            //<base> element should be after <meta charset=...>
            assertThat(base.elementSiblingIndex()).as("<base> position should be after <meta charset=...> position")
                                                  .isGreaterThan(result.head().selectFirst("meta[charset]").elementSiblingIndex());
        }

        /**
         * Non-root servlet context should use href with appropriate URL for the context.
         */
        @Test
        void nonRootContext()
        throws IOException, ServletException
        {
            MockServletContext context = new MockServletContext();
            context.setContextPath("/galah");
            Document result = runFilter("react-index.html", context, false);
            Element base = result.head().getElementsByTag("base").first();

            assertThat(base.attr("href")).isEqualTo("/galah/");

            //<base> element should be after <meta charset=...>
            assertThat(base.elementSiblingIndex()).as("<base> position should be after <meta charset=...> position")
                                                  .isGreaterThan(result.head().selectFirst("meta[charset]").elementSiblingIndex());
        }

        /**
         * React index does not have base tag, but test whether replacemode=true works still.
         */
        @Test
        void withReplace()
        throws IOException, ServletException
        {
            MockServletContext context = new MockServletContext();
            Document result = runFilter("react-index.html", context, true);
            Element base = result.head().getElementsByTag("base").first();

            assertThat(base.attr("href")).isEqualTo("/");

            //<base> element should be after <meta charset=...>
            assertThat(base.elementSiblingIndex()).as("<base> position should be after <meta charset=...> position")
                                                  .isGreaterThan(result.head().selectFirst("meta[charset]").elementSiblingIndex());
        }
    }

    /**
     * Tests with HTML without a head element.
     */
    @Nested
    class NoHeadHtml
    {
        @Test
        void rootContext()
        throws IOException, ServletException
        {
            MockServletContext context = new MockServletContext();
            Document result = runFilter("no-head-index.html", context, false);

            //Should have injected head and base elements
            Element base = result.head().getElementsByTag("base").first();
            assertThat(base.attr("href")).isEqualTo("/");

            //Verify body is left untouched
            assertThat(result.body().child(0).tagName()).isEqualTo("h1");
            assertThat(result.body().child(0).text()).isEqualTo("Hello");
            assertThat(result.body().child(1).tagName()).isEqualTo("p");
            assertThat(result.body().child(1).text()).isEqualTo("Good morning, what will be for eating?");
        }

        @Test
        void nonRootContext()
        throws IOException, ServletException
        {
            MockServletContext context = new MockServletContext();
            context.setContextPath("/galah");
            Document result = runFilter("no-head-index.html", context, false);

            //Should have injected head and base elements
            Element base = result.head().getElementsByTag("base").first();
            assertThat(base.attr("href")).isEqualTo("/galah/");

            //Verify body is left untouched
            assertThat(result.body().child(0).tagName()).isEqualTo("h1");
            assertThat(result.body().child(0).text()).isEqualTo("Hello");
            assertThat(result.body().child(1).tagName()).isEqualTo("p");
            assertThat(result.body().child(1).text()).isEqualTo("Good morning, what will be for eating?");
        }

        @Test
        void withReplace()
        throws IOException, ServletException
        {
            MockServletContext context = new MockServletContext();
            Document result = runFilter("no-head-index.html", context, true);

            //Should have injected head and base elements
            Element base = result.head().getElementsByTag("base").first();
            assertThat(base.attr("href")).isEqualTo("/");

            //Verify body is left untouched
            assertThat(result.body().child(0).tagName()).isEqualTo("h1");
            assertThat(result.body().child(0).text()).isEqualTo("Hello");
            assertThat(result.body().child(1).tagName()).isEqualTo("p");
            assertThat(result.body().child(1).text()).isEqualTo("Good morning, what will be for eating?");
        }

    }

    /**
     * HTML with a head element but no meta charset element.
     */
    @Nested
    class HeadWithoutCharsetHtml
    {
        @Test
        void withoutReplace()
        throws IOException, ServletException
        {
            MockServletContext context = new MockServletContext();
            Document result = runFilter("head-no-charset-index.html", context, false);

            //Injected base element, title should remain
            Element base = result.head().getElementsByTag("base").first();
            assertThat(base.attr("href")).isEqualTo("/");
            Element title = result.head().getElementsByTag("title").first();
            assertThat(title.text()).isEqualTo("My Page");

            //Verify body is left untouched
            assertThat(result.body().child(0).tagName()).isEqualTo("h1");
            assertThat(result.body().child(0).text()).isEqualTo("Hello");
            assertThat(result.body().child(1).tagName()).isEqualTo("p");
            assertThat(result.body().child(1).text()).isEqualTo("Good morning, what will be for eating?");
        }

        @Test
        void withReplace()
        throws IOException, ServletException
        {
            MockServletContext context = new MockServletContext();
            Document result = runFilter("head-no-charset-index.html", context, true);

            //Injected base element, title should remain
            Element base = result.head().getElementsByTag("base").first();
            assertThat(base.attr("href")).isEqualTo("/");
            Element title = result.head().getElementsByTag("title").first();
            assertThat(title.text()).isEqualTo("My Page");

            //Verify body is left untouched
            assertThat(result.body().child(0).tagName()).isEqualTo("h1");
            assertThat(result.body().child(0).text()).isEqualTo("Hello");
            assertThat(result.body().child(1).tagName()).isEqualTo("p");
            assertThat(result.body().child(1).text()).isEqualTo("Good morning, what will be for eating?");
        }
    }

    /**
     * HTML with existing base element.
     */
    @Nested
    class ExistingBaseElement
    {
        @Test
        void noReplaceMode()
        throws IOException, ServletException
        {
            MockServletContext context = new MockServletContext();
            Document result = runFilter("existing-base-index.html", context, false);

            //Page should be left alone
            Element base = result.head().getElementsByTag("base").first();
            assertThat(base.attr("href")).isEqualTo("/leave/this/alone/");
        }

        @Test
        void replaceMode()
        throws IOException, ServletException
        {
            MockServletContext context = new MockServletContext();
            Document result = runFilter("existing-base-index.html", context, true);

            //Page should be left alone
            Element base = result.head().getElementsByTag("base").first();
            assertThat(base.attr("href")).isEqualTo("/");
        }
    }

    /**
     * Servlet that simply serves up a classpath resource.
     */
    private static class GetResourceServlet extends HttpServlet
    {
        private final String resourceName;

        /**
         * Creates a servlet.
         *
         * @param resourceName the name of the classpath resource to serve in the response, relative to the test class.
         */
        public GetResourceServlet(String resourceName)
        {
            this.resourceName = resourceName;
        }

        @Override
        protected void doGet(HttpServletRequest req, HttpServletResponse resp)
        throws IOException
        {
            resp.getOutputStream().write(readResource(resourceName));
        }

        private static byte[] readResource(String name)
        {
            URL resourceUrl = TestHtmlBaseElementFilter.class.getResource(name);
            if (resourceUrl == null)
                throw new Error("Missing test resource: " + name);

            try (InputStream is = resourceUrl.openStream())
            {
                return is.readAllBytes();
            }
            catch (IOException e)
            {
                throw new IOError(e);
            }
        }
    }
}
