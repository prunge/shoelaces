# Shoelaces Web module

A module containing additional utilities for Spring Boot web applications

### Single-page applications

The `@EnableJavascriptRoutedSinglePageApplication` annotation can be added to the Spring Boot
application class to make all requests not handled by other controllers/web handlers go through
a default one registered to a known main page such as `index.html`.  It will also set up

This is useful for single-page applications such as React applications where the request URL may
be different URLs but which need to be routed through to the same controller to serve up some
static Javascript.  By placing React-generated javascript files in the `/static` directory
of your JAR/WAR, everything will be set up to run a React application with a Spring backend.

In addition, served HTML pages from this catch-all controller are filtered and an 
HTML `<base>` element is added pointing to the webapp's context path.  This allows
the webapp to be deployed as a WAR to any context path 
(or run standalone with a configured context path) and still allow the Javascript routing to work.

### Usage

#### Single-page applications

Add the `@EnableJavascriptRoutedSinglePageApplication` annotation to Spring Boot application
classes.

e.g.

```
@SpringBootApplication
@EnableJavascriptRoutedSinglePageApplication
public class MyApplication extends SpringBootServletInitializer
{
    public static void main(String[] args)
    {
        SpringApplication.run(MyApplication.class, args);
    }
}
```
