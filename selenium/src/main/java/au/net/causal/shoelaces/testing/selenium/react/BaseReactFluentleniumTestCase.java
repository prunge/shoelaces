package au.net.causal.shoelaces.testing.selenium.react;

import au.net.causal.shoelaces.testing.selenium.BaseFluentleniumTestCase;
import au.net.causal.shoelaces.testing.selenium.Browser;
import io.fluentlenium.configuration.ConfigurationProperties;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

/**
 * Fixes issues with HtmlUnit running React apps in dev mode, and adds support for running the frontend on either port 8080 or 3000 (default for webpack devserver) by default.
 * Adds additional script to remove window.event to allow HTMLUnit to be able to run react apps in
 * dev mode.  Extend this test case base class if you are developing a React app and need to run them with HTMLUnit.
 * <p>
 *
 * Fixes "Cannot set property [Window].event that has only a getter to value 'null'." error:
 * see <a href="https://github.com/facebook/react/issues/13688">https://github.com/facebook/react/issues/13688</a>
 *
 * @since Shoelaces 2.2
 */
public class BaseReactFluentleniumTestCase extends BaseFluentleniumTestCase
{
    @Override
    protected int[] webServerPotentialPorts()
    {
        //Overridden to support frontend running on either port 3000 for development and 8080 for full integration testing
        return new int[] {3000, 8080};
    }

    @Override
    protected Capabilities customizeCapabilities(String driverName, Capabilities capabilities)
    {
        if ("htmlunit".equals(driverName))
        {
            //HTMLUnit seems to work best in Firefox mode when working with React apps
            DesiredCapabilities c = new DesiredCapabilities(Browser.HTMLUNIT.getBrowserName(), null, null);
            c.setVersion("firefox");
            if (capabilities == null)
                capabilities = c;
            else
                capabilities = capabilities.merge(c);

        }

        return super.customizeCapabilities(driverName, capabilities);
    }

    @Override
    protected WebDriver newRawWebDriver(String webDriverString, Capabilities capabilities, ConfigurationProperties configuration)
    {
        if ("htmlunit".equals(webDriverString))
            return new ReactFixedHtmlUnitDriver(capabilities);

        return super.newRawWebDriver(webDriverString, capabilities, configuration);
    }
}
