package au.net.causal.shoelaces.testing.selenium;

import org.openqa.selenium.os.CommandLine;
import org.openqa.selenium.remote.CommandExecutor;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.service.DriverCommandExecutor;
import org.openqa.selenium.remote.service.DriverService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Cleans up any Selenium webdriver process that have failed to be killed due to the main process running as a shim.
 * <p>
 *
 * This problem can happen on Windows with Chocolatey webdriver shims in certain situations.  Selenium kills off the shim
 * but the spawned driver processes remain.  This cleaner uses reflection hacks to gain access to the main process of
 * a remote webdriver, then scans for any processes whose parent is that process.  When the time comes to clean up
 * the driver, any processes whose parent is owned by the remote webdriver process are also killed.
 */
public class WebDriverShimProcessCleaner implements WebDriverCleaner
{
    private static final Logger log = LoggerFactory.getLogger(WebDriverShimProcessCleaner.class);

    private final Collection<? extends ProcessHandle> childProcesses;
    
    public WebDriverShimProcessCleaner(RemoteWebDriver webDriver)
    {
        this.childProcesses = driverProcess(webDriver).map(WebDriverShimProcessCleaner::readChildrenOfDriverProcess)
                                                      .orElse(List.of());
    }
    
    private static Collection<? extends ProcessHandle> readChildrenOfDriverProcess(Process driverProcess)
    {
        //The actual driver will be a direct child of the shim process
        return driverProcess.children().collect(Collectors.toUnmodifiableList());
    }
    
    public Collection<? extends ProcessHandle> getChildProcesses()
    {
        return childProcesses;
    }

    @Override
    public void cleanUpDriver()
    {
        if (log.isDebugEnabled())
        {
            log.debug("Need to clean up "  + childProcesses.stream().filter(ProcessHandle::isAlive).count() +
                    " WebDriver child processes...");
        }
        for (ProcessHandle childProcess : childProcesses)
        {
            if (childProcess.isAlive())
            {
                log.info("Killing remaining WebDriver process " + childProcess.pid() + ":" + childProcess.info());
                boolean killed = childProcess.destroyForcibly();
                if (!killed)
                    log.warn("Failed to kill WebDriver process " + childProcess.pid());
            }
        }
    }

    private static Optional<Process> driverProcess(RemoteWebDriver driver)
    {
        CommandExecutor executor = driver.getCommandExecutor();
        if (!(executor instanceof DriverCommandExecutor))
            return Optional.empty();

        DriverCommandExecutor dExecutor = (DriverCommandExecutor)executor;

        //Reflectively read process
        DriverService service = readServiceFromExecutor(dExecutor);
        CommandLine process = readProcessFromService(service);
        Object /*OsProcess*/ osProcess = readOsProcessFromCommandLine(process);
        Object /*OsProcess.SeleniumWatchDog*/ watchdog = readExecuteWatchDog(osProcess);
        Process javaProcess = readProcessFromWatchDog(watchdog);

        return Optional.of(javaProcess);
    }
    
    private static Process readProcessFromWatchDog(Object /*OsProcess.SeleniumWatchDog*/ watchdog)
    {
        return readFieldValueReflectively("org.openqa.selenium.os.OsProcess$SeleniumWatchDog", watchdog, Process.class, "process");
    }

    private static Object /*OsProcess.SeleniumWatchDog*/ readExecuteWatchDog(Object /*OsProcess*/ osProcess)
    {
        return readFieldValueReflectively("org.openqa.selenium.os.OsProcess", osProcess, Object.class, "executeWatchdog");
    }

    private static Object /*OsProcess*/ readOsProcessFromCommandLine(CommandLine commandLine)
    {
        return readFieldValueReflectively(CommandLine.class, commandLine, Object.class /*OsProcess*/, "process");
    }

    private static CommandLine readProcessFromService(DriverService service)
    {
        return readFieldValueReflectively(DriverService.class, service, CommandLine.class, "process");
    }

    private static DriverService readServiceFromExecutor(DriverCommandExecutor executor)
    {
        return readFieldValueReflectively(DriverCommandExecutor.class, executor, DriverService.class, "service");
    }

    private static <F, C> F readFieldValueReflectively(String fromTypeClassName, Object from, Class<F> fieldType, String fieldName)
    {
        try
        {
            //This cast is fine since C is introduced and we're checking with cast
            @SuppressWarnings("unchecked") Class<C> fromType = (Class<C>)Class.forName(fromTypeClassName);
            return readFieldValueReflectively(fromType, fromType.cast(from), fieldType, fieldName);
        }
        catch (ClassNotFoundException e)
        {
            NoClassDefFoundError err = new NoClassDefFoundError(e.getMessage());
            err.initCause(e);
            throw err;
        }
    }

    private static <C, F> F readFieldValueReflectively(Class<C> fromType, C from, Class<F> fieldType, String fieldName)
    {
        try
        {
            Field field = fromType.getDeclaredField(fieldName);
            field.setAccessible(true);
            Object fieldValue = field.get(from);
            return fieldType.cast(fieldValue);
        }
        catch (NoSuchFieldException e)
        {
            NoSuchFieldError err = new NoSuchFieldError(e.getMessage());
            err.initCause(e);
            throw err;
        }
        catch (IllegalAccessException e)
        {
            IllegalAccessError err = new IllegalAccessError(e.getMessage());
            err.initCause(e);
            throw err;
        }
    }
}
