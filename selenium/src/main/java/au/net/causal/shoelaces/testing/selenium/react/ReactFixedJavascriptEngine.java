package au.net.causal.shoelaces.testing.selenium.react;

import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.ScriptException;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebWindow;
import com.gargoylesoftware.htmlunit.javascript.JavaScriptEngine;
import com.gargoylesoftware.htmlunit.javascript.host.Window;

import java.lang.reflect.Field;

/**
 * Fixes issues with HtmlUnit running React apps in dev mode.
 * <p>
 *
 * Fixes an issue with History objects.
 *
 * @since Shoelaces 2.2
 *
 * @see History
 */
public class ReactFixedJavascriptEngine extends JavaScriptEngine
{
    public ReactFixedJavascriptEngine(WebClient webClient)
    {
        super(webClient);
    }

    /**
     * Applies the workaround/fix for the History bug in HTMLUnit.  Subclasses may override this method to supress this fix.
     *
     * @param webWindow the web window to initialize for.
     *
     * @see History
     */
    protected void applyHistoryFix(WebWindow webWindow)
    {
        Window jsWindow = webWindow.getScriptableObject();

        try
        {
            Field historyField = Window.class.getDeclaredField("history_");
            historyField.setAccessible(true);

            History history = new History();
            history.setParentScope(jsWindow);
            history.setPrototype(jsWindow.getPrototype(com.gargoylesoftware.htmlunit.javascript.host.History.class));

            historyField.set(jsWindow, history);
        }
        catch (ReflectiveOperationException e)
        {
            throw new ScriptException(null, e);
        }
    }

    @Override
    public void initialize(WebWindow webWindow, Page page)
    {
        super.initialize(webWindow, page);
        applyHistoryFix(webWindow);
    }
}
