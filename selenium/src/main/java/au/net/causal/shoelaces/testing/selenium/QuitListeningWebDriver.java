package au.net.causal.shoelaces.testing.selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Wraps an existing webdriver, notifying registered listeners when the webdriver quits.
 */
public class QuitListeningWebDriver extends EventFiringWebDriver
{
    private final List<WebDriverQuitListener> quitListeners = new CopyOnWriteArrayList<>();
    
    public QuitListeningWebDriver(WebDriver driver)
    {
        super(driver);
    }

    /**
     * Registers a listener that is notified when the webdriver quits.
     *
     * @param quitListener listener to register.
     *
     * @return this driver.
     *
     * @see #unregister(WebDriverQuitListener)
     */
    public QuitListeningWebDriver register(WebDriverQuitListener quitListener)
    {
        quitListeners.add(quitListener);
        return this;
    }

    /**
     * Removes a previously registered listener.  Does nothing if <code>quitListener</code> is not
     * registered with this driver.
     *
     * @param quitListener the listener to remove.
     *
     * @return this driver.
     *
     * @see #register(WebDriverQuitListener)
     */
    public QuitListeningWebDriver unregister(WebDriverQuitListener quitListener)
    {
        quitListeners.remove(quitListener);
        return this;
    }

    @Override
    public void quit()
    {
        for (WebDriverQuitListener quitListener : quitListeners)
        {
            quitListener.beforeQuit(getWrappedDriver());
        }
        try
        {
            super.quit();
        }
        catch (Throwable e)
        {
            try
            {
                for (WebDriverQuitListener quitListener : quitListeners)
                {
                    quitListener.afterQuitError(e);
                }
            }
            catch (Throwable listenerError)
            {
                e.addSuppressed(listenerError);
            }
            throw e;
        }

        for (WebDriverQuitListener quitListener : quitListeners)
        {
            quitListener.afterQuit();
        }
    }
}
