package au.net.causal.shoelaces.testing.selenium;

import org.openqa.selenium.WebDriver;

/**
 * Listener that is notified before/after WebDriver quits.
 *
 * @see WebDriver#quit()
 */
public interface WebDriverQuitListener
{
    /**
     * Called before the driver actually quits.
     */
    public void beforeQuit(WebDriver driver);

    /**
     * Called after the driver successfully quits without error.
     */
    public void afterQuit();

    /**
     * Called if an error occurs when the driver quits.
     *
     * @param error the error that occurred.
     */
    public void afterQuitError(Throwable error);
}
