package au.net.causal.shoelaces.testing.selenium.react;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.javascript.AbstractJavaScriptEngine;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

/**
 * Fixes issues with HtmlUnit running React apps in dev mode.
 * <p>
 *
 * Fixes "Cannot set property [Window].event that has only a getter to value 'null'." error:
 * see <a href="https://github.com/facebook/react/issues/13688">https://github.com/facebook/react/issues/13688</a>
 * <p>
 *
 * Also fixes issues with History objects.
 *
 * @since Shoelaces 2.2
 *
 * @see History
 */
public class ReactFixedHtmlUnitDriver extends HtmlUnitDriver
{
    public ReactFixedHtmlUnitDriver(Capabilities capabilities)
    {
        super(capabilities);
    }

    @Override
    protected WebClient modifyWebClient(WebClient webClient)
    {
        webClient.setScriptPreProcessor(new ReactFixedScriptPreProcessor());
        AbstractJavaScriptEngine<?> oldEngine = webClient.getJavaScriptEngine();
        webClient.setJavaScriptEngine(new ReactFixedJavascriptEngine(webClient));
        oldEngine.shutdown();
        return super.modifyWebClient(webClient);
    }
}
