package au.net.causal.shoelaces.testing.selenium.react;

import com.gargoylesoftware.htmlunit.javascript.configuration.JsxClass;
import com.gargoylesoftware.htmlunit.javascript.configuration.JsxConstructor;
import com.gargoylesoftware.htmlunit.javascript.configuration.JsxFunction;
import com.gargoylesoftware.htmlunit.javascript.configuration.JsxGetter;
import com.gargoylesoftware.htmlunit.javascript.configuration.JsxSetter;

/**
 * A patched version of the original History object that fixes the bug in replaceState/pushState where
 * the third argument, when omitted in JS code, is passed in as the string "undefined" instead of an actual
 * undefined value.  We work around this by translating this special string back to proper null values.
 * It's a bit of a hack but works well enough for testing given that getting a literal string "undefined" in real life
 * scenarios would be very rare.
 *
 * @since Shoelaces 2.2
 */
@JsxClass
public class History extends com.gargoylesoftware.htmlunit.javascript.host.History
{
    @JsxConstructor
    public History()
    {
    }

    @JsxGetter
    @Override
    public int getLength()
    {
        return super.getLength();
    }

    @JsxGetter
    @Override
    public Object getState()
    {
        return super.getState();
    }

    @JsxFunction
    @Override
    public void back()
    {
        super.back();
    }

    @JsxFunction
    @Override
    public void forward()
    {
        super.forward();
    }

    @JsxFunction
    @Override
    public void go(final int relativeIndex)
    {
        super.go(relativeIndex);
    }

    @JsxFunction
    @Override
    public void replaceState(final Object object, final String title, Object url)
    {
        if ("undefined".equals(url))
            url = null;

        super.replaceState(object, title, url);
    }

    @JsxFunction
    @Override
    public void pushState(final Object object, final String title, Object url)
    {
        if ("undefined".equals(url))
            url = null;

        super.pushState(object, title, url);
    }

    @JsxGetter
    @Override
    public String getScrollRestoration()
    {
        return super.getScrollRestoration();
    }

    @JsxSetter
    @Override
    public void setScrollRestoration(final String scrollRestoration)
    {
        super.setScrollRestoration(scrollRestoration);
    }
}
