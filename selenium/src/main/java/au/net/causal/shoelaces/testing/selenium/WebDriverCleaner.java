package au.net.causal.shoelaces.testing.selenium;

/**
 * Performs additional cleanup when a Selenium webdriver has finished being used.
 */
public interface WebDriverCleaner 
{
    /**
     * Performs Selenium webdriver cleanup.
     */
    public void cleanUpDriver();
}
