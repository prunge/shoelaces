package au.net.causal.shoelaces.testing.selenium;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Identifies a browser based on its name.
 * <p>
 *
 * Used internally as a substitute for Selenium's remote browser class since this is only in Selenium 4 and Shoelaces maintains an ability to run under both
 * Selenium 3.x and 4.x.
 *
 * @see org.openqa.selenium.remote.Browser
 *
 * @since Shoelaces 3.0
 */
public enum Browser
{
    HTMLUNIT("htmlunit"),
    FIREFOX("firefox"),
    CHROME("chrome"),
    EDGE("MicrosoftEdge", "msedge"),
    IE("internet explorer");

    private final String browserName;
    private final Set<String> allNames;

    private Browser(String browserName, String... otherNames)
    {
        this.browserName = browserName;
        Set<String> allNames = new LinkedHashSet<>(otherNames.length + 1);
        allNames.add(browserName);
        allNames.addAll(List.of(otherNames));
        this.allNames = Set.copyOf(allNames);
    }

    /**
     * @return the main browser name.
     */
    public String getBrowserName()
    {
        return browserName;
    }

    /**
     * @return all names this browser uses.
     */
    public Set<String> getAllNames()
    {
        return allNames;
    }

    /**
     * @return true if the specified browser name matches any of this browser's known names.
     */
    public boolean is(String browserName)
    {
        return allNames.contains(browserName);
    }
}
