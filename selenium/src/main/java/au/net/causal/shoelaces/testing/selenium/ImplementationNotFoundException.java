package au.net.causal.shoelaces.testing.selenium;

/**
 * Occurs when a specific webdriver implementation is not found.
 */
public class ImplementationNotFoundException extends RuntimeException
{
    public ImplementationNotFoundException()
    {
    }

    public ImplementationNotFoundException(String message)
    {
        super(message);
    }

    public ImplementationNotFoundException(Throwable cause)
    {
        super(cause);
    }

    public ImplementationNotFoundException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
