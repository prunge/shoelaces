package au.net.causal.shoelaces.testing.selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WrapsDriver;

/**
 * Utilities for working with web drivers.
 */
public final class WebDriverTools
{
    /**
     * Private constructor to prevent instantiation.
     */
    private WebDriverTools()
    {
    }

    /**
     * Unwraps a webdriver, returning an object that implements the given interface or extends the given class.
     * <p>
     *
     * If <code>driver</code> is an instance of <code>type</code> then it is simply returned appropriately cast.
     * If it is a {@linkplain WrapsDriver wrapper / proxy}, it is unwrapped (possibly repeatedly for multiple layers)
     * until an implementation of <code>type</code> is found, or if not, an exception is thrown.
     *
     * @param driver the driver to cast / unwrap.
     * @param type the target type to find.
     * @param <D> the target type to find.
     *
     * @return an implementation of <code>type</code>, never null.
     *
     * @throws ImplementationNotFoundException if an implementation of <code>type</code> could not be found in any
     *                                         layer.
     */
    public static <D> D unwrapDriver(WebDriver driver, Class<D> type)
    {
        WebDriver curDriver = driver;
        while (!type.isInstance(curDriver))
        {
            if (curDriver instanceof WrapsDriver)
                curDriver = ((WrapsDriver)curDriver).getWrappedDriver();
            else
                throw new ImplementationNotFoundException("Driver " + driver + " is not a " + type.getName());
        }

        return type.cast(curDriver);
    }
}
