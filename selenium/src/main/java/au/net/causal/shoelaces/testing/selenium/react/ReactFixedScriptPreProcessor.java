package au.net.causal.shoelaces.testing.selenium.react;

import com.gargoylesoftware.htmlunit.ScriptPreProcessor;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

/**
 * Fixes issues with HtmlUnit running React apps in dev mode.  Adds additional script to remove window.event to allow HTMLUnit to be able to run react apps in
 * dev mode.
 * <p>
 *
 * Fixes "Cannot set property [Window].event that has only a getter to value 'null'." error:
 * see <a href="https://github.com/facebook/react/issues/13688">https://github.com/facebook/react/issues/13688</a>
 *
 * @since Shoelaces 2.2
 */
public class ReactFixedScriptPreProcessor implements ScriptPreProcessor
{
    @Override
    public String preProcess(HtmlPage htmlPage, String sourceCode, String sourceName, int lineNumber, HtmlElement htmlElement)
    {
        //window.event is causing all sorts of problems with development react scripts
        //see https://github.com/facebook/react/issues/13688

        sourceCode = "delete window.event;\n" + sourceCode;
        return sourceCode;
    }
}
