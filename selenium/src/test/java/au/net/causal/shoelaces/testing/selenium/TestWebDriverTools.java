package au.net.causal.shoelaces.testing.selenium;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WrapsDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

@MockitoSettings(strictness = Strictness.LENIENT)
class TestWebDriverTools
{
    private static final WebDriver simpleDriver = new HtmlUnitDriver();

    @Nested
    class UnwrapDriver
    {
        @Mock
        private MyWrappingDriver wrappedDriver;

        /**
         * Wraps {@link #wrappedDriver}, giving us two layers of wrapping.
         */
        @Mock
        private MyWrappingDriver multiWrappedDriver;

        @BeforeEach
        void setUpMocks()
        {
            when(wrappedDriver.getWrappedDriver()).thenReturn(simpleDriver);
            when(multiWrappedDriver.getWrappedDriver()).thenReturn(wrappedDriver);
        }

        /**
         * Driver already implements the interface, so it's just a simple cast.
         */
        @Test
        void noWrapperSimpleCast()
        {
            JavascriptExecutor js = WebDriverTools.unwrapDriver(simpleDriver, JavascriptExecutor.class);
            assertThat(js).isSameAs(simpleDriver);
        }

        /**
         * Driver does not implement the interface.
         */
        @Test
        void noWrapperNoImplementation()
        {
            Throwable thrown = catchThrowable(() -> WebDriverTools.unwrapDriver(simpleDriver, NothingImplementsThis.class));
            assertThat(thrown).isInstanceOf(ImplementationNotFoundException.class);
        }

        @Test
        void singleWrapperFindsImplementation()
        {
            JavascriptExecutor js = WebDriverTools.unwrapDriver(wrappedDriver, JavascriptExecutor.class);
            assertThat(js).isSameAs(simpleDriver);
        }

        @Test
        void singleWrapperNoImplementation()
        {
            Throwable thrown = catchThrowable(() -> WebDriverTools.unwrapDriver(wrappedDriver, NothingImplementsThis.class));
            assertThat(thrown).isInstanceOf(ImplementationNotFoundException.class);
        }

        @Test
        void multipleLayerWrapperFindsImplementation()
        {
            JavascriptExecutor js = WebDriverTools.unwrapDriver(multiWrappedDriver, JavascriptExecutor.class);
            assertThat(js).isSameAs(simpleDriver);
        }

        @Test
        void multipleLayerWrapperNoImplementation()
        {
            Throwable thrown = catchThrowable(() -> WebDriverTools.unwrapDriver(multiWrappedDriver, NothingImplementsThis.class));
            assertThat(thrown).isInstanceOf(ImplementationNotFoundException.class);
        }
    }

    private static interface NothingImplementsThis
    {
    }

    private static interface MyWrappingDriver extends WebDriver, WrapsDriver
    {
    }

}
