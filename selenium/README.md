# Shoelaces Selenium module

A testing library that makes it easier to test your projects 
with Selenium.  It uses [Fluentlenium](https://fluentlenium.com), 
so you get all the goodies from that plus additional features to make 
testing and test setup easier.

Using this module should make it much easier to run tests.  It is 
designed so that if you write your Selenium tests using this module, 
it is easy to run your tests with a browser you have 
installed, and if you have multiple browsers it is easy to select
which browser you want to run your Selenium tests with. 

When using the Shoelaces Selenium module to write your tests:

- Use JUnit 5
- Use [Fluentlenium](https://fluentlenium.com), 
which extends Selenium and makes it a bit easier to write Selenium tests

### Features

#### Automatically chosen WebDriver implementation

Shoelaces will automatically select the WebDriver implementation 
(which controls which browser is being tested) based on:

- [Fluentlenium system properties](https://fluentlenium.com/docs/configuration/#configuration-ways): 
  if set, these can be used to 
choose which browser to run.
- Whatever drivers are installed on the local machine.  So if you have 
Firefox and its driver installed and on the path, this will be
chosen.  Shoelaces knows about Firefox, Chrome, Edge and Internet
Explorer so if you have one of these your tests will run with one of
these browsers if nothing has been explicitly configured.
- Configuration file pointing to a remote Selenium server.  If this 
configuration file exists it will be used to configure Selenium 
and the browser type automatically.  This is especially useful when 
running tools that spin up browsers in containers such as the
BrowserBox Maven Plugin.  
- A few lines of code added to a test class can make it easily choose 
a particular browser type if that browser is available locally.
This can be useful when testing locally and you need to quickly test 
against a specific browser type.

#### Easily configure the base URL of your application

By overriding the `webServerHost()` or `getBaseUrl()` methods, 
you can easily specify the base URL of your application.  This can 
still be customized through properties when running tests, but providing
sensible defaults can make tests easier to run from an IDE when the 
default base URL is known.

#### Automatically find a running instance of your application

Sometimes front-end proxies are used when doing development, such as 
when using `yarn run`.  At other times, the whole server will be
deployed and run in a single container.  Shoelaces has the ability to 
scan a number of known ports for your application and configure the
base URL automatically depending on whether it finds an instance running
on a set of ports.

For example, say there's an application whose front-end is built in 
React and uses `yarn` to build, while the back-end is built in Spring
Boot.  Typical development would involve running the Spring Boot 
instance on port 8080 and the `yarn` front-end proxy on port 3000.
But sometimes a developer might do a full build and run everything on 
port 8080.  By overriding the `webServerPotentialPorts()` method to 
return an array of `[3000, 8080]`, these ports will be scanned when
running tests and the base URL will be configured to use port 3000
only if an instance is detected to be running there.

This feature makes it easier to simply run your Selenium tests without
having to think about configuring the ports all the time.

#### Kill of stray webdriver shims after test execution

Sometimes stray Selenium webdriver processes may be left on the system.
This can happen when there are shims installed to the actual 
web driver executables for particular browser types - a known problem
when installing them using Chocolatey.  

Shoelaces will detect any webdriver processes that your tests spawned 
and after test execution has completed will kill them if they are 
still around.

### Usage

#### Setting up your project

- Add the Shoelaces Selenium module Maven dependency to your POM at test scope:
```
<dependency>
    <groupId>au.net.causal.shoelaces</groupId>
    <artifactId>shoelaces-selenium</artifactId>
    <!-- you can use the BOM and omit this version, or select the appropriate one -->
    <version>${shoelaces.version}</version> 
    <scope>test</scope>
</dependency>
```
- Add an appropriate configuration to your POM for launching your 
  tests.  e.g.
```
<plugin>
    <groupId>org.codehaus.mojo</groupId>
    <artifactId>build-helper-maven-plugin</artifactId>
    <executions>
        <execution>
            <id>reserve-tomcat-port</id>
            <goals>
                <goal>reserve-network-port</goal>
            </goals>
            <phase>pre-integration-test</phase>
            <configuration>
                <portNames>
                    <portName>tomcat.http.port</portName>
                    <portName>tomcat.ajp.port</portName>
                </portNames>
            </configuration>
        </execution>
    </executions>
</plugin>
<plugin>
    <groupId>org.codehaus.cargo</groupId>
    <artifactId>cargo-maven3-plugin</artifactId>
    <executions>
        <execution>
            <id>start-container</id>
            <phase>pre-integration-test</phase>
            <goals>
                <goal>start</goal>
            </goals>
        </execution>
        <execution>
            <id>stop-container</id>
            <phase>post-integration-test</phase>
            <goals>
                <goal>stop</goal>
            </goals>
        </execution>
    </executions>
    <configuration>
        <configuration>
            <properties>
                <cargo.servlet.port>${tomcat.http.port}</cargo.servlet.port>
                <cargo.tomcat.ajp.port>${tomcat.ajp.port}</cargo.tomcat.ajp.port>
            </properties>
        </configuration>
        <container>
            <containerId>tomcat10x</containerId>
            <log>${project.build.directory}/cargo-tomcat.log</log>
            <artifactInstaller>
                <groupId>org.apache.tomcat</groupId>
                <artifactId>tomcat</artifactId>
                <version>${tomcat.version}</version>
            </artifactInstaller>
        </container>
    </configuration>
</plugin>
<plugin>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-failsafe-plugin</artifactId>
    <configuration>
        <systemPropertyVariables>
            <test.server.port>${tomcat.http.port}</test.server.port>
        </systemPropertyVariables>
    </configuration>
    <executions>
        <execution>
            <goals>
                <goal>integration-test</goal>
                <goal>verify</goal>
            </goals>
        </execution>
    </executions>
</plugin>
```

This is just a basic configuration, there are many options for this 
one, including runnning as standalone WAR vs using Cargo and using
a plugin to launch browsers in containers such as the BrowserBox Maven plugin
instead of using the locally installed browser.

#### Installing browser drivers

This will completely depend on your operating system.  For Windows, Chocolatey can be used to
install one or more of these packages:

- [Firefox](https://chocolatey.org/packages/selenium-gecko-driver)
- [Chrome](https://chocolatey.org/packages/selenium-chrome-driver)
- [Edge](https://chocolatey.org/packages/selenium-edge-driver)
- [Internet Explorer](https://chocolatey.org/packages/selenium-ie-driver)

Other operating systems have their own installation mechanisms.  You only need a minimum of one browser
and driver to run Selenium tests locally automatically.  There are other options too, such as using
a Docker and/or the BrowserBox Maven Plugin to spin up browsers inside containers, however often
due to simplicity and ease-of-debugging a local browser is preferred - but it's up to the 
developer to choose.

#### Extend the `BaseFluentleniumTestCase` class

```
import au.net.causal.shoelaces.testing.selenium.BaseFluentleniumTestCase;

class MySeleniumIT extends BaseFluentleniumTestCase
{
}
```

This base class gives you code necessary to set up Selenium and provides detection for
the base URL and browser type.

#### Perform any necessary setup

```
/**
 * Go to the application start page before each test.
 */
@BeforeEach
void startUpOnMainPage()
{
    goToMainPage();
}
``` 

Often for a set of tests you will want to navigate to a particular web page. 
The `goToMainPage()` method provided by `BaseFluentleniumTestCase` navigates the browser
to the base URL of the application.

#### Configure base URL and ports

By default, the base URL is set to `http://localhost:8080/`.  This can be overridden by customizing
`getBaseURL()`.

It is also possible to customize the port detection behaviour, so if your server might be running
on multiple ports Shoelaces will detect which one it is running on and set the base URL accordingly.
Override the `webServerPotentialPorts()` method to do this:

```
@Override
protected int[] webServerPotentialPorts()
{
    return new int[] {3000, 8080};
}
```

In this example, the web server may be running on 3000 (typical for a Javascript proxy such as 
what can be run from `yarn` or `npm`) or 8080.  When running tests, Shoelaces will check both ports and
use 3000 if there is something running there, otherwise it will use 8080. 

#### Write a test

```
@Test
void testMainPageWithPageObjects()
{
    assertThat(window().title()).isEqualTo("Good Morning");
    assertThat($("h1")).hasText("Good Morning");
    assertThat($("p")).hasText("What will be for eating?");
}
```

Use all the normal features you would use for Selenium and Fluentlenium here.  Here is just
a very simple example.

#### Run tests from your IDE

This should be very simple, just run your test without configuration and it will use the
base URL with the detected port and any installed browser with a driver that can be found.

#### Running against a particular local browser using `quickRun()`

Sometimes you may want to run tests against a specific type of browser (maybe there's a bug
that only occurs on Edge).  Override the `quickRun()` method to select a browser type and configuration:

```
@Override
protected QuickRun quickRun()
{
    return QuickRun.edge();
}
```

This is a quick-and-dirty option that allows you to quickly run a particular test with a 
custom browser configuration.  This is something you should only do locally and not check in
to source control - when running these tests from continuous integration the default browser
should be used.  The `quickRun()` method is marked as deprecated for this reason so you remember 
not to commit this code when you override it in your tests.

However it can be handy to do this when you can't remember how to configure Selenium using
system properties.  Autocompletion after `QuickRun.[browserType()]` allows additional configuration 
depending on the browser type, for example `QuickRun.firefox().headless()` to run 
Firefox in headless mode.

