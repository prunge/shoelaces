package au.net.causal.shoelaces.jerseyclient;

import au.net.causal.shoelaces.jerseyclient.JerseyClientFactory.CloseableClient;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.ext.ContextResolver;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.glassfish.jersey.jackson.JacksonFeature;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@AutoConfiguration
@ConditionalOnClass(Client.class)
public class JerseyClientAutoConfiguration
{
    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    @Bean
    @ConditionalOnMissingBean(JerseyClientFactory.class)
    public JerseyClientFactory jerseyClientFactory(Optional<Collection<? extends JerseyClientBuilderConfigurer>> builderConfigurers,
                                                   Optional<Collection<? extends JerseyClientConfigurer>> clientConfigurers)
    {
        return () -> 
        {
            ClientBuilder builder = ClientBuilder.newBuilder();
            builderConfigurers.orElse(List.of()).forEach(configurer -> configurer.configure(builder));
            Client client = builder.build();
            clientConfigurers.orElse(List.of()).forEach(configurer -> configurer.configure(client));
            return new CloseableClient(client);
        };
    }

    @Bean
    @JerseyClientConfigured
    public ObjectMapper configureObjectMapperForJersey(Optional<Collection<? extends JerseyClientObjectMapperConfigurer>> objectMapperConfigurers, 
                                                       ObjectMapper existingObjectMapper)
    {
        ObjectMapper objectMapper = existingObjectMapper.copy();
        objectMapperConfigurers.orElse(List.of()).forEach(configurer -> configurer.configure(objectMapper));
        return objectMapper;
    }
    
    @Bean
    public JerseyClientBuilderConfigurer jacksonObjectMapperJerseyClientConfigurer(@JerseyClientConfigured ObjectMapper objectMapper)
    {
        return builder -> 
        {
            builder.register(JacksonFeature.class);
            builder.register(new ObjectMapperContextResolver(objectMapper), ContextResolver.class);
        };
    }

    private static final class ObjectMapperContextResolver implements ContextResolver<ObjectMapper>
    {
        private final ObjectMapper objectMapper;

        private ObjectMapperContextResolver(ObjectMapper objectMapper)
        {
            this.objectMapper = objectMapper;
        }

        @Override
        public ObjectMapper getContext(Class<?> type)
        {
            return this.objectMapper;
        }

    }
}
