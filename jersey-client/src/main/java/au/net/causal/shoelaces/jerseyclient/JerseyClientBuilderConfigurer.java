package au.net.causal.shoelaces.jerseyclient;

import jakarta.ws.rs.client.ClientBuilder;

/**
 * Hook for customizing Jersey client builders.  These are used when creating Jersey clients made available through
 * {@link JerseyClientFactory}.
 * <p>
 *
 * Beans that implement this interface will be picked up and their <code>configure()</code> methods will be run
 * when the Jersey client is initialized.  This allows features and settings to be easily configured for clients at the
 * builder level.
 *
 * @see JerseyClientBuilderConfigurer
 */
@FunctionalInterface
public interface JerseyClientBuilderConfigurer
{
    /**
     * Configures the Jersey client builder.
     *
     * @param clientBuilder the builder to configure.
     */
    public void configure(ClientBuilder clientBuilder);
}
