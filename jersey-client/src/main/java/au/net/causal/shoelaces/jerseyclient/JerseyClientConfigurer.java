package au.net.causal.shoelaces.jerseyclient;

import jakarta.ws.rs.client.Client;

/**
 * Hook for customizing Jersey client beans.  These are used when creating Jersey clients made available through
 * {@link JerseyClientFactory}.
 * <p>
 *
 * Beans that implement this interface will be picked up and their <code>configure()</code> methods will be run
 * when the Jersey client is initialized.  This allows features to be easily configured for clients.
 *
 * @see JerseyClientBuilderConfigurer
 */
@FunctionalInterface
public interface JerseyClientConfigurer
{
    /**
     * Configure the Jersey client.
     *
     * @param client Jersey client to configure.
     */
    public void configure(Client client);
}
