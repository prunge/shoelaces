package au.net.causal.shoelaces.jerseyclient;

import jakarta.ws.rs.client.Client;

import java.util.Objects;

/**
 * Creates pre-configured JAX-RS clients.  A bean of this type is made available for injection.
 * <p>
 *
 * Customizing Jersey clients that this factory produces can be done by making beans of type {@link JerseyClientConfigurer}
 * and {@link JerseyClientBuilderConfigurer}.
 */
public interface JerseyClientFactory
{
    /**
     * @return a JAX-RS client wrapped in an autocloseable wrapper.
     */
    public CloseableClient newClient();

    /**
     * Wrapper for clients that makes them easy to use with try-with-resources.
     */
    public static class CloseableClient implements AutoCloseable
    {
        private final Client client;

        public CloseableClient(Client client)
        {
            this.client = Objects.requireNonNull(client);
        }

        /**
         * @return the actual client.
         */
        public Client client()
        {
            return client;
        }

        @Override
        public void close()
        {
            client.close();
        }
    }
}
