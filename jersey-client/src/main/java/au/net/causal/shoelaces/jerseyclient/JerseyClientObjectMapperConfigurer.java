package au.net.causal.shoelaces.jerseyclient;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Hook for customizing the Jackson object mapper used by Jersey clients that are made available from
 * {@link JerseyClientFactory}s.
 * <p>
 *
 * Beans that implement this interface will be picked up and their <code>configure()</code> methods will be run
 * when a Jersey client is initialized.  This allows object mapper features to be easily configured from Spring Boot
 * applications for Jersey clients.
 * <p>
 *
 * Configurers only affect <i>Jersey client</i> object mappers, which are actually copied from the 'standard' object mapper in
 * Spring Boot.  This allows the standard object mapper to be used for other purposes while Jersey client ones can be
 * customized specifically.
 */
@FunctionalInterface
public interface JerseyClientObjectMapperConfigurer
{
    /**
     * Customize an object mapper that is used for Jersey clients.
     *
     * @param objectMapper the object mapper to configure.
     */
    public void configure(ObjectMapper objectMapper);
}
