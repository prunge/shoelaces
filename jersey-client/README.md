# Shoelaces Jersey Client module

Provides bean that can be used to acquire pre-configured Jersey clients
for accessing web services.  Jersey clients can be customized
through Spring configuration, for example JSON serialization can be 
configured.

### Usage

After adding the Shoelaces Jersey Client module dependency to your 
application, inject a `JerseyClientFactory` bean where you need it and 
then use it to get a Jersey client:

```
@Autowired
private JerseyClientFactory clientFactory;
```

```
try (CloseableClient c = jerseyClientFactory.newClient())
{
    Client jerseyClient = c.client();
    
    // ... do things with the Jersey client
    URI uri = URI.create("https://mywebservice.example.com/myendpoint");
    String response = c.client().target(uri)
                                .request(MediaType.TEXT_PLAIN_TYPE)
                                .get(String.class);
}
```

The actual Jersey client is wrapped in a `CloseableClient` to make
Jersey work better with try-with-resources.  The underlying client
is retrieved using `client()` as shown in the example.

### Customization

#### Jersey client builders

Jersey client builders, which are used to generate Jersey clients from
`JerseyClientFactory` beans, can be configured by registering one or 
more `JerseyClientBuilderConfigurer` beans.  Example:

```
@Bean
public JerseyClientBuilderConfigurer configureJerseyBuilder()
{
    return builder -> builder.connectTimeout(60, TimeUnit.SECONDS)
                             .readTimeout(15, TimeUnit.SECONDS);
}
```

In this example, the connect and read timeouts are configured globally
for all created Jersey clients.


#### Jersey clients
7
To customize Jersey clients that are created from `JerseyClientFactory` 
beans, register one or more `JerseyClientConfigurer` beans.  Example:

```
@Bean
public JerseyClientConfigurer configureJerseyClientCredentials(SecurityProperties securityProperties)
{
    return client -> client.register(HttpAuthenticationFeature.basicBuilder()
                                                              .credentials(securityProperties.getUser().getName(),
                                                                           securityProperties.getUser().getPassword())
                                                              .build());
}
```

In this example, basic authentication is configured globally for all 
Jersey clients.

#### JSON serialization and deserialization

To customize the Jersey client object mapper register one or more beans of 
type `JerseyClientObjectMapperConfigurer`.  This allows the Jackson 
object mapper to be customized.  In the following example, Jersey
clients will write dates as numeric timestamps (as opposed to string
representations) with millisecond precision:

```
@Bean
public JerseyClientObjectMapperConfigurer objectMapperConfigurer()
{
    return objectMapper ->
    {
        objectMapper.enable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        objectMapper.disable(SerializationFeature.WRITE_DATE_TIMESTAMPS_AS_NANOSECONDS);
    };
}
```

The configurer is a functional interface that accepts a Jackson
object mapper as a parameter.  Customize this by calling methods, 
enabling features, etc.

Jersey clients created by this module use a custom object mapper that is
copied from the default global object mapper bean.  Even if you provide
a custom global object mapper bean, the Jersey-client-specific 
customizers will only affect the version of the object mapper derived
for use with Jersey clients.
