# Shoelaces Logging Configurator module

Allows Logback logging to be custom-configured by a configuration
file under the [application home](../apphome/README.md) directory.
This is useful to allow users of your Spring Boot application to 
customize logging easily without having to set system properties or 
modify application command line arguments as is 
[required](https://docs.spring.io/spring-boot/docs/current/reference/html/howto-logging.html)
in a standard Spring Boot application.

### Usage

- Add the Shoelaces Logging Configurator module dependency to your 
application.
- When deployed, add a `logback.xml` configuration file under your
application's application home directory.
- When your application is started, this logging configuration 
will be used unless it is overridden.

See the [Application Home](../apphome/README.md) module documentation 
for more information on the application home directory and how its 
location is calculated for your application.

For more information on the `logback.xml` file format, see the 
[Logback configuration documentation](https://logback.qos.ch/manual/configuration.html).  

### Overriding logging configuration

The standard way in 
[Spring Boot to configure logging](https://docs.spring.io/spring-boot/docs/current/reference/html/howto-logging.html) 
is to set the `logging.config` system property to the location of a 
logging configuration file.  If this is done, then that file will be 
used and the config file under application home will not be used.

### Example

On a Linux machine running under user `john` and Spring Boot 
application with a main class named `com.myapp.GalahApplication`, the 
logging configuration file will be 
`/home/john/.galah/logback.xml`.
