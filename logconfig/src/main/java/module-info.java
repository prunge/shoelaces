open module au.net.causal.shoelaces.logconfig
{
    exports au.net.causal.shoelaces.logconfig;

    requires au.net.causal.shoelaces.apphome;
    requires spring.core;
    requires spring.beans;
    requires spring.boot;
    requires spring.context;
    requires spring.web;
    requires spring.boot.autoconfigure;
}
