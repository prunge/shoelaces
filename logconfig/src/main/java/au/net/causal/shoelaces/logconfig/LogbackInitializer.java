package au.net.causal.shoelaces.logconfig;

import au.net.causal.shoelaces.apphome.ApplicationHome;
import org.springframework.boot.context.event.ApplicationEnvironmentPreparedEvent;
import org.springframework.boot.context.logging.LoggingApplicationListener;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.MutablePropertySources;
import org.springframework.core.env.PropertiesPropertySource;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Properties;

/**
 * Configures Logback logging if a logback configuration file exists in the application home directory.
 * <p>
 *
 * This initializer is called early when the Spring application is initialized.  If the <code>logging.config</code>
 * property isn't set (which would indicate the user wishes to use a custom logging configuration file), the
 * application home directory (see the application home Shoelaces module documentation for more info on this)
 * is searched for a logback configuration file, and if it exists then the <code>logging.config</code>
 * property is set to the location of this file.  This allows users of a Spring Boot application that uses this
 * module to easily configure logging by adding a logging configuration file in a known place.
 * <p>
 *
 * This initializer is registered with Spring using <code>spring.factories</code> so it is loaded at startup.
 * If you wish to subclass and override some of this logic, you will need to also register your subclass using
 * <code>spring.factories</code> and ensure that your initializer has an order
 * (using the <code>@Order</code> annotation) that makes it run before this one.
 */
@Order(LoggingApplicationListener.DEFAULT_ORDER - 10) //Must go before the thing that actually initializes logging
public class LogbackInitializer implements ApplicationListener<ApplicationEvent>
{
    private final String logbackConfigFileName;

    /**
     * Default constructor, called by Spring.
     */
    public LogbackInitializer()
    {
        this("logback.xml");
    }

    /**
     * Creates a logback intiializer that looks for a logback configuration file with the specified file name
     * inside the application home directory.
     *
     * @param logbackConfigFileName logback configuration file name to look for under application home.
     */
    public LogbackInitializer(String logbackConfigFileName)
    {
        this.logbackConfigFileName = logbackConfigFileName;
    }

    @Override
    public void onApplicationEvent(ApplicationEvent event)
    {
        if (event instanceof ApplicationEnvironmentPreparedEvent)
            onApplicationEnvironmentPreparedEvent((ApplicationEnvironmentPreparedEvent)event);
    }

    /**
     * Early in application initialization, if logging config has not already been set, configure it to point to
     * the logback configuration file in the application home directory if that file exists.
     *
     * @param event the application lifecycle event.
     */
    private void onApplicationEnvironmentPreparedEvent(ApplicationEnvironmentPreparedEvent event)
    {
        //If user has already overridden logging config then leave it alone
        if (event.getEnvironment().getProperty(LoggingApplicationListener.CONFIG_PROPERTY) != null)
            return;

        //This code runs very early in the application lifecycle and no beans have been set up yet,
        //so recreate application home bean just for us from the environment
        ApplicationHome applicationHome = ApplicationHome.fromEnvironment(event.getEnvironment());
        Path logbackConfigFile = getAdditionalLogbackConfigurationFile(applicationHome);
        if (logbackConfigFile != null)
        {
            MutablePropertySources propertySources = event.getEnvironment().getPropertySources();
            Properties loggingProps = new Properties();
            loggingProps.setProperty(LoggingApplicationListener.CONFIG_PROPERTY, logbackConfigFile.toAbsolutePath().toString());
            propertySources.addFirst(new PropertiesPropertySource("logging", loggingProps));
        }
    }

    /**
     * Finds the logback configuration file under the application home directory.
     *
     * @param applicationHome the application home.
     *
     * @return the logback configuration file, or null if it does not exist.
     */
    protected Path getAdditionalLogbackConfigurationFile(ApplicationHome applicationHome)
    {
        Path logbackConfig = applicationHome.getApplicationHomeDirectory().resolve(logbackConfigFileName);

        if (Files.exists(logbackConfig))
            return logbackConfig;

        return null;
    }
}
