# Change Log

### 3.1

unreleased

- Upgrade to Fluentlenium 6.0.0. 

### 3.0

2023-05-22

- Upgrade to Spring Boot 3.1.0
- Add missing jersey-common library to BOM


### 2.2

2022-10-08

- Upgrade to Spring Boot 2.7.2
- Add abstract test case for testing React apps with HTMLUnit with workarounds for HTMLUnit bugs
- Add web module that can be used to allow Spring to deal with React single-page applications
- Add workaround/fix for Jersey's bad application/JSON top-level string response so that these
  are written as proper JSON quoted strings
- Add handler in Jersey module for handling date/time query parameters 

### 2.1.1

2020-07-21

- Fixed non-deployment of modules on release by making last module releasable.
  Fixes Nexus deploy issue <https://issues.sonatype.org/browse/NEXUS-19853>.

### 2.1

2020-07-21

- Upgrade to Spring Boot 2.3.1
- Fixed apphome and Jersey compatibility issues with latest Spring Boot
  version
- Selenium: Added hook to be able to customize Selenium capabilities after
  webdriver has been created

### 2.0

2019-08-11

- Initial 2.0 version
