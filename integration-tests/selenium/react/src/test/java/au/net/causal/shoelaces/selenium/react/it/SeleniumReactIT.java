package au.net.causal.shoelaces.selenium.react.it;

import au.net.causal.shoelaces.testing.selenium.react.BaseReactFluentleniumTestCase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static io.fluentlenium.assertj.FluentLeniumAssertions.*;

class SeleniumReactIT extends BaseReactFluentleniumTestCase
{
    /**
     * Explicitly test HTMLUnit.
     */
    @Override
    @SuppressWarnings("deprecation") //It's deprecated because it's not meant to be used in checked in code, but this is an exception for our ITs
    protected QuickRun quickRun()
    {
        return QuickRun.htmlUnit();
    }

    /**
     * Go to the application start page before each test.
     */
    @BeforeEach
    void startUpOnMainPage()
    {
        goToMainPage();
    }

    @Test
    void testMainPageHasExpectedContent()
    {
        assertThat($("img")).hasClass("App-logo");
        assertThat($("a")).hasClass("App-link")
                          .hasText("Learn React")
                          .hasAttributeValue("href", "https://reactjs.org");
    }
}
