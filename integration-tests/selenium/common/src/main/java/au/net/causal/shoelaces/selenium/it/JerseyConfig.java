package au.net.causal.shoelaces.selenium.it;

import jakarta.ws.rs.ApplicationPath;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

@Component
@ApplicationPath("/api")
public class JerseyConfig extends ResourceConfig
{
    public JerseyConfig()
    {
        register(AppResource.class);
    }
}
