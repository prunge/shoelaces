package au.net.causal.shoelaces.selenium.it;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
@Path("/")
public class AppResource
{
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("stuff")
    public List<String> getStuff()
    {
        return Arrays.asList("one", "two", "three");
    }
}
