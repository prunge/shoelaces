package au.net.causal.shoelaces.selenium.it;

import au.net.causal.shoelaces.testing.JerseyServerEndpointExtension;
import jakarta.ws.rs.NotAuthorizedException;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.*;

class RestResourcesBadLoginIT
{
    @RegisterExtension
    static JerseyServerEndpointExtension server = new JerseyServerEndpointExtension()
                                                        .usePath("/api");

    @RegisterExtension
    static JerseyServerEndpointExtension serverRoot = new JerseyServerEndpointExtension();

    private WebTarget stuffTarget()
    {
        return server.target("stuff");
    }

    @Test
    void wrongLogin()
    {
        server.useLogin("wronguser", "wrongpassword");
        assertThatExceptionOfType(NotAuthorizedException.class).isThrownBy(() ->
        {
            stuffTarget().request(MediaType.APPLICATION_JSON_TYPE).get(new GenericType<List<String>>(){});
        });
    }

    @Test
    void noLogin()
    {
        server.useNoLogin();
        assertThatExceptionOfType(NotAuthorizedException.class).isThrownBy(() ->
        {
            stuffTarget().request(MediaType.APPLICATION_JSON_TYPE).get(new GenericType<List<String>>(){});
        });
    }

    /**
     * Resources outside /api should not be protected by password.
     */
    @Test
    void unprotectedPage()
    {
        serverRoot.useNoLogin();
        String result = serverRoot.target().request(MediaType.TEXT_HTML).get(String.class);
        assertThat(result).contains("Good Morning");
    }
}
