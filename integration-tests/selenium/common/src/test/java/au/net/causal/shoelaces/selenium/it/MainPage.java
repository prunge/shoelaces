package au.net.causal.shoelaces.selenium.it;

import io.fluentlenium.core.FluentPage;
import io.fluentlenium.core.domain.FluentWebElement;
import org.openqa.selenium.support.FindBy;

class MainPage extends FluentPage
{
    @FindBy(tagName = "h1")
    private FluentWebElement heading;

    @FindBy(tagName = "p")
    private FluentWebElement message;

    public String getHeadingText()
    {
        return heading.text();
    }

    public String getMessageText()
    {
        return message.text();
    }
}
