package au.net.causal.shoelaces.selenium.it;

import au.net.causal.shoelaces.testing.selenium.BaseFluentleniumTestCase;
import io.fluentlenium.core.annotation.Page;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.assertj.core.api.Assertions.*;
import static io.fluentlenium.assertj.FluentLeniumAssertions.*;

class SeleniumIT extends BaseFluentleniumTestCase
{
    private static final Logger log = LoggerFactory.getLogger(SeleniumIT.class);

    @Page
    private MainPage mainPage;

    /**
     * For the integration test, we'll force the use of HTMLUnit so it can run anywhere and won't need a version of
     * Chrome of Firefox installed.  Since this is a dead-simple page HTMLUnit should work fine with it.
     */
    @Override
    @SuppressWarnings("deprecation") //It's deprecated because it's not meant to be used in checked in code, but this is an exception for our ITs
    protected QuickRun quickRun()
    {
        return QuickRun.htmlUnit();
    }

    /**
     * Go to the application start page before each test.
     */
    @BeforeEach
    void startUpOnMainPage()
    {
        goToMainPage();
    }

    @Test
    void testMainPageWithoutPageObjects()
    {
        log.info("Page title: " + window().title());
        assertThat(window().title()).isEqualTo("Good Morning");
        assertThat($("h1")).hasText("Good Morning");
        assertThat($("p")).hasText("What will be for eating?");
    }

    @Test
    void testMainPageWithPageObjects()
    {
        log.info("Page title: " + window().title());
        assertThat(window().title()).isEqualTo("Good Morning");
        assertThat(mainPage.getHeadingText()).isEqualToNormalizingWhitespace("Good Morning");
        assertThat(mainPage.getMessageText()).isEqualToNormalizingWhitespace("What will be for eating?");
    }
}
