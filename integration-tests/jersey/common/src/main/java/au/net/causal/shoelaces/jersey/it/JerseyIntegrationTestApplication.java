package au.net.causal.shoelaces.jersey.it;

import au.net.causal.shoelaces.jersey.JerseyObjectMapperConfigurer;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@SpringBootApplication
public class JerseyIntegrationTestApplication extends SpringBootServletInitializer
{
    public static void main(String[] args)
    {
        SpringApplication.run(JerseyIntegrationTestApplication.class, args);
    }
    
    @Bean
    public JerseyObjectMapperConfigurer customizeJerseyObjectMapper()
    {
        return om -> om.enable(SerializationFeature.WRITE_ENUMS_USING_INDEX)
                       //For the date/time tests, want source timezone/offset to be passed through
                       .disable(DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE);
    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http)
    throws Exception
    {
        http.csrf(c ->
        {
            c.disable();
        });
        http.authorizeHttpRequests(r ->
        {
            r.requestMatchers(new AntPathRequestMatcher("/api/**")).authenticated()
             .anyRequest().permitAll();
        });
        http.httpBasic(c -> {});

        return http.build();
    }
}
