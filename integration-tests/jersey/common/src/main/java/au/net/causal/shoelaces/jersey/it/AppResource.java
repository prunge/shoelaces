package au.net.causal.shoelaces.jersey.it;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
@Path("/")
public class AppResource
{
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("stuff")
    public List<String> getStuff()
    {
        return Arrays.asList("one", "two", "three");
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("somethingElse")
    public MyResponse getSomethingElse()
    {
        return new MyResponse(Thingy.C);
    }

    /**
     * By default, Jersey returns JSON strings in plain format (no surrounding quotes) which is wrong.  This tests that our custom provider fixes this problem.
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("jsonString")
    public String jsonString()
    {
        return "a JSON string";
    }

    /**
     * Use this to make sure the custom provider for JSON strings doesn't affect text/plain responses.
     */
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("plainString")
    public String plainString()
    {
        return "a plain string";
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("jsonNumber")
    public int jsonNumber()
    {
        return 42;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("jsonBoolean")
    public boolean jsonBoolean()
    {
        return true;
    }

    public static class MyResponse
    {
        public Thingy thingy;

        public MyResponse(Thingy thingy)
        {
            this.thingy = thingy;
        }
    }

    public static enum Thingy
    {
        A, B, C, D;
    }
}
