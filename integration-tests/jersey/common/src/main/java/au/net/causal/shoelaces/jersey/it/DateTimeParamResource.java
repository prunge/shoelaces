package au.net.causal.shoelaces.jersey.it;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.MonthDay;
import java.time.OffsetDateTime;
import java.time.OffsetTime;
import java.time.Period;
import java.time.Year;
import java.time.YearMonth;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;

@Component
@Path("/dateTimeParams")
public class DateTimeParamResource
{
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("localDate")
    public LocalDate localDate(@QueryParam("date") @DefaultValue("2000-01-01") LocalDate d)
    {
        return d.plusDays(3);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("localDateCustomFormat")
    public LocalDate localDateCustomFormat(@QueryParam("date") @DefaultValue("01-01-2000") @JsonFormat(pattern = "dd-MM-yyyy") LocalDate d)
    {
        return d.plusDays(3);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("localTime")
    public LocalTime localTime(@QueryParam("time") @DefaultValue("19:15") LocalTime t)
    {
        return t.plusMinutes(30);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("localTimeCustomFormat")
    public LocalTime localTimeCustomFormat(@QueryParam("time") @DefaultValue("7:15 PM") @JsonFormat(pattern = "h:mm a", locale = "en_AU") LocalTime t)
    {
        return t.plusMinutes(30);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("localDateTime")
    public LocalDateTime localDateTime(@QueryParam("date") @DefaultValue("2000-01-01T19:15") LocalDateTime d)
    {
        return d.plusDays(3);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("localDateTimeCustomFormat")
    public LocalDateTime localDateTimeCustomFormat(@QueryParam("date") @DefaultValue("01-01-2000 7:15 PM") @JsonFormat(pattern = "dd-MM-yyyy h:mm a", locale = "en_AU") LocalDateTime d)
    {
        return d.plusDays(3);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("zonedDateTime")
    public ZonedDateTime zonedDateTime(@QueryParam("date") @DefaultValue("2000-01-01T19:15Z") ZonedDateTime d)
    {
        return d.plusDays(3);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("zonedDateTimeCustomFormat")
    public ZonedDateTime zonedDateTimeCustomFormat(@QueryParam("date") @DefaultValue("01-01-2000 7:15 PM Z") @JsonFormat(pattern = "dd-MM-yyyy h:mm a z", locale = "en_AU") ZonedDateTime d)
    {
        return d.plusDays(3);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("offsetDateTime")
    public OffsetDateTime offsetDateTime(@QueryParam("date") @DefaultValue("2000-01-01T19:15Z") OffsetDateTime d)
    {
        return d.plusDays(3);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("offsetDateTimeCustomFormat")
    public OffsetDateTime offsetDateTimeCustomFormat(@QueryParam("date") @DefaultValue("01-01-2000 7:15 PM Z") @JsonFormat(pattern = "dd-MM-yyyy h:mm a z", locale = "en_AU") OffsetDateTime d)
    {
        return d.plusDays(3);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("offsetTime")
    public OffsetTime offsetTime(@QueryParam("time") @DefaultValue("19:15Z") OffsetTime t)
    {
        return t.plusHours(3);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("offsetTimeCustomFormat")
    public OffsetTime offsetTimeCustomFormat(@QueryParam("time") @DefaultValue("7:15 PM Z") @JsonFormat(pattern = "h:mm a z", locale = "en_AU") OffsetTime t)
    {
        return t.plusHours(3);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("instant")
    public Instant instant(@QueryParam("date") @DefaultValue("2000-01-01T19:15:00Z") Instant d)
    {
        return d.plus(3, ChronoUnit.HOURS);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("instantCustomFormat")
    public Instant instantCustomFormat(@QueryParam("date") @DefaultValue("01-01-2000 7:15 PM Z") @JsonFormat(pattern = "dd-MM-yyyy h:mm a z", locale = "en_AU") Instant d)
    {
        return d.plus(3, ChronoUnit.HOURS);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("yearMonth")
    public LocalDate yearMonth(@QueryParam("date") @DefaultValue("2000-01") YearMonth d)
    {
        return d.atDay(9);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("yearMonthCustomFormat")
    public LocalDate yearMonthCustomFormat(@QueryParam("date") @DefaultValue("01-2000") @JsonFormat(pattern = "MM-yyyy") YearMonth d)
    {
        return d.atDay(9);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("monthDay")
    public LocalDate monthDay(@QueryParam("date") @DefaultValue("--01-02") MonthDay d)
    {
        return d.atYear(1977);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("monthDayCustomFormat")
    public LocalDate monthDayCustomFormat(@QueryParam("date") @DefaultValue("02-01") @JsonFormat(pattern = "dd-MM") MonthDay d)
    {
        return d.atYear(1977);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("year")
    public LocalDate year(@QueryParam("year") @DefaultValue("2000") Year y)
    {
        return y.atMonth(1).atDay(2);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("yearCustomFormat")
    public LocalDate yearCustomFormat(@QueryParam("year") @DefaultValue("10") @JsonFormat(pattern = "yy") Year y)
    {
        return y.atMonth(1).atDay(2);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("period")
    public LocalDate period(@QueryParam("period") @DefaultValue("P1M2D") Period p)
    {
        return LocalDate.of(2000, 1, 1).plus(p);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("duration")
    public LocalDateTime duration(@QueryParam("duration") @DefaultValue("PT6H15M") Duration d)
    {
        return LocalDateTime.of(2000, 1, 1, 3, 10).plus(d);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("zoneOffset")
    public ZoneOffset zoneOffset(@QueryParam("zoneOffset") @DefaultValue("Z") ZoneOffset z)
    {
        return ZoneOffset.ofTotalSeconds(z.getTotalSeconds() + Math.toIntExact(Duration.ofHours(2).toSeconds()));
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("zoneId")
    public ZoneOffset zoneId(@QueryParam("zoneId") @DefaultValue("Z") ZoneId z)
    {
        return z.getRules().getOffset(LocalDateTime.of(1977, 1, 9, 0, 0));
    }
}
