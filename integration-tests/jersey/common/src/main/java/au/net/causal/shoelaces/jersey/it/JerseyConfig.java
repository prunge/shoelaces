package au.net.causal.shoelaces.jersey.it;

import au.net.causal.shoelaces.jersey.JerseyJacksonDateTimeParamConverterProvider;
import au.net.causal.shoelaces.jersey.common.JacksonJsonStringWriterProvider;
import jakarta.ws.rs.ApplicationPath;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

@Component
@ApplicationPath("/api")
public class JerseyConfig extends ResourceConfig
{
    public JerseyConfig()
    {
        register(AppResource.class);
        register(DateTimeParamResource.class);

        //This will ensure application/json Strings are written as JSON strings and not plain strings
        register(JacksonJsonStringWriterProvider.class);

        register(JerseyJacksonDateTimeParamConverterProvider.class);
    }
}
