package au.net.causal.shoelaces.jersey.it;

import au.net.causal.shoelaces.jersey.common.JacksonJsonStringReaderProvider;
import au.net.causal.shoelaces.testing.JerseyServerEndpointExtension;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;

import static org.assertj.core.api.Assertions.*;

/**
 * Tests the {@link au.net.causal.shoelaces.jersey.JerseyJacksonDateTimeParamConverterProvider} converts dates and times properly.
 */
class DateTimeParamIT
{
    @RegisterExtension
    static JerseyServerEndpointExtension server = new JerseyServerEndpointExtension()
                                                        .useLogin("admin", "admin")
                                                        .usePath("/api/dateTimeParams")
                                                        //Required for proper handling of JSON string results
                                                        .useClientBuilderConfigurer(builder -> builder.register(JacksonJsonStringReaderProvider.class));

    @Nested
    class LocalDate
    {
        private WebTarget localDateParamTarget()
        {
            return server.target("localDate");
        }

        private WebTarget localDateCustomFormatParamTarget()
        {
            return server.target("localDateCustomFormat");
        }

        @Test
        void simple()
        {
            String result = localDateParamTarget().queryParam("date", "1977-01-09")
                                                  .request(MediaType.APPLICATION_JSON_TYPE)
                                                  .get(String.class);
            assertThat(result).isEqualTo("1977-01-12");
        }

        @Test
        void defaultValues()
        {
            String result = localDateParamTarget().request(MediaType.APPLICATION_JSON_TYPE)
                                                  .get(String.class);
            assertThat(result).isEqualTo("2000-01-04");
        }

        @Test
        void invalidFormat()
        {
            Response response = localDateParamTarget().queryParam("date", "not-a-date")
                                                      .request(MediaType.APPLICATION_JSON_TYPE)
                                                      .get();
            assertThat(response.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());
        }

        @Test
        void customFormat()
        {
            String result = localDateCustomFormatParamTarget().queryParam("date", "09-01-1977")
                                                              .request(MediaType.APPLICATION_JSON_TYPE)
                                                              .get(String.class);
            assertThat(result).isEqualTo("1977-01-12");
        }

        @Test
        void customFormatDefaultValues()
        {
            String result = localDateCustomFormatParamTarget().request(MediaType.APPLICATION_JSON_TYPE)
                                                              .get(String.class);
            assertThat(result).isEqualTo("2000-01-04");
        }

        @Test
        void customFormatInvalidFormat()
        {
            Response response = localDateCustomFormatParamTarget().queryParam("date", "not-a-date")
                                                                  .request(MediaType.APPLICATION_JSON_TYPE)
                                                                  .get();
            assertThat(response.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());
        }
    }

    @Nested
    class LocalTime
    {
        private WebTarget localTimeParamTarget()
        {
            return server.target("localTime");
        }

        private WebTarget localTimeCustomFormatParamTarget()
        {
            return server.target("localTimeCustomFormat");
        }

        @Test
        void simple()
        {
            String result = localTimeParamTarget().queryParam("time", "17:15:05")
                                                  .request(MediaType.APPLICATION_JSON_TYPE)
                                                  .get(String.class);
            assertThat(result).isEqualTo("17:45:05");
        }

        @Test
        void hhmm()
        {
            String result = localTimeParamTarget().queryParam("time", "17:15")
                                                  .request(MediaType.APPLICATION_JSON_TYPE)
                                                  .get(String.class);
            assertThat(result).isEqualTo("17:45:00");
        }

        @Test
        void defaultValues()
        {
            String result = localTimeParamTarget().request(MediaType.APPLICATION_JSON_TYPE)
                                                  .get(String.class);
            assertThat(result).isEqualTo("19:45:00");
        }

        @Test
        void invalidFormat()
        {
            Response response = localTimeParamTarget().queryParam("time", "not-a-time")
                                                      .request(MediaType.APPLICATION_JSON_TYPE)
                                                      .get();
            assertThat(response.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());
        }

        @Test
        void customFormat()
        {
            String result = localTimeCustomFormatParamTarget().queryParam("time", "7:45 PM")
                                                              .request(MediaType.APPLICATION_JSON_TYPE)
                                                              .get(String.class);
            assertThat(result).isEqualTo("20:15:00");
        }

        @Test
        void customFormatDefaultValues()
        {
            String result = localTimeCustomFormatParamTarget().request(MediaType.APPLICATION_JSON_TYPE)
                                                              .get(String.class);
            assertThat(result).isEqualTo("19:45:00");
        }

        @Test
        void customFormatInvalidFormat()
        {
            Response response = localTimeCustomFormatParamTarget().queryParam("time", "not-a-time")
                                                                  .request(MediaType.APPLICATION_JSON_TYPE)
                                                                  .get();
            assertThat(response.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());
        }
    }

    @Nested
    class LocalDateTime
    {
        private WebTarget localDateTimeParamTarget()
        {
            return server.target("localDateTime");
        }

        private WebTarget localDateTimeCustomFormatParamTarget()
        {
            return server.target("localDateTimeCustomFormat");
        }

        @Test
        void simple()
        {
            String result = localDateTimeParamTarget().queryParam("date", "1977-01-09T17:15")
                                                      .request(MediaType.APPLICATION_JSON_TYPE)
                                                      .get(String.class);
            assertThat(result).isEqualTo("1977-01-12T17:15:00");
        }

        @Test
        void defaultValues()
        {
            String result = localDateTimeParamTarget().request(MediaType.APPLICATION_JSON_TYPE)
                                                      .get(String.class);
            assertThat(result).isEqualTo("2000-01-04T19:15:00");
        }

        @Test
        void invalidFormat()
        {
            Response response = localDateTimeParamTarget().queryParam("date", "not-a-date")
                                                          .request(MediaType.APPLICATION_JSON_TYPE)
                                                          .get();
            assertThat(response.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());
        }

        @Test
        void customFormat()
        {
            String result = localDateTimeCustomFormatParamTarget().queryParam("date", "09-01-1977 7:45 PM")
                                                                  .request(MediaType.APPLICATION_JSON_TYPE)
                                                                  .get(String.class);
            assertThat(result).isEqualTo("1977-01-12T19:45:00");
        }

        @Test
        void customFormatDefaultValues()
        {
            String result = localDateTimeCustomFormatParamTarget().request(MediaType.APPLICATION_JSON_TYPE)
                                                                  .get(String.class);
            assertThat(result).isEqualTo("2000-01-04T19:15:00");
        }

        @Test
        void customFormatInvalidFormat()
        {
            Response response = localDateTimeCustomFormatParamTarget().queryParam("date", "not-a-date")
                                                                      .request(MediaType.APPLICATION_JSON_TYPE)
                                                                      .get();
            assertThat(response.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());
        }
    }

    @Nested
    class ZonedDateTime
    {
        private WebTarget zonedDateTimeParamTarget()
        {
            return server.target("zonedDateTime");
        }

        private WebTarget zonedDateTimeCustomFormatParamTarget()
        {
            return server.target("zonedDateTimeCustomFormat");
        }

        @Test
        void simple()
        {
            String result = zonedDateTimeParamTarget().queryParam("date", "1977-01-09T17:15Z")
                                                      .request(MediaType.APPLICATION_JSON_TYPE)
                                                      .get(String.class);
            assertThat(result).isEqualTo("1977-01-12T17:15:00Z");
        }

        @Test
        void withZone()
        {
            String result = zonedDateTimeParamTarget().queryParam("date", "1977-01-09T17:15Z[Australia/Brisbane]")
                                                      .request(MediaType.APPLICATION_JSON_TYPE)
                                                      .get(String.class);
            //Brisbane is +10:00 and no daylight savings, so expect result to be sent with expected offset
            //Seems the ZonedDateTime's toString() is used on response which loses actual zone
            assertThat(result).isEqualTo("1977-01-13T03:15:00+10:00");
        }

        @Test
        void defaultValues()
        {
            String result = zonedDateTimeParamTarget().request(MediaType.APPLICATION_JSON_TYPE)
                                                      .get(String.class);
            assertThat(result).isEqualTo("2000-01-04T19:15:00Z");
        }

        @Test
        void invalidFormat()
        {
            Response response = zonedDateTimeParamTarget().queryParam("date", "not-a-date")
                                                          .request(MediaType.APPLICATION_JSON_TYPE)
                                                          .get();
            assertThat(response.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());
        }

        @Test
        void customFormat()
        {
            String result = zonedDateTimeCustomFormatParamTarget().queryParam("date", "09-01-1977 7:45 PM Z")
                                                                  .request(MediaType.APPLICATION_JSON_TYPE)
                                                                  .get(String.class);
            assertThat(result).isEqualTo("1977-01-12T19:45:00Z");
        }

        @Test
        void customFormatDefaultValues()
        {
            String result = zonedDateTimeCustomFormatParamTarget().request(MediaType.APPLICATION_JSON_TYPE)
                                                                  .get(String.class);
            assertThat(result).isEqualTo("2000-01-04T19:15:00Z");
        }

        @Test
        void customFormatInvalidFormat()
        {
            Response response = zonedDateTimeCustomFormatParamTarget().queryParam("date", "not-a-date")
                                                                      .request(MediaType.APPLICATION_JSON_TYPE)
                                                                      .get();
            assertThat(response.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());
        }
    }

    @Nested
    class OffsetDateTime
    {
        private WebTarget offsetDateTimeParamTarget()
        {
            return server.target("offsetDateTime");
        }

        private WebTarget offsetDateTimeCustomFormatParamTarget()
        {
            return server.target("offsetDateTimeCustomFormat");
        }

        @Test
        void simple()
        {
            String result = offsetDateTimeParamTarget().queryParam("date", "1977-01-09T17:15-11:00")
                                                       .request(MediaType.APPLICATION_JSON_TYPE)
                                                       .get(String.class);
            assertThat(result).isEqualTo("1977-01-12T17:15:00-11:00");
        }

        @Test
        void defaultValues()
        {
            String result = offsetDateTimeParamTarget().request(MediaType.APPLICATION_JSON_TYPE)
                                                       .get(String.class);
            assertThat(result).isEqualTo("2000-01-04T19:15:00Z");
        }

        @Test
        void invalidFormat()
        {
            Response response = offsetDateTimeParamTarget().queryParam("date", "not-a-date")
                                                           .request(MediaType.APPLICATION_JSON_TYPE)
                                                           .get();
            assertThat(response.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());
        }

        @Test
        void customFormat()
        {
            String result = offsetDateTimeCustomFormatParamTarget().queryParam("date", "09-01-1977 7:45 PM -11:00")
                                                                   .request(MediaType.APPLICATION_JSON_TYPE)
                                                                   .get(String.class);
            assertThat(result).isEqualTo("1977-01-12T19:45:00-11:00");
        }

        @Test
        void customFormatDefaultValues()
        {
            String result = offsetDateTimeCustomFormatParamTarget().request(MediaType.APPLICATION_JSON_TYPE)
                                                                   .get(String.class);
            assertThat(result).isEqualTo("2000-01-04T19:15:00Z");
        }

        @Test
        void customFormatInvalidFormat()
        {
            Response response = offsetDateTimeCustomFormatParamTarget().queryParam("date", "not-a-date")
                                                                       .request(MediaType.APPLICATION_JSON_TYPE)
                                                                       .get();
            assertThat(response.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());
        }
    }

    @Nested
    class OffsetTime
    {
        private WebTarget offsetTimeParamTarget()
        {
            return server.target("offsetTime");
        }

        private WebTarget offsetTimeCustomFormatParamTarget()
        {
            return server.target("offsetTimeCustomFormat");
        }

        @Test
        void simple()
        {
            String result = offsetTimeParamTarget().queryParam("time", "17:15-11:00")
                                                   .request(MediaType.APPLICATION_JSON_TYPE)
                                                   .get(String.class);
            assertThat(result).isEqualTo("20:15-11:00");
        }

        @Test
        void defaultValues()
        {
            String result = offsetTimeParamTarget().request(MediaType.APPLICATION_JSON_TYPE)
                                                   .get(String.class);
            assertThat(result).isEqualTo("22:15Z");
        }

        @Test
        void invalidFormat()
        {
            Response response = offsetTimeParamTarget().queryParam("time", "not-a-time")
                                                       .request(MediaType.APPLICATION_JSON_TYPE)
                                                       .get();
            assertThat(response.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());
        }

        @Test
        void customFormat()
        {
            String result = offsetTimeCustomFormatParamTarget().queryParam("time", "5:45 PM -11:00")
                                                               .request(MediaType.APPLICATION_JSON_TYPE)
                                                               .get(String.class);
            assertThat(result).isEqualTo("20:45-11:00");
        }

        @Test
        void customFormatDefaultValues()
        {
            String result = offsetTimeCustomFormatParamTarget().request(MediaType.APPLICATION_JSON_TYPE)
                                                               .get(String.class);
            assertThat(result).isEqualTo("22:15Z");
        }

        @Test
        void customFormatInvalidFormat()
        {
            Response response = offsetTimeCustomFormatParamTarget().queryParam("time", "not-a-time")
                                                                   .request(MediaType.APPLICATION_JSON_TYPE)
                                                                   .get();
            assertThat(response.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());
        }
    }

    @Nested
    class Instant
    {
        private WebTarget instantParamTarget()
        {
            return server.target("instant");
        }

        private WebTarget instantCustomFormatParamTarget()
        {
            return server.target("instantCustomFormat");
        }

        @Test
        void simple()
        {
            String result = instantParamTarget().queryParam("date", "1977-01-09T17:15:00Z")
                                                .request(MediaType.APPLICATION_JSON_TYPE)
                                                .get(String.class);
            assertThat(result).isEqualTo("1977-01-09T20:15:00Z");
        }

        @Test
        void defaultValues()
        {
            String result = instantParamTarget().request(MediaType.APPLICATION_JSON_TYPE)
                                                .get(String.class);
            assertThat(result).isEqualTo("2000-01-01T22:15:00Z");
        }

        @Test
        void invalidFormat()
        {
            Response response = instantParamTarget().queryParam("date", "not-a-date")
                                                    .request(MediaType.APPLICATION_JSON_TYPE)
                                                    .get();
            assertThat(response.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());
        }

        @Test
        void customFormat()
        {
            String result = instantCustomFormatParamTarget().queryParam("date", "09-01-1977 7:45 PM -11:00")
                                                            .request(MediaType.APPLICATION_JSON_TYPE)
                                                            .get(String.class);
            assertThat(result).isEqualTo("1977-01-10T09:45:00Z");
        }

        @Test
        void customFormatDefaultValues()
        {
            String result = instantCustomFormatParamTarget().request(MediaType.APPLICATION_JSON_TYPE)
                                                            .get(String.class);
            assertThat(result).isEqualTo("2000-01-01T22:15:00Z");
        }

        @Test
        void customFormatInvalidFormat()
        {
            Response response = instantCustomFormatParamTarget().queryParam("date", "not-a-date")
                                                                .request(MediaType.APPLICATION_JSON_TYPE)
                                                                .get();
            assertThat(response.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());
        }
    }

    @Nested
    class YearMonth
    {
        private WebTarget yearMonthParamTarget()
        {
            return server.target("yearMonth");
        }

        private WebTarget yearMonthCustomFormatParamTarget()
        {
            return server.target("yearMonthCustomFormat");
        }

        @Test
        void simple()
        {
            String result = yearMonthParamTarget().queryParam("date", "1977-01")
                                                  .request(MediaType.APPLICATION_JSON_TYPE)
                                                  .get(String.class);
            assertThat(result).isEqualTo("1977-01-09");
        }

        @Test
        void defaultValues()
        {
            String result = yearMonthParamTarget().request(MediaType.APPLICATION_JSON_TYPE)
                                                  .get(String.class);
            assertThat(result).isEqualTo("2000-01-09");
        }

        @Test
        void invalidFormat()
        {
            Response response = yearMonthParamTarget().queryParam("date", "not-a-date")
                                                      .request(MediaType.APPLICATION_JSON_TYPE)
                                                      .get();
            assertThat(response.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());
        }

        @Test
        void customFormat()
        {
            String result = yearMonthCustomFormatParamTarget().queryParam("date", "01-1977")
                                                              .request(MediaType.APPLICATION_JSON_TYPE)
                                                              .get(String.class);
            assertThat(result).isEqualTo("1977-01-09");
        }

        @Test
        void customFormatDefaultValues()
        {
            String result = yearMonthCustomFormatParamTarget().request(MediaType.APPLICATION_JSON_TYPE)
                                                              .get(String.class);
            assertThat(result).isEqualTo("2000-01-09");
        }

        @Test
        void customFormatInvalidFormat()
        {
            Response response = yearMonthCustomFormatParamTarget().queryParam("date", "not-a-date")
                                                                  .request(MediaType.APPLICATION_JSON_TYPE)
                                                                  .get();
            assertThat(response.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());
        }
    }

    @Nested
    class MonthDay
    {
        private WebTarget monthDayParamTarget()
        {
            return server.target("monthDay");
        }

        private WebTarget monthDayCustomFormatParamTarget()
        {
            return server.target("monthDayCustomFormat");
        }

        @Test
        void simple()
        {
            String result = monthDayParamTarget().queryParam("date", "--01-09")
                                                 .request(MediaType.APPLICATION_JSON_TYPE)
                                                 .get(String.class);
            assertThat(result).isEqualTo("1977-01-09");
        }

        @Test
        void defaultValues()
        {
            String result = monthDayParamTarget().request(MediaType.APPLICATION_JSON_TYPE)
                                                 .get(String.class);
            assertThat(result).isEqualTo("1977-01-02");
        }

        @Test
        void invalidFormat()
        {
            Response response = monthDayParamTarget().queryParam("date", "not-a-date")
                                                     .request(MediaType.APPLICATION_JSON_TYPE)
                                                     .get();
            assertThat(response.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());
        }

        @Test
        void customFormat()
        {
            String result = monthDayCustomFormatParamTarget().queryParam("date", "09-01")
                                                             .request(MediaType.APPLICATION_JSON_TYPE)
                                                             .get(String.class);
            assertThat(result).isEqualTo("1977-01-09");
        }

        @Test
        void customFormatDefaultValues()
        {
            String result = monthDayCustomFormatParamTarget().request(MediaType.APPLICATION_JSON_TYPE)
                                                             .get(String.class);
            assertThat(result).isEqualTo("1977-01-02");
        }

        @Test
        void customFormatInvalidFormat()
        {
            Response response = monthDayCustomFormatParamTarget().queryParam("date", "not-a-date")
                                                                 .request(MediaType.APPLICATION_JSON_TYPE)
                                                                 .get();
            assertThat(response.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());
        }
    }

    @Nested
    class Year
    {
        private WebTarget yearParamTarget()
        {
            return server.target("year");
        }

        private WebTarget yearCustomFormatParamTarget()
        {
            return server.target("yearCustomFormat");
        }

        @Test
        void simple()
        {
            String result = yearParamTarget().queryParam("year", "1977")
                                             .request(MediaType.APPLICATION_JSON_TYPE)
                                             .get(String.class);
            assertThat(result).isEqualTo("1977-01-02");
        }

        @Test
        void defaultValues()
        {
            String result = yearParamTarget().request(MediaType.APPLICATION_JSON_TYPE)
                                             .get(String.class);
            assertThat(result).isEqualTo("2000-01-02");
        }

        @Test
        void invalidFormat()
        {
            Response response = yearParamTarget().queryParam("year", "not-a-year")
                                                 .request(MediaType.APPLICATION_JSON_TYPE)
                                                 .get();
            assertThat(response.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());
        }

        @Test
        void customFormat()
        {
            String result = yearCustomFormatParamTarget().queryParam("year", "05")
                                                         .request(MediaType.APPLICATION_JSON_TYPE)
                                                         .get(String.class);
            assertThat(result).isEqualTo("2005-01-02");
        }

        @Test
        void customFormatDefaultValues()
        {
            String result = yearCustomFormatParamTarget().request(MediaType.APPLICATION_JSON_TYPE)
                                                         .get(String.class);
            assertThat(result).isEqualTo("2010-01-02");
        }

        @Test
        void customFormatInvalidFormat()
        {
            Response response = yearCustomFormatParamTarget().queryParam("year", "not-a-year")
                                                             .request(MediaType.APPLICATION_JSON_TYPE)
                                                             .get();
            assertThat(response.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());
        }
    }

    @Nested
    class Period
    {
        private WebTarget periodParamTarget()
        {
            return server.target("period");
        }

        @Test
        void simple()
        {
            String result = periodParamTarget().queryParam("period", "P3M8D")
                                               .request(MediaType.APPLICATION_JSON_TYPE)
                                               .get(String.class);
            assertThat(result).isEqualTo("2000-04-09");
        }

        @Test
        void defaultValues()
        {
            String result = periodParamTarget().request(MediaType.APPLICATION_JSON_TYPE)
                                               .get(String.class);
            assertThat(result).isEqualTo("2000-02-03");
        }

        @Test
        void invalidFormat()
        {
            Response response = periodParamTarget().queryParam("period", "not-a-period")
                                                   .request(MediaType.APPLICATION_JSON_TYPE)
                                                   .get();
            assertThat(response.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());
        }
    }

    @Nested
    class Duration
    {
        private WebTarget durationParamTarget()
        {
            return server.target("duration");
        }

        @Test
        void simple()
        {
            String result = durationParamTarget().queryParam("duration", "P3DT7H15M")
                                                 .request(MediaType.APPLICATION_JSON_TYPE)
                                                 .get(String.class);
            assertThat(result).isEqualTo("2000-01-04T10:25:00");
        }

        @Test
        void defaultValues()
        {
            String result = durationParamTarget().request(MediaType.APPLICATION_JSON_TYPE)
                                                 .get(String.class);
            assertThat(result).isEqualTo("2000-01-01T09:25:00");
        }

        @Test
        void invalidFormat()
        {
            Response response = durationParamTarget().queryParam("duration", "not-a-duration")
                                                     .request(MediaType.APPLICATION_JSON_TYPE)
                                                     .get();
            assertThat(response.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());
        }
    }

    @Nested
    class ZoneOffset
    {
        private WebTarget zoneOffsetParamTarget()
        {
            return server.target("zoneOffset");
        }

        @Test
        void simple()
        {
            String result = zoneOffsetParamTarget().queryParam("zoneOffset", "-11:00")
                                                   .request(MediaType.APPLICATION_JSON_TYPE)
                                                   .get(String.class);
            assertThat(result).isEqualTo("-09:00");
        }

        @Test
        void defaultValues()
        {
            String result = zoneOffsetParamTarget().request(MediaType.APPLICATION_JSON_TYPE)
                                                   .get(String.class);
            assertThat(result).isEqualTo("+02:00");
        }

        @Test
        void invalidFormat()
        {
            Response response = zoneOffsetParamTarget().queryParam("zoneOffset", "not-a-zone-offset")
                                                       .request(MediaType.APPLICATION_JSON_TYPE)
                                                       .get();
            assertThat(response.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());
        }
    }

    @Nested
    class ZoneId
    {
        private WebTarget zoneIdParamTarget()
        {
            return server.target("zoneId");
        }

        @Test
        void simple()
        {
            String result = zoneIdParamTarget().queryParam("zoneId", "Australia/Sydney")
                                               .request(MediaType.APPLICATION_JSON_TYPE)
                                               .get(String.class);
            assertThat(result).isEqualTo("+11:00");
        }

        @Test
        void defaultValues()
        {
            String result = zoneIdParamTarget().request(MediaType.APPLICATION_JSON_TYPE)
                                               .get(String.class);
            assertThat(result).isEqualTo("Z");
        }

        @Test
        void invalidFormat()
        {
            Response response = zoneIdParamTarget().queryParam("zoneId", "not-a-zone-id")
                                                   .request(MediaType.APPLICATION_JSON_TYPE)
                                                   .get();
            assertThat(response.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());
        }
    }
}
