package au.net.causal.shoelaces.jersey.it;

import au.net.causal.shoelaces.testing.JerseyServerEndpointExtension;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.MediaType;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import static org.assertj.core.api.Assertions.*;

class JsonStringResponseIT
{
    @RegisterExtension
    static JerseyServerEndpointExtension server = new JerseyServerEndpointExtension()
                                                        .useLogin("admin", "admin")
                                                        .usePath("/api");

    private WebTarget jsonStringTarget()
    {
        return server.target("jsonString");
    }

    private WebTarget plainStringTarget()
    {
        return server.target("plainString");
    }

    @Test
    void applicationJsonString()
    throws IOException
    {
        //Use input stream to get raw response content
        InputStream response = jsonStringTarget().request(MediaType.APPLICATION_JSON_TYPE).get(InputStream.class);
        byte[] responseRawContent = response.readAllBytes();
        String responseRawString = new String(responseRawContent, StandardCharsets.UTF_8);

        //Check the JSON string is quoted
        assertThat(responseRawString).isEqualTo("\"a JSON string\"");
    }

    @Test
    void textPlainString()
    throws IOException
    {
        //Use input stream to get raw response content
        InputStream response = plainStringTarget().request(MediaType.TEXT_PLAIN_TYPE).get(InputStream.class);
        byte[] responseRawContent = response.readAllBytes();
        String responseRawString = new String(responseRawContent, StandardCharsets.UTF_8);

        //Check there are no JSON quotes on this one
        assertThat(responseRawString).isEqualTo("a plain string");
    }
}
