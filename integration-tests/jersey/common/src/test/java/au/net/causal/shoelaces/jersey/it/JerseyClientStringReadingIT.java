package au.net.causal.shoelaces.jersey.it;

import au.net.causal.shoelaces.jersey.common.JacksonJsonStringReaderProvider;
import au.net.causal.shoelaces.jersey.common.UnexpectedJsonTypeException;
import au.net.causal.shoelaces.testing.JerseyServerEndpointExtension;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.MediaType;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;

import static org.assertj.core.api.Assertions.*;

/**
 * Test the {@link au.net.causal.shoelaces.jersey.common.JacksonJsonStringReaderProvider} class
 * also works on the client side when reading JSON strings.  Without it, a client would get
 * extra quote-marks in the input.
 * <p>
 *
 * Separated into a different test class since the client needs to be configured specially with our
 * reader.
 */
class JerseyClientStringReadingIT
{
    @RegisterExtension
    static JerseyServerEndpointExtension server = new JerseyServerEndpointExtension()
            .useLogin("admin", "admin")
            .usePath("/api")
            //This will make the client read JSON strings properly
            .useClientBuilderConfigurer(builder -> builder.register(JacksonJsonStringReaderProvider.class));

    private WebTarget stuffTarget()
    {
        return server.target("stuff");
    }

    private WebTarget jsonStringTarget()
    {
        return server.target("jsonString");
    }

    private WebTarget plainStringTarget()
    {
        return server.target("plainString");
    }

    private WebTarget jsonNumberTarget()
    {
        return server.target("jsonNumber");
    }

    private WebTarget jsonBooleanTarget()
    {
        return server.target("jsonBoolean");
    }

    /**
     * Without the custom provider on the client (but working on the server), application/json
     * responses will be read as plain text and therefore we'll get quoted JSON as the result, which
     * is incorrect.
     */
    @Test
    void jsonStringReading()
    {
        String result = jsonStringTarget().request(MediaType.APPLICATION_JSON_TYPE)
                                          .get(String.class);
        assertThat(result).isEqualTo("a JSON string");
    }

    /**
     * This test ensures that our changes made for reading application/json does not mess up reading
     * text/plain responses.
     */
    @Test
    void plainStringReading()
    {
        String result = plainStringTarget().request(MediaType.TEXT_PLAIN_TYPE)
                                           .get(String.class);
        assertThat(result).isEqualTo("a plain string");
    }

    /**
     * Should be able to convert primitive JSON types into strings.
     */
    @Test
    void jsonNumberAsStringReading()
    {
        String result = jsonNumberTarget().request(MediaType.APPLICATION_JSON_TYPE)
                                          .get(String.class);
        assertThat(result).isEqualTo("42");
    }

    /**
     * This is just the default behaviour, our provider shouldn't get in the way of this.
     */
    @Test
    void jsonNumberAsNumberReading()
    {
        Integer result = jsonNumberTarget().request(MediaType.APPLICATION_JSON_TYPE)
                                           .get(Integer.class);
        assertThat(result).isEqualTo(42);
    }

    /**
     * Should be able to convert primitive JSON types into strings.
     */
    @Test
    void jsonBooleanAsStringReading()
    {
        String result = jsonBooleanTarget().request(MediaType.APPLICATION_JSON_TYPE)
                                           .get(String.class);
        assertThat(result).isEqualTo("true");
    }

    /**
     * Attempting to read a non-primitive JSON type from the client as a JSON string should
     * result in an error.
     */
    @Test
    void jsonArrayAsStringReading()
    {
        assertThatExceptionOfType(UnexpectedJsonTypeException.class).isThrownBy(
                () -> stuffTarget().request(MediaType.APPLICATION_JSON_TYPE)
                                   .get(String.class)
        );
    }
}
