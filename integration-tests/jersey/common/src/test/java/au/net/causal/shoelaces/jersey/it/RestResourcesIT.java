package au.net.causal.shoelaces.jersey.it;

import au.net.causal.shoelaces.testing.JerseyServerEndpointExtension;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;

import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.*;

class RestResourcesIT
{
    @RegisterExtension
    static JerseyServerEndpointExtension server = new JerseyServerEndpointExtension()
                                                        .useLogin("admin", "admin")
                                                        .usePath("/api");

    private WebTarget stuffTarget()
    {
        return server.target("stuff");
    }

    private WebTarget somethingElseTarget()
    {
        return server.target("somethingElse");
    }

    @Test
    void simpleJerseyCall()
    {
        List<String> results = stuffTarget().request(MediaType.APPLICATION_JSON_TYPE)
                                            .get(new GenericType<List<String>>(){});
        assertThat(results).containsExactly("one", "two", "three");
    }

    @Test
    void objectMapperConfigurationOnServerTest()
    {
        Map<String, String> response = somethingElseTarget().request(MediaType.APPLICATION_JSON_TYPE)
                                                            .get(new GenericType<Map<String, String>>() {});

        //Because WRITE_ENUMS_USING_INDEX mode is turned on, we should get "2" as the value instead of "C"
        assertThat(response).isEqualTo(Map.of("thingy", "2"));
    }
}
