package au.net.causal.shoelaces.jdbc.it;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
@Path("/")
public class AppResource
{
    @Autowired
    private DataSource dataSource;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("stuff")
    public List<String> getStuff()
    {
        return Arrays.asList("one", "two", "three");
    }
    
    private boolean tableExists(String name, Connection con)
    throws SQLException
    {
        try (ResultSet rs = con.getMetaData().getTables(null, null, name, null))
        {
            return rs.next();
        }
    }
    
    private void dropTable(String name, Connection con)
    throws SQLException
    {
        try (PreparedStatement stat = con.prepareStatement("drop table " + name))
        {
            stat.execute();
        }
    }
    
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("setUpDatabase")
    public String setUpDatabase()
    throws SQLException
    {
        try (Connection con = dataSource.getConnection())
        {
            //If table exists, drop it
            if (tableExists("CAT", con))
                dropTable("CAT", con);
            
            //Recreate table
            try (PreparedStatement stat = con.prepareStatement("create table CAT (name varchar(50) not null primary key, owner varchar(50) not null)"))
            {
                stat.execute();
            }
        }
        
        return "CAT";
    }    
    
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("addCat")
    public String addCat(@QueryParam("name") String name, @QueryParam("owner") String owner)
    throws SQLException
    {
        try (Connection con = dataSource.getConnection(); 
             PreparedStatement stat = con.prepareStatement("insert into CAT (name, owner) values (?, ?)"))
        {
            stat.setString(1, name);
            stat.setString(2, owner);
            stat.executeUpdate();
        }
        
        return name;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("getCats")
    public List<String> getCats()
    throws SQLException
    {
        List<String> catNames = new ArrayList<>();
        try (Connection con = dataSource.getConnection();
             PreparedStatement stat = con.prepareStatement("select name from CAT order by name");
             ResultSet rs = stat.executeQuery())
        {
            while (rs.next())
            {
                String name = rs.getString(1);
                catNames.add(name);
            }
        }

        return catNames;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("getConnectionWarnings")
    public List<String> getConnectionWarnings()
    throws SQLException
    {
        List<String> warningText = new ArrayList<>();
        try (Connection con = dataSource.getConnection())
        {
            SQLWarning warning = con.getWarnings();
            while (warning != null)
            {
                warningText.add(warning.getMessage());
                warning = warning.getNextWarning();
            }
        }

        return warningText;
    }


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("getBirds")
    public List<String> getBirds()
    throws SQLException
    {
        List<String> birdNames = new ArrayList<>();
        try (Connection con = dataSource.getConnection();
             PreparedStatement stat = con.prepareStatement("select name from BIRD order by name");
             ResultSet rs = stat.executeQuery())
        {
            while (rs.next())
            {
                String name = rs.getString(1);
                birdNames.add(name);
            }
        }

        return birdNames;
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("getDatabaseUrl")
    public String getDatabaseUrl()
    throws SQLException
    {
        try (Connection con = dataSource.getConnection())
        {
            return con.getMetaData().getURL();
        }
    }
}
