package au.net.causal.shoelaces.jdbc.it;

import au.net.causal.shoelaces.testing.JerseyServerEndpointExtension;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class DatabaseResourcesIT
{
    @RegisterExtension
    static JerseyServerEndpointExtension server = new JerseyServerEndpointExtension()
                                                        .useLogin("admin", "admin")
                                                        .usePath("/api");

    private WebTarget setUpDatabaseTarget()
    {
        return server.target("setUpDatabase");
    }

    private WebTarget addCatTarget()
    {
        return server.target("addCat");
    }

    private WebTarget getCatsTarget()
    {
        return server.target("getCats");
    }

    private WebTarget getConnectionWarningsTarget()
    {
        return server.target("getConnectionWarnings");
    }

    @BeforeEach
    void setUpDatabase()
    {
        String result = setUpDatabaseTarget().request(MediaType.TEXT_PLAIN_TYPE)
                                             .get(String.class);
        assertThat(result).isEqualTo("CAT");
    }

    @Test
    void databaseAddAndReading()
    {
        //Add some cats
        String getResult = addCatTarget().queryParam("name", "Twisp")
                                         .queryParam("owner", "John Galah")
                                         .request(MediaType.TEXT_PLAIN_TYPE)
                                         .get(String.class);
        assertThat(getResult).isEqualTo("Twisp");
        getResult = addCatTarget().queryParam("name", "Dinah-Kah")
                                  .queryParam("owner", "Jenny Cockatoo")
                                  .request(MediaType.TEXT_PLAIN_TYPE)
                                  .get(String.class);
        assertThat(getResult).isEqualTo("Dinah-Kah");

        //List the cats
        List<String> cats = getCatsTarget().request(MediaType.APPLICATION_JSON_TYPE)
                                           .get(new GenericType<List<String>>() {});
        assertThat(cats).containsExactly("Dinah-Kah", "Twisp");
    }

    /**
     * With Derby, when create=true every connection gives a warning that the database was not created but a connection
     * made to existing database instead.  We have code in Shoelaces to pre-create the DB and then use another URL
     * without create=true if needed.  This test ensures this functionality is working.
     */
    @Test
    void databaseConnectionHasNoWarnings()
    {
        List<String> warnings = getConnectionWarningsTarget().request(MediaType.APPLICATION_JSON_TYPE)
                                                             .get(new GenericType<List<String>>() {});
        assertThat(warnings).isEmpty();
    }
}
