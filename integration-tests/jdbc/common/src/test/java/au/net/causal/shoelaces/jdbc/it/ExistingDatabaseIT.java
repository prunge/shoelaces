package au.net.causal.shoelaces.jdbc.it;

import au.net.causal.shoelaces.testing.JerseyServerEndpointExtension;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Run tests against the database with tables and data that should have been added by a script in the Maven plugin.
 * Proves that the database is file-system backed and is using the right directory.
 */
@Tag("diskonly")
class ExistingDatabaseIT
{
    @RegisterExtension
    static JerseyServerEndpointExtension server = new JerseyServerEndpointExtension()
                                                        .useLogin("admin", "admin")
                                                        .usePath("/api");

    private WebTarget getBirdsTarget()
    {
        return server.target("getBirds");
    }

    @Test
    void databaseAddAndReading()
    {
        //List the birds
        List<String> birds = getBirdsTarget().request(MediaType.APPLICATION_JSON_TYPE)
                                             .get(new GenericType<List<String>>() {});
        assertThat(birds).containsExactly("Squinky", "Whiteman");
    }
}
