package au.net.causal.shoelaces.jdbc.it;

import au.net.causal.shoelaces.testing.JerseyServerEndpointExtension;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.MediaType;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;

import static org.assertj.core.api.Assertions.*;

/**
 * Tests to verify the database is in-memory only.
 */
class MemoryDatabaseIT
{
    @RegisterExtension
    static JerseyServerEndpointExtension server = new JerseyServerEndpointExtension()
                                                        .useLogin("admin", "admin")
                                                        .usePath("/api");

    private WebTarget getDatabaseUrlTarget()
    {
        return server.target("getDatabaseUrl");
    }

    @Test
    void databaseIsInMemory()
    {
        String jdbcUrl = getDatabaseUrlTarget().request(MediaType.TEXT_PLAIN_TYPE)
                                               .get(String.class);
        assertThat(jdbcUrl).startsWith("jdbc:derby:memory:");
    }
}
