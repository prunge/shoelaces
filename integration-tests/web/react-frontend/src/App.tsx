import React from 'react';
import logo from './logo.svg';
import './App.css';
import {BrowserRouter, NavLink, Switch, Route} from 'react-router-dom';

function App() {
  return (
    <BrowserRouter basename={new URL(document.baseURI).pathname}>
      <nav>
        <ul>
          <li>
            <NavLink to="/" exact={true} activeStyle={{color: "red"}}>Home</NavLink>
          </li>
          <li>
            <NavLink to="/galah" exact={true} activeStyle={{color: "red"}}>Galah Page</NavLink>
          </li>
        </ul>
      </nav>

      <Switch>
        <Route path="/galah" exact={true}>
          <div className="galahMessage">This page is full of galahs!</div>
        </Route>
        <Route path="/" exact={true}>
          <div className="App">
            <header className="App-header">
              <img src={logo} className="App-logo" alt="logo" />
              <p>
                Edit <code>src/App.tsx</code> and save to reload.
              </p>
              <a
                  className="App-link"
                  href="https://reactjs.org"
                  target="_blank"
                  rel="noopener noreferrer"
              >
                Learn React
              </a>
            </header>
          </div>
        </Route>
      </Switch>
    </BrowserRouter>
  );
}

export default App;
