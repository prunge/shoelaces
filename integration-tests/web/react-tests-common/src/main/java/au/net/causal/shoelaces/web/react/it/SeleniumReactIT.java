package au.net.causal.shoelaces.web.react.it;

import au.net.causal.shoelaces.testing.selenium.react.BaseReactFluentleniumTestCase;
import io.fluentlenium.core.domain.FluentList;
import io.fluentlenium.core.domain.FluentWebElement;
import org.junit.jupiter.api.Test;

import static io.fluentlenium.assertj.FluentLeniumAssertions.*;

class SeleniumReactIT extends BaseReactFluentleniumTestCase
{
    /**
     * If the app is running on a base context, this property will be set to it.  Otherwise assume we are running under root context path.
     */
    private static final String contextPath = System.getProperty("test.server.context", "");

    SeleniumReactIT()
    {
        server.usePath(contextPath);
    }

    /**
     * Explicitly test HTMLUnit.
     */
    @Override
    @SuppressWarnings("deprecation") //It's deprecated because it's not meant to be used in checked in code, but this is an exception for our ITs
    protected QuickRun quickRun()
    {
        return QuickRun.htmlUnit();
    }

    @Test
    void mainPageHasExpectedContent()
    {
        goToMainPage();
        assertThat($("img")).hasClass("App-logo");
        assertThat($("a.App-link")).hasText("Learn React")
                                   .hasAttributeValue("href", "https://reactjs.org");
    }

    @Test
    void mainPageHasNavBar()
    {
        goToMainPage();
        FluentList<FluentWebElement> navItems = $("nav li a");
        assertThat(navItems).hasSize(2);
        assertThat(navItems.get(0)).hasText("Home")
                                   .hasAttributeValue("href", server.getUri().toString())
                                   .isClickable();
        assertThat(navItems.get(1)).hasText("Galah Page")
                                   .hasAttributeValue("href", server.getUri().resolve("galah").toString())
                                   .isClickable();
    }

    @Test
    void navigationToSecondaryPageByJavascriptNavigation()
    {
        goToMainPage();

        //Check original page is displayed
        assertThat($("a.App-link")).isNotEmpty();

        //Use nav-bar to navigate to secondary page using React router
        FluentWebElement galahPageLink = $("nav li a").get(1);
        assertThat(galahPageLink).isClickable();
        galahPageLink.click(); //Click on 'Galah Page' link
        FluentWebElement messageElement = $("div.galahMessage").single();
        await().until(messageElement).displayed();

        //Check proper content of secondary page
        assertThat(messageElement).hasText("This page is full of galahs!");

        //Check original page has gone away
        assertThat($("a.App-link")).isEmpty();
    }

    /**
     * Go directly to the secondary page /galah by using a URL in the browser (like with a bookmark).
     * Tests that the server-side redirects work.
     */
    @Test
    void secondaryPageByDirectBrowserUrl()
    {
        goTo(server.getUri().resolve("galah"));

        //Check proper content of secondary page
        assertThat($("div.galahMessage").single()).hasText("This page is full of galahs!");

        //Check we are not on the original page with the React logo
        assertThat($("a.App-link")).isEmpty();
    }

    /**
     * The /ibis URL is defined to be served by a Spring controller.  The single-page Javascript application redirect logic should only be used as a fallback,
     * so this page and any Spring controller or Jersey endpoint should be served properly and not redirected.
     */
    @Test
    void springControllerPageIsStillServed()
    {
        goTo(server.getUri().resolve("ibis"));
        assertThat($("h1")).hasText("Hello");
        assertThat($("p")).hasText("You have hit the ibis page!");
    }
}
