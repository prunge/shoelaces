package au.net.causal.shoelaces.web.react.it;

import au.net.causal.shoelaces.testing.JerseyServerEndpointExtension;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.*;

/**
 * Verify that Jersey calls to /api/* can make it through the single page filter.  That filter is supposed to intercept patterns like that, but if Jersey is also registered
 * those URLs should be taken by Jersey (and other specific registrations) and only afterwards the filter gets everything else.
 */
public class RestResourcesIT
{
    /**
     * If the app is running on a base context, this property will be set to it.  Otherwise assume we are running under root context path.
     */
    private static final String contextPath = System.getProperty("test.server.context", "/");

    @RegisterExtension
    static JerseyServerEndpointExtension server = new JerseyServerEndpointExtension()
                                                        .usePath(contextPath + "api");

    private WebTarget stuffTarget()
    {
        return server.target("stuff");
    }

    @Test
    void simpleJerseyCall()
    {
        List<String> results = stuffTarget().request(MediaType.APPLICATION_JSON_TYPE)
                                            .get(new GenericType<List<String>>(){});
        assertThat(results).containsExactly("one", "two", "three");
    }
}
