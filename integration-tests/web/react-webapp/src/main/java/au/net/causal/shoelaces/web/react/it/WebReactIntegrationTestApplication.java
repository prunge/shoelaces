package au.net.causal.shoelaces.web.react.it;

import au.net.causal.shoelaces.web.EnableJavascriptRoutedSinglePageApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
@EnableJavascriptRoutedSinglePageApplication
public class WebReactIntegrationTestApplication extends SpringBootServletInitializer
{
    public static void main(String[] args)
    {
        SpringApplication.run(WebReactIntegrationTestApplication.class, args);
    }
}
