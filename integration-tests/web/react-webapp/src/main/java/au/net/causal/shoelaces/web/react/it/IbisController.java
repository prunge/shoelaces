package au.net.causal.shoelaces.web.react.it;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * A server-side page served from a controller to prove that the explicitly defined controllers still work and won't get redirected
 * by the single-page javascript application redirection logic.
 */
@RestController
public class IbisController
{
    @RequestMapping("/ibis")
    public String ibisPage()
    {
        return "<html><body><h1>Hello</h1><p>You have hit the ibis page!</p></body></html>";
    }
}
