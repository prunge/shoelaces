package au.net.causal.shoelaces.assorted.it;

import au.net.causal.shoelaces.testing.JerseyServerEndpointExtension;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.*;

class RestResourcesIT
{
    @RegisterExtension
    static JerseyServerEndpointExtension server = new JerseyServerEndpointExtension()
                                                        .useLogin("admin", "admin")
                                                        .usePath("/api");

    private WebTarget stuffTarget()
    {
        return server.target("stuff");
    }

    private WebTarget clockTarget()
    {
        return server.target("clock");
    }

    @Test
    void simpleJerseyCall()
    {
        List<String> results = stuffTarget().request(MediaType.APPLICATION_JSON_TYPE)
                                            .get(new GenericType<List<String>>(){});
        assertThat(results).containsExactly("one", "two", "three");
    }

    @Test
    void clockInjectionWorks()
    {
        String result = clockTarget().request(MediaType.APPLICATION_JSON_TYPE)
                                     .get(String.class);

        //It's using the current date, so really only need to verify the call worked
        //If clock was not injected, the call woudl fail as a clock would not be able to be injected
        assertThat(result).isNotEmpty();
    }
}
