package au.net.causal.shoelaces.assorted.it;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.Clock;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;

@Component
@Path("/")
public class AppResource
{
    @Autowired
    private Clock clock;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("stuff")
    public List<String> getStuff()
    {
        return Arrays.asList("one", "two", "three");
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("clock")
    public String clock()
    {
        return LocalDateTime.now(clock).format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
    }
}
