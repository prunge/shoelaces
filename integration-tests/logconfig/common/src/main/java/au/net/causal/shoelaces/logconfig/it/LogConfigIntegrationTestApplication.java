package au.net.causal.shoelaces.logconfig.it;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@SpringBootApplication
public class LogConfigIntegrationTestApplication extends SpringBootServletInitializer
{
    public static void main(String[] args)
    {
        SpringApplication.run(LogConfigIntegrationTestApplication.class, args);
    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http)
    throws Exception
    {
        http.authorizeHttpRequests(r ->
        {
            r.requestMatchers(new AntPathRequestMatcher("/api/**")).authenticated()
             .anyRequest().permitAll();
        });
        http.httpBasic(c -> {});

        return http.build();
    }
}
