package au.net.causal.shoelaces.logconfig.it;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
@Path("/")
public class AppResource
{
    private static final Logger log = LoggerFactory.getLogger(AppResource.class);

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("stuff")
    public List<String> getStuff()
    {
        return Arrays.asList("one", "two", "three");
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("logwrite")
    public String logWrite(@QueryParam("message") String message)
    {
        log.info("Wrote log message: " + message);
        return "done";
    }
}
