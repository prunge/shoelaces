package au.net.causal.shoelaces.logconfig.it;

import au.net.causal.shoelaces.testing.JerseyServerEndpointExtension;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class RestResourcesIT
{
    @RegisterExtension
    static JerseyServerEndpointExtension server = new JerseyServerEndpointExtension()
                                                        .useLogin("admin", "admin")
                                                        .usePath("/api");

    private Path testHome() 
    {
        Path appHome;
        String homeStr = System.getProperty("test.application.home.directory");
        if (homeStr == null)
        {
            //For running in an IDE, use the default application home
            appHome = Paths.get(System.getProperty("user.home"), ".").resolve(".logconfigintegrationtest");
        }
        else
            appHome = Paths.get(homeStr);
        
        return appHome.toAbsolutePath().normalize();
    }
    
    private Path logDirectory() 
    {
        return testHome().resolve("logs");
    }
    
    private Path logFile()
    {
        return logDirectory().resolve("endpoint.log");
    }
    
    private WebTarget stuffTarget()
    {
        return server.target("stuff");
    }

    private WebTarget logWriteTarget()
    {
        return server.target("logwrite");
    }

    @Test
    void simpleJerseyCall()
    {
        List<String> results = stuffTarget().request(MediaType.APPLICATION_JSON_TYPE)
                                            .get(new GenericType<List<String>>(){});
        assertThat(results).containsExactly("one", "two", "three");
    }

    @Test
    void loggingWorks()
    throws IOException
    {
        String result = logWriteTarget().queryParam("message", "Make sure you log this")
                                        .request(MediaType.TEXT_PLAIN_TYPE)
                                        .get(String.class);
        assertThat(result).isEqualTo("done");

        assertThat(logFile()).exists();
        String fileContent = new String(Files.readAllBytes(logFile()), StandardCharsets.UTF_8).trim();
        assertThat(fileContent).endsWith("Wrote log message: Make sure you log this");
    }
}
