package au.net.causal.shoelaces.liquibase.it;

import au.net.causal.shoelaces.testing.JerseyServerEndpointExtension;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class RestResourcesIT
{
    @RegisterExtension
    static JerseyServerEndpointExtension server = new JerseyServerEndpointExtension()
                                                        .useLogin("admin", "admin")
                                                        .usePath("/api");

    private static Path appServerLogFile;

    private static WebTarget stuffTarget()
    {
        return server.target("stuff");
    }

    private static WebTarget applicationHomeDirectoryTarget()
    {
        return server.target("applicationHomeDirectory");
    }

    @BeforeAll
    static void setUpAppServerLogFile()
    {
        String homeDirectory = applicationHomeDirectoryTarget().request(MediaType.TEXT_PLAIN_TYPE)
                                                               .get(String.class);

        //This specific file is configured in logback.xml of the test application
        //It logs application startup events and all liquibase activity
        appServerLogFile = Paths.get(homeDirectory).resolve("logs").resolve("liquibase.log");
        assertThat(appServerLogFile).exists();
    }

    @Test
    void simpleJerseyCall()
    {
        List<String> results = stuffTarget().request(MediaType.APPLICATION_JSON_TYPE)
                                            .get(new GenericType<List<String>>(){});
        assertThat(results).containsExactly("one", "two", "three");
    }

    @Test
    void derbyNotClosedByLiquibase()
    throws IOException
    {
        //Application must have fully started up - validate this to ensure we're not still waiting/flushing
        assertThat(Files.lines(appServerLogFile)).describedAs("Application log should contain startup message")
                                                 .anyMatch(line -> line.contains("Started LiquibaseIntegrationTestApplication in "));

        //Validate that Liquibase has not closed Derby connection by checking the logs
        assertThat(Files.lines(appServerLogFile)).describedAs("Derby should not have been shut down by Liquibase")
                                                 .noneMatch(line -> line.contains("Shutting down derby connection: jdbc:derby"));
    }
}
