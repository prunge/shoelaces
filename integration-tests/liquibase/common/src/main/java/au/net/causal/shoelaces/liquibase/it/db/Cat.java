package au.net.causal.shoelaces.liquibase.it.db;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import java.time.LocalDate;

@Entity
public class Cat
{
    @JsonIgnore
    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    @NotBlank
    @Size(min = 1, max = 100)
    private String name;

    @NotNull
    @NotBlank
    @Size(min = 1, max = 100)
    private String owner;

    private LocalDate born;

    protected Cat()
    {
    }

    public Cat(String name, String owner, LocalDate born)
    {
        this.name = name;
        this.owner = owner;
        this.born = born;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getOwner()
    {
        return owner;
    }

    public void setOwner(String owner)
    {
        this.owner = owner;
    }

    public LocalDate getBorn()
    {
        return born;
    }

    public void setBorn(LocalDate born)
    {
        this.born = born;
    }
}
