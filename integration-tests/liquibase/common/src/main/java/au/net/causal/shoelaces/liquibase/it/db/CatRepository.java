package au.net.causal.shoelaces.liquibase.it.db;

import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;

public interface CatRepository extends CrudRepository<Cat, Long>, QuerydslPredicateExecutor<Cat>
{

}
