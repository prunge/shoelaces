package au.net.causal.shoelaces.liquibase.it;

import au.net.causal.shoelaces.apphome.ApplicationHome;
import au.net.causal.shoelaces.liquibase.it.db.Cat;
import au.net.causal.shoelaces.liquibase.it.db.CatRepository;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import org.glassfish.jersey.internal.guava.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static au.net.causal.shoelaces.liquibase.it.db.QCat.cat;

@Component
@Path("/")
public class AppResource
{
    @Autowired
    private CatRepository catRepository;

    @Autowired
    private ApplicationHome appConfig;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("stuff")
    public List<String> getStuff()
    {
        return Arrays.asList("one", "two", "three");
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("morestuff")
    public List<Cat> getMoreStuff()
    {
        return Lists.newArrayList(catRepository.findAll(cat.name.asc()));
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("addcat")
    public Cat addCat(@QueryParam("name") String name, @QueryParam("owner") String owner, @QueryParam("born") String bornStr)
    {
        LocalDate born = LocalDate.parse(bornStr);
        Cat cat = new Cat(name, owner, born);
        cat = catRepository.save(cat);
        return cat;
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("applicationHomeDirectory")
    public String getApplicationHomeDirectory()
    {
        return appConfig.getApplicationHomeDirectory().toAbsolutePath().toString();
    }
}
