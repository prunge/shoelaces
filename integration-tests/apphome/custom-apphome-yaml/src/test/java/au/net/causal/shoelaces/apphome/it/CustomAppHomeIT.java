package au.net.causal.shoelaces.apphome.it;

import au.net.causal.shoelaces.testing.JerseyServerEndpointExtension;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.MediaType;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;

import static org.assertj.core.api.Assertions.*;

class CustomAppHomeIT
{
    @RegisterExtension
    static JerseyServerEndpointExtension server = new JerseyServerEndpointExtension()
                                                        .useLogin("admin", "admin")
                                                        .usePath("/api");

    private WebTarget customValueTarget()
    {
        return server.target("valueFromProps");
    }

    @Test
    void applicationYamlInAppHomeIsUsed()
    {
        String customPropValue = customValueTarget().request(MediaType.TEXT_PLAIN_TYPE)
                                                    .get(String.class);
        assertThat(customPropValue).isEqualTo("something-else"); //Set in custom application.yaml in app home
    }
}
