package au.net.causal.shoelaces.apphome.it;

import au.net.causal.shoelaces.apphome.ApplicationHome;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
@Path("/")
public class AppResource
{
    @Autowired
    private ApplicationHome appConfig;

    @Value("${apphometest.myValue}")
    private String valueFromProps;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("stuff")
    public List<String> getStuff()
    {
        return Arrays.asList("one", "two", "three");
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("applicationName")
    public String getApplicationName()
    {
        return appConfig.getApplicationName();
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("applicationHomeDirectory")
    public String getApplicationHomeDirectory()
    {
        return appConfig.getApplicationHomeDirectory().toAbsolutePath().toString();
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("valueFromProps")
    public String getValueFromProps()
    {
        return valueFromProps;
    }

}
