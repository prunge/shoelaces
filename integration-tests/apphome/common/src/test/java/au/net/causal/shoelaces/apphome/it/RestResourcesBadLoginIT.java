package au.net.causal.shoelaces.apphome.it;

import au.net.causal.shoelaces.testing.JerseyServerEndpointExtension;
import jakarta.ws.rs.NotAuthorizedException;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.*;

class RestResourcesBadLoginIT
{
    @RegisterExtension
    static JerseyServerEndpointExtension server = new JerseyServerEndpointExtension()
                                                        .usePath("/api");

    @RegisterExtension
    static JerseyServerEndpointExtension serverRoot = new JerseyServerEndpointExtension();

    private WebTarget stuffTarget()
    {
        return server.target("stuff");
    }

    @Test
    void wrongLogin()
    {
        server.useLogin("wronguser", "wrongpassword");
        assertThatExceptionOfType(NotAuthorizedException.class).isThrownBy(() ->
        {
            stuffTarget().request(MediaType.APPLICATION_JSON_TYPE).get(new GenericType<List<String>>(){});
        });
    }

    @Test
    void noLogin()
    {
        server.useNoLogin();
        assertThatExceptionOfType(NotAuthorizedException.class).isThrownBy(() ->
        {
            stuffTarget().request(MediaType.APPLICATION_JSON_TYPE).get(new GenericType<List<String>>(){});
        });
    }

    /**
     * Resources outside /api should not be protected by password.  Page does not exist though, so should get 404 not found instead of unauthorized.
     */
    @Test
    void notFoundForUnprotectedMissingResource()
    {
        serverRoot.useNoLogin();
        assertThatExceptionOfType(NotFoundException.class).isThrownBy(() ->
        {
            serverRoot.target().request(MediaType.TEXT_HTML).get(String.class);
        });
    }
}
