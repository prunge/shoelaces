package au.net.causal.shoelaces.apphome.it;

import au.net.causal.shoelaces.testing.JerseyServerEndpointExtension;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.*;

class RestResourcesIT
{
    @RegisterExtension
    static JerseyServerEndpointExtension server = new JerseyServerEndpointExtension()
                                                        .useLogin("admin", "admin")
                                                        .usePath("/api");

    private WebTarget stuffTarget()
    {
        return server.target("stuff");
    }

    private WebTarget applicationNameTarget()
    {
        return server.target("applicationName");
    }

    private WebTarget applicationHomeDirectoryTarget()
    {
        return server.target("applicationHomeDirectory");
    }

    private WebTarget customValueTarget()
    {
        return server.target("valueFromProps");
    }

    @Test
    void simpleJerseyCall()
    {
        List<String> results = stuffTarget().request(MediaType.APPLICATION_JSON_TYPE)
                                            .get(new GenericType<List<String>>(){});
        assertThat(results).containsExactly("one", "two", "three");
    }

    @Test
    void appConfigApplicationName()
    {
        String name = applicationNameTarget().request(MediaType.TEXT_PLAIN_TYPE)
                                           .get(String.class);
        assertThat(name).isEqualTo("apphomeintegrationtest");
    }

    @Tag("appHomeAutoName")
    @Test
    void appConfigApplicationHomeDirectory()
    {
        String homeDirectory = applicationHomeDirectoryTarget().request(MediaType.TEXT_PLAIN_TYPE)
                                                               .get(String.class);
        assertThat(homeDirectory).endsWith(".apphomeintegrationtest");
    }
}
