package au.net.causal.shoelaces.apphome.it;

import au.net.causal.shoelaces.testing.JerseyServerEndpointExtension;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.MediaType;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;

import static org.assertj.core.api.Assertions.*;

class CustomAppHomeIT
{
    @RegisterExtension
    static JerseyServerEndpointExtension server = new JerseyServerEndpointExtension()
                                                        .useLogin("admin", "admin")
                                                        .usePath("/mycontext/api");

    private WebTarget applicationNameTarget()
    {
        return server.target("applicationName");
    }

    @Test
    void applicationNameIsDerivedFromWarContext()
    {
        String applicationName = applicationNameTarget().request(MediaType.TEXT_PLAIN_TYPE)
                                                        .get(String.class);

        //Application name is derived from the WAR context name
        assertThat(applicationName).isEqualTo("mycontext");
    }
}
