package au.net.causal.shoelaces.jerseyclient.it;

import au.net.causal.shoelaces.testing.JerseyServerEndpointExtension;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.*;

class RestResourcesIT
{
    @RegisterExtension
    static JerseyServerEndpointExtension server = new JerseyServerEndpointExtension()
                                                        .useLogin("admin", "admin")
                                                        .usePath("/api");

    private WebTarget stuffTarget()
    {
        return server.target("stuff");
    }

    private WebTarget jerseyClientTarget()
    {
        return server.target("stuffWithJerseyClient");
    }
    
    private WebTarget someDateClientTarget()
    {
        return server.target("someDateWithJerseyClient");
    }

    @Test
    void simpleJerseyCall()
    {
        List<String> results = stuffTarget().request(MediaType.APPLICATION_JSON_TYPE)
                                            .get(new GenericType<List<String>>(){});
        assertThat(results).containsExactly("one", "two", "three");
    }

    @Test
    void jerseyClientOnServerIsWorking()
    {
        List<String> results = jerseyClientTarget().request(MediaType.APPLICATION_JSON_TYPE)
                                                   .get(new GenericType<List<String>>(){});
        assertThat(results).containsExactly("ONE", "TWO", "THREE");
    }

    /**
     * The client's object mapper on the server is being configured correctly by running JerseyClientObjectMapperConfigurers.
     * The server is using a client to call back onto itself using another endpoint.  This tests the client that is configured
     * through Spring on the server is actually configured correctly.  In this test we configure the Jersey client's object 
     * mapper to send dates as timestamps and not to send nanoseconds of timestamps (which is not the default setting).  
     * If the configuration doesn't work, nanoseconds will still be sent through and received in the test.  If it does work, 
     * nanoseconds should be truncated.
     */
    @Test
    void clientObjectMapperCustomizationOnServer() 
    {
        String result = someDateClientTarget().request(MediaType.APPLICATION_JSON_TYPE).get(String.class);
        
        //No nanos=19 should be present
        assertThat(result).isEqualTo("1977-01-09T07:05:17");
    }
}
