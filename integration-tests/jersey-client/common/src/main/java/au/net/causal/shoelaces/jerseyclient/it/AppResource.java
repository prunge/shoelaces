package au.net.causal.shoelaces.jerseyclient.it;

import au.net.causal.shoelaces.jerseyclient.JerseyClientFactory;
import au.net.causal.shoelaces.jerseyclient.JerseyClientFactory.CloseableClient;
import jakarta.ws.rs.ClientErrorException;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.UriInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Component
@Path("/")
public class AppResource
{
    @Autowired
    private JerseyClientFactory jerseyClientFactory;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("stuff")
    public List<String> getStuff()
    {
        return Arrays.asList("one", "two", "three");
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("stuffWithJerseyClient")
    public List<String> getStuffWithJerseyClient(@Context UriInfo uriInfo)
    {
        //Call back onto ourself using Jersey client
        try (CloseableClient c = jerseyClientFactory.newClient())
        {
            URI uri = uriInfo.getRequestUri().resolve("stuff");
            List<String> results = c.client().target(uri)
                                    .request(MediaType.APPLICATION_JSON_TYPE)
                                    .get(new GenericType<List<String>>(){});
            return results.stream()
                          .map(s -> s.toUpperCase(Locale.ROOT))
                          .collect(Collectors.toList());
        }
        catch (ClientErrorException e)
        {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage(), e);
        }
    }
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("someDate")
    public LocalDateTime getSomeDate(GetSomeDateRequest input)
    {
        //We are passing back seconds and nanoseconds of the input time
        return LocalDateTime.of(1977, 1, 9, 7, 5, input.getInput().getSecond(), input.getInput().getNano());
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("someDateWithJerseyClient")
    public String getSomeDateWithJerseyClient(@Context UriInfo uriInfo)
    {
        //Call back onto ourself using Jersey client
        try (CloseableClient c = jerseyClientFactory.newClient())
        {
            URI uri = uriInfo.getRequestUri().resolve("someDate");
            GetSomeDateRequest request = new GetSomeDateRequest();
            
            //Because of the way the client is configured (WRITE_DATE_TIMESTAMPS_AS_NANOSECONDS is disabled), 
            //the 19 nanoseconds are going to be truncated
            //This means the date we get back will have the server's date, with seconds set as 17, but no nanos
            request.setInput(LocalDateTime.of(2010, 9, 3, 13, 15, 17, 19));
            return c.client().target(uri)
                             .request(MediaType.APPLICATION_JSON_TYPE)
                             .post(Entity.json(request), LocalDateTime.class)
                             .format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
        }
        catch (ClientErrorException e)
        {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    private static class GetSomeDateRequest
    {
        private LocalDateTime input;

        public LocalDateTime getInput()
        {
            return input;
        }

        public void setInput(LocalDateTime input)
        {
            this.input = input;
        }
    }
}
