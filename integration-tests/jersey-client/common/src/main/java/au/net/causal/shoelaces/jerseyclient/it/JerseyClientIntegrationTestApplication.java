package au.net.causal.shoelaces.jerseyclient.it;

import au.net.causal.shoelaces.jerseyclient.JerseyClientConfigurer;
import au.net.causal.shoelaces.jerseyclient.JerseyClientObjectMapperConfigurer;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@SpringBootApplication
public class JerseyClientIntegrationTestApplication extends SpringBootServletInitializer
{
    public static void main(String[] args)
    {
        SpringApplication.run(JerseyClientIntegrationTestApplication.class, args);
    }

    /**
     * Truncate nanoseconds from Jersey client timestamps when we are sending requests.
     */
    @Bean
    public JerseyClientObjectMapperConfigurer objectMapperConfigurer()
    {
        return objectMapper ->
        {
            objectMapper.enable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
            objectMapper.disable(SerializationFeature.WRITE_DATE_TIMESTAMPS_AS_NANOSECONDS);
        };
    }

    @Bean
    public JerseyClientConfigurer configureJerseyClientCredentials(SecurityProperties securityProperties)
    {
        return client -> client.register(HttpAuthenticationFeature.basicBuilder()
                                                                  .credentials(securityProperties.getUser().getName(),
                                                                               securityProperties.getUser().getPassword())
                                                                  .build());
    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http)
    throws Exception
    {
        http.csrf(c ->
        {
            c.disable();
        });
        http.authorizeHttpRequests(r ->
        {
            r.requestMatchers(new AntPathRequestMatcher("/api/**")).authenticated()
             .anyRequest().permitAll();
        });
        http.httpBasic(c -> {});

        return http.build();
    }
}
