# Shoelaces Liquibase module

Makes Liquibase work better with a Derby embedded database by making
Liquibase not close the connection after use.

### Why is this needed?

By default, Liquibase closes its database connection after use (which 
happens when setting up the schema when an application starts up).  When
running with a Derby embedded database, this will make the database 
shut down.  While not critical, it generates warnings when the
connection is used later.

The Shoelaces Liquibase module overrides the Derby database adapter
Liquibase uses, making Liquibase _not_ shut down embedded Derby
databases.

Most recent versions of Spring boot (and its Hibernate version) seem
to no longer show warnings after Liquibase has closed the embedded
database.  So this module's only use is to avoid shutting down 
Derby embedded databases needlessly.

### Usage

Add the Shoelaces Liquibase module dependency to your application.  If
Liquibase is enabled in your application, everything should work
correctly.  When using a Derby embedded database, there should no longer
be any warnings about using connections.
