package au.net.causal.shoelaces.liquibase;

import liquibase.database.core.DerbyDatabase;
import liquibase.exception.DatabaseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A custom Derby database subclass for Liquibase that does not shut down embedded Derby database instances.
 * I don't see a good reason why it should be shut down, and can throw warning exceptions (though non-fatal) when
 * Hibernate's auto-DDL generation happens and tries to reopen connections.
 */
public class NonClosingDerbyDatabase extends DerbyDatabase
{
    private static final Logger log = LoggerFactory.getLogger(NonClosingDerbyDatabase.class);

    @Override
    public void close()
    throws DatabaseException
    {
        log.debug("NOT closing Derby database from Liquibase - this was a configured override");
    }

    @Override
    public int getPriority()
    {
        //Must be higher priority than the original DerbyDatabase
        return super.getPriority() + 1;
    }
}
