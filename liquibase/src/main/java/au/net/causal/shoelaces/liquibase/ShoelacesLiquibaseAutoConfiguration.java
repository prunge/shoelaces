package au.net.causal.shoelaces.liquibase;

import liquibase.change.DatabaseChange;
import liquibase.database.DatabaseFactory;
import liquibase.integration.spring.SpringLiquibase;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseAutoConfiguration;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseAutoConfiguration.LiquibaseConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.EntityManagerFactoryDependsOnPostProcessor;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.orm.jpa.AbstractEntityManagerFactoryBean;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;

import javax.sql.DataSource;

@AutoConfiguration
@AutoConfigureBefore({LiquibaseAutoConfiguration.class, LiquibaseConfiguration.class})
@ConditionalOnClass({SpringLiquibase.class, DatabaseChange.class })
@ConditionalOnBean({DataSource.class, EmbeddedDatabase.class})
@ConditionalOnProperty(prefix = "spring.liquibase", name = "enabled", matchIfMissing = true)
@AutoConfigureAfter({DataSourceAutoConfiguration.class,
                     HibernateJpaAutoConfiguration.class })
public class ShoelacesLiquibaseAutoConfiguration implements InitializingBean
{
    //Depend on the embedded database bean purely so we eagerly set up the database (including Derby log hacks)
    //before this bean runs its stuff
    public ShoelacesLiquibaseAutoConfiguration(EmbeddedDatabase database)
    {
    }

    @Override
    public void afterPropertiesSet()
    throws Exception
    {
        //This needs to run after the Derby log hacks are run since it loads the Derby driver
        DatabaseFactory.getInstance().register(new NonClosingDerbyDatabase());
    }

    /**
     * Purely here so we can register a dependent bean on JPA and have our configuration come in earlier than Liquibase's.
     */
    @Bean
    public String liquibaseNonClosingDerbyDatabasePreBean()
    {
        return "";
    }

    /**
     * See {@code LiquibaseAutoConfiguration.LiquibaseJpaDependencyConfiguration} for why we need this in its
     * complicated form.  Liquibase's JPA configuration registers a post-processor that kicks in much earlier, which
     * makes AutoConfigureBefore not work (it's only a hint anyway).  We register our own post-processor, make it
     * ordered since Spring will always run ordered post-processors before unordered ones (Liquibase's is unordered),
     * and register a fake bean (need at least one) so that the post-processor actually works.
     */
    @Configuration
    @ConditionalOnClass(LocalContainerEntityManagerFactoryBean.class)
    @ConditionalOnBean(AbstractEntityManagerFactoryBean.class)
    protected static class LiquibaseJpaDependencyConfiguration
    extends EntityManagerFactoryDependsOnPostProcessor
    implements Ordered
    {
        public LiquibaseJpaDependencyConfiguration()
        {
            super("liquibaseNonClosingDerbyDatabasePreBean");
        }

        @Override
        public int getOrder()
        {
            //Any order is fine - all Ordered post processors are run before non-ordered ones
            //Liquibase's one is not ordered, all we care about is registering before that one
            return 0;
        }
    }
}
