# Shoelaces AppHome module

This module allows a Spring Boot application to gain the concept of an
application home directory and have one configured with an appropriate
name at runtime.  The application home directory allows users to 
easily configure your application and provides a common storage space
for your application for any additional files. 

### What can the Application Home directory be used for?

- An `application.properties` or `application.yaml` file in this directory can set
Spring configuration options that the user wants to change.
- Any other files specific to the application necessary for running that
are not bundled inside the application can be placed in this  directory.
For example: embedded database files, Lucene indexes.

### Naming and location of the Application Home directory

Shoelaces will choose an appropriate name based on the application:

- The `application.home.directory` property will be used if configured.
This may be configured by system property, command line option, or 
as a property inside the _application-bundled_ `application.properties`
file.
- Otherwise, the directory will be named `.{application name}` 
underneath the `user.home` directory.

`application name` is calculated by the following:

- If configured, use the `application.name` property.  Can be configured
through a system property, command line option or as a property inside 
the _application-bundled_ `application.properties` file.
- If running as a WAR in a Java EE container, use the context path of
the application.  For example, if the WAR is configured to serve
underneath the `/my-application` context, the application name will 
become `my-application` and the default application home directory will
be `~/.my-application/`.
- Otherwise, the application name will be calculated from the Spring
Boot application class (the one annotated with `@SpringBootApplication`), 
trimming off package name and any `application` prefix or suffix. 
For example, if the main class is called `SuperDooperApplication` then
the application name becaomes `superdooper` and the default application
home directory will be `~/.superdooper/`.
- If the main class is just called `Application`, then use the last 
segment of the package name.  For example, if the main class is 
`com.mycompany.golden.Application` then the application name becomes
`golden` and the default application home directory will be 
`~/.golden/`.
- Finally, if everything else fails (such as when the main class is 
called `Application` and is in the default package) the application name
becomes `spring-boot-app`.

If desired, this automatic naming can be configured by subclassing
`ApplicationHomeDirectoryRunListener` and overriding 
`calculateConfigurationDirectory()` and/or `calculateApplicationName()`.

### How to use this module

#### Basic usage

- Add the `shoelaces-apphome` dependency to your project.  For Maven
users, that will involve adding an appropriate `<dependency>` element
to your POM.

This will allow users to customize your application
by adding an `application.properties` to your application home
directory.

#### Programmatically accessing application home

Inject a `au.net.causal.shoelaces.apphome.ApplicationHome` bean to 
gain access to additional functionality such as reading the 
application home directory and application name.  This can be used to 
read and/or write additional files to this directory.

Example:

```
@Autowired
private ApplicationHome appConfig;
```

```
Path dataFile = appConfig.getApplicationHomeDirectory().resolve("mydata.txt");
Files.writeString(dataFile "My data file");
```
