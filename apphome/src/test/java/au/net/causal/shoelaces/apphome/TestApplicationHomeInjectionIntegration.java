package au.net.causal.shoelaces.apphome;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.nio.file.Path;
import java.nio.file.Paths;

import static org.assertj.core.api.Assertions.*;

/**
 * Integration tests apphome injection.
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = TestApplicationHomeInjectionIntegration.ReallySimpleApp.class)
class TestApplicationHomeInjectionIntegration
{
    @Autowired
    private ApplicationHome appHome;

    private Path userHome = Paths.get(System.getProperty("user.home", ".")).toAbsolutePath();

    @BeforeEach
    void verifySetupBeanInjectedBySpring()
    {
        assertThat(appHome).as("setup bean should be injected by Spring").isNotNull();
    }

    @Test
    void applicationNameIsConfigured()
    {
        //Our test class is the application, so this will be the name - yeah it's long
        assertThat(appHome.getApplicationName()).isEqualTo("testapplicationhomeinjectionintegration");
    }

    @Test
    void applicationHomeDirectoryIsConfigured()
    {
        //Our test class is the application, so this will be the name - yeah it's long
        assertThat(appHome.getApplicationHomeDirectory())
                .isEqualTo(userHome.resolve(".testapplicationhomeinjectionintegration"));
    }

    @SpringBootApplication
    public static class ReallySimpleApp
    {

    }
}
