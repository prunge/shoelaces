package au.net.causal.shoelaces.apphome;

import jakarta.servlet.ServletContext;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.boot.SpringApplication;
import org.springframework.core.env.PropertySource;
import org.springframework.mock.env.MockEnvironment;
import org.springframework.mock.web.MockServletContext;
import org.springframework.web.context.support.ServletContextPropertySource;
import org.springframework.web.context.support.StandardServletEnvironment;

import java.nio.file.Path;
import java.nio.file.Paths;

import static org.assertj.core.api.Assertions.assertThat;

class TestApplicationHomeDirectoryRunListener
{
    private Path userHome = Paths.get(System.getProperty("user.home", ".")).toAbsolutePath();
    private SpringApplication application = new SpringApplication();
    private MockEnvironment environment = new MockEnvironment()
                                            .withProperty("user.home", userHome.toString());

    @Nested
    class ApplicationName
    {
        @Test
        void fromClassNamePlain()
        {
            class Galah {} //Just for the application name
            application.setMainApplicationClass(Galah.class);
            ApplicationHomeDirectoryRunListener listener = new ApplicationHomeDirectoryRunListener(application, new String[0]);

            String applicationName = listener.calculateApplicationName(environment);

            assertThat(applicationName).isEqualTo("galah");
        }

        @Test
        void fromClassNameSuffix()
        {
            class CockatooApplication {} //Just for the application name
            application.setMainApplicationClass(CockatooApplication.class);
            ApplicationHomeDirectoryRunListener listener = new ApplicationHomeDirectoryRunListener(application, new String[0]);

            String applicationName = listener.calculateApplicationName(environment);

            assertThat(applicationName).isEqualTo("cockatoo");
        }

        @Test
        void fromClassNamePrefix()
        {
            class ApplicationIbis {} //Just for the application name
            application.setMainApplicationClass(ApplicationIbis.class);
            ApplicationHomeDirectoryRunListener listener = new ApplicationHomeDirectoryRunListener(application, new String[0]);

            String applicationName = listener.calculateApplicationName(environment);

            assertThat(applicationName).isEqualTo("ibis");
        }

        /**
         * For application class called 'Application', use the last segment of the package name.
         */
        @Test
        void fromClassNamePackage()
        {
            class Application {} //Just for the application name
            application.setMainApplicationClass(Application.class);
            ApplicationHomeDirectoryRunListener listener = new ApplicationHomeDirectoryRunListener(application, new String[0]);

            String applicationName = listener.calculateApplicationName(environment);

            assertThat(applicationName).isEqualTo("apphome"); //This is the last segment of the package name of this test class
        }

        @Test
        void fromServletContextPath()
        {
            ApplicationHomeDirectoryRunListener listener = new ApplicationHomeDirectoryRunListener(application, new String[0]);
            MockServletContext servletContext = new MockServletContext();
            servletContext.setContextPath("/galah-app");
            PropertySource<ServletContext> servletSource = new ServletContextPropertySource(StandardServletEnvironment.SERVLET_CONTEXT_PROPERTY_SOURCE_NAME, servletContext);
            environment.getPropertySources().addLast(servletSource);

            String applicationName = listener.calculateApplicationName(environment);

            assertThat(applicationName).isEqualTo("galah-app");
        }

        @Test
        void fromServletContextPathWithSlashes()
        {
            ApplicationHomeDirectoryRunListener listener = new ApplicationHomeDirectoryRunListener(application, new String[0]);
            MockServletContext servletContext = new MockServletContext();
            servletContext.setContextPath("/some/app");
            PropertySource<ServletContext> servletSource = new ServletContextPropertySource(StandardServletEnvironment.SERVLET_CONTEXT_PROPERTY_SOURCE_NAME, servletContext);
            environment.getPropertySources().addLast(servletSource);

            String applicationName = listener.calculateApplicationName(environment);

            assertThat(applicationName).isEqualTo("some_app");
        }

        @Test
        void skippingEmptyServletContextPath()
        {
            class MyApp {} //Just for the application name
            application.setMainApplicationClass(MyApp.class);
            ApplicationHomeDirectoryRunListener listener = new ApplicationHomeDirectoryRunListener(application, new String[0]);
            MockServletContext servletContext = new MockServletContext();
            servletContext.setContextPath("/");
            PropertySource<ServletContext> servletSource = new ServletContextPropertySource(StandardServletEnvironment.SERVLET_CONTEXT_PROPERTY_SOURCE_NAME, servletContext);
            environment.getPropertySources().addLast(servletSource);

            String applicationName = listener.calculateApplicationName(environment);

            assertThat(applicationName).isEqualTo("myapp");
        }

        @Test
        void explicitlySpecified()
        {
            environment.setProperty(ApplicationHomeDirectoryRunListener.APPLICATION_NAME_PROPERTY, "my-specified-app");
            ApplicationHomeDirectoryRunListener listener = new ApplicationHomeDirectoryRunListener(application, new String[0]);

            String applicationName = listener.calculateApplicationName(environment);

            assertThat(applicationName).isEqualTo("my-specified-app");
        }
    }

    @Nested
    class ApplicationConfigurationDirectory
    {
        @Test
        void explicitlySpecified()
        {
            Path dir = userHome.resolve("testtemp").toAbsolutePath();
            environment.setProperty(ApplicationHomeDirectoryRunListener.APPLICATION_HOME_DIRECTORY_PROPERTY, dir.toString());

            ApplicationHomeDirectoryRunListener listener = new ApplicationHomeDirectoryRunListener(application, new String[0]);
            Path configurationDirectory = listener.calculateConfigurationDirectory("appname", environment);

            assertThat(configurationDirectory).isEqualTo(dir);
        }

        @Test
        void calculatedFromApplicationName()
        {
            ApplicationHomeDirectoryRunListener listener = new ApplicationHomeDirectoryRunListener(application, new String[0]);
            Path configurationDirectory = listener.calculateConfigurationDirectory("galah-app", environment);

            assertThat(configurationDirectory).isEqualTo(userHome.resolve(".galah-app"));
        }
    }
}
