open module au.net.causal.shoelaces.apphome
{
    exports au.net.causal.shoelaces.apphome;

    requires spring.core;
    requires spring.beans;
    requires spring.boot;
    requires spring.context;
    requires spring.web;
    requires spring.boot.autoconfigure;

    requires static jakarta.servlet;
}
