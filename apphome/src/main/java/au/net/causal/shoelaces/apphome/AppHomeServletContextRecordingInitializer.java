package au.net.causal.shoelaces.apphome;

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import org.springframework.core.annotation.Order;
import org.springframework.web.WebApplicationInitializer;

/**
 * Spring Boot after a particular 2.x version does not allow access to the servlet context from the run listener (at the stage it is called its property source is still a stub)
 * so we record the context path here as well so we can access it from there.
 */
@Order(-10) //Must be higher than SpringBootServletInitializer
public class AppHomeServletContextRecordingInitializer implements WebApplicationInitializer
{
    static String lastSeenContextPath;

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException
    {
        lastSeenContextPath = servletContext.getContextPath();
    }
}
