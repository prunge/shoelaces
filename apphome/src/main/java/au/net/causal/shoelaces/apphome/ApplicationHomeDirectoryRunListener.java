package au.net.causal.shoelaces.apphome;

import jakarta.servlet.ServletContext;
import org.springframework.boot.ConfigurableBootstrapContext;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringApplicationRunListener;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.PropertiesPropertySource;
import org.springframework.core.env.PropertySource;
import org.springframework.web.context.support.StandardServletEnvironment;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Locale;
import java.util.Optional;
import java.util.Properties;

/**
 * Sets up application name and config directory properties early in the Spring lifecycle.
 * <p>
 *
 * This needs to run very early in the application lifecycle so that application name and home directory is configured before logging is configured, since
 * logging configuration may be read from the application home directory.
 */
@Order(-10) //Less than EventPublishingRunListener's order of 0
public class ApplicationHomeDirectoryRunListener implements SpringApplicationRunListener
{
    /**
     * System property name that can be used to override the automatically generated application name.
     */
    public static final String APPLICATION_NAME_PROPERTY = "application.name";

    /**
     * System property name that can be used to override the application home directory.
     */
    public static final String APPLICATION_HOME_DIRECTORY_PROPERTY = "application.home.directory";
    
    private final SpringApplication application;

    /**
     * Requires application signature for run listeners.
     */
    public ApplicationHomeDirectoryRunListener(SpringApplication application, String[] args)
    {
        this.application = application;
    }
    
    @Override
    public void environmentPrepared(ConfigurableBootstrapContext bootstrapContext,
                                    ConfigurableEnvironment environment)
    {
        String applicationName = calculateApplicationName(environment);
        Path applicationConfigurationDirectory = calculateConfigurationDirectory(applicationName, environment);
        
        Properties configProps = new Properties();
        configProps.setProperty(APPLICATION_NAME_PROPERTY, applicationName);
        configProps.setProperty(APPLICATION_HOME_DIRECTORY_PROPERTY, applicationConfigurationDirectory.toString());
        environment.getPropertySources().addLast(new PropertiesPropertySource("apphome", configProps));
    }

    /**
     * Calculates the application home directory based on the application name and any overrides present in the
     * environment.
     * <p>
     *
     * If the application home is overridden by property in the environment, then that is used.  Otherwise
     * a directory named <code>.[application name]</code> underneath the user home directory is returned.
     *
     * @param applicationName the application name, either calculated or configured.
     * @param environment environment which may contain overrides.
     *
     * @return application home directory.
     */
    protected Path calculateConfigurationDirectory(String applicationName, ConfigurableEnvironment environment)
    {
        //Can be explicitly defined, if so use that
        String configDirStr = environment.getProperty(APPLICATION_HOME_DIRECTORY_PROPERTY);
        if (configDirStr != null)
            return Paths.get(configDirStr);
        
        String userHomeStr = environment.getProperty("user.home");
        if (userHomeStr == null)
            userHomeStr = ".";
        
        Path userHome = Paths.get(userHomeStr);
        Path configDir = userHome.resolve("." + applicationName);
        configDir = configDir.toAbsolutePath();
        
        return configDir;
    }

    /**
     * Calculates the application name.
     *
     * @param environment environment where the application name may have been explicitly configured.
     *
     * @return the application name.
     */
    protected String calculateApplicationName(ConfigurableEnvironment environment)
    {
        //If explicitly defined already, don't calculate
        String applicationName = environment.getProperty(APPLICATION_NAME_PROPERTY);
        if (applicationName != null)
            return applicationName;
        
        return applicationNameFromServletContextPath(environment)
                .or(this::applicationNameFromApplicationClass)
                .orElse("spring-boot-app"); //Default if nothing else works
    }

    /**
     * Finds the main Spring Boot application class and attempts to calculate an appropriate application name
     * from it.
     * <p>
     *
     * Will try to take the simple name of the application class, strip any 'Application' prefix or suffix and use that.
     * If that results in an empty string, the last segment of the application class's package name is used.
     * If the application class is in the default package, no value is returned.
     *
     * @return calculated application name.
     */
    private Optional<String> applicationNameFromApplicationClass()
    {
        Class<?> mainApplicationClass = application.getMainApplicationClass();
        if (mainApplicationClass == null)
            return Optional.empty();

        String mainName = mainApplicationClass.getSimpleName().toLowerCase(Locale.ENGLISH);

        //Remove 'Application' prefix or suffix
        if (mainName.endsWith("application"))
            mainName = mainName.substring(0, mainName.length() - "application".length());
        if (mainName.startsWith("application"))
            mainName = mainName.substring("application".length());

        if (!mainName.isEmpty())
            return Optional.of(mainName);

        //If the application class is just called 'Application' then use the last segment of the package name
        String packageName = application.getMainApplicationClass().getPackageName();
        int lastDotIndex = packageName.lastIndexOf('.');
        if (lastDotIndex >= 0)
            return Optional.of(packageName.substring(lastDotIndex + 1));

        return Optional.empty();
    }

    /**
     * Calculate from servlet context path.
     * Only works when running WAR in a container, not as a standalone.
     * When standalone, property source is still a stub so there's no servlet context available
     */
    private Optional<String> applicationNameFromServletContextPath(ConfigurableEnvironment environment)
    {
        PropertySource<?> propertySource = environment.getPropertySources().get(StandardServletEnvironment.SERVLET_CONTEXT_PROPERTY_SOURCE_NAME);
        if (propertySource == null)
            return Optional.empty();

        Object servletContextSource = propertySource.getSource();
        String contextPath;

        //This might still be a stub in some versions of Spring boot, so we have a fallback below
        if (servletContextSource instanceof ServletContext)
        {
            ServletContext servletContext = (ServletContext)servletContextSource;
            contextPath = servletContext.getContextPath();
        }
        else //Fall back to trying this - a bit of a hack but a last resort for accessing the servlet context
            contextPath = AppHomeServletContextRecordingInitializer.lastSeenContextPath;

        if (contextPath != null)
        {
            //Remove leading/trailing '/' characters
            while (contextPath.startsWith("/"))
            {
                contextPath = contextPath.substring(1);
            }
            while (contextPath.endsWith("/"))
            {
                contextPath = contextPath.substring(0, contextPath.length() - 1);
            }

            //Translate '/' characters into '_'
            contextPath = contextPath.replace('/', '_');

            if (!contextPath.isEmpty())
                return Optional.of(contextPath);
        }

        return Optional.empty();
    }
}
