package au.net.causal.shoelaces.apphome;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.context.config.ConfigDataEnvironmentPostProcessor;
import org.springframework.boot.env.EnvironmentPostProcessor;
import org.springframework.boot.env.PropertySourceLoader;
import org.springframework.core.Ordered;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.PropertySource;
import org.springframework.core.io.PathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.SpringFactoriesLoader;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Adds configuration for any application configuration files in the application home directory.
 * Files named 'application.properties', 'application.yaml' are used.  This configuration takes precedence over application
 * classpath configuration, but is overridden by system properties and environment variables.
 * <p>
 *
 * All property source loaders configured in Spring will be used for loading properties.  By default, this allows loading properties and YAML files.
 *
 * @since 2.2
 */
public class AppHomeEnvironmentPostProcessor implements EnvironmentPostProcessor, Ordered
{
    //Default order is before config's so that our properties take preference over classpath ones
    //(config adds its classpath one after us because it runs later)
    private int order = ConfigDataEnvironmentPostProcessor.ORDER - 1;

    /**
     * The base name of the configuration files.  e.g. {@value}.properties, {@value}.yaml.
     */
    private String configurationFileName = "application";

    @Override
    public void postProcessEnvironment(ConfigurableEnvironment environment, SpringApplication application)
    {
        ApplicationHome applicationHome = ApplicationHome.fromEnvironment(environment);

        //For any application.properties/yml/etc. add property source
        Collection<? extends PropertySource<?>> propertySources = readApplicationHomeConfig(applicationHome);
        for (PropertySource<?> propertySource : propertySources)
        {
            environment.getPropertySources().addLast(propertySource);
        }
    }

    /**
     * The base name of the configuration files.  Defaults to 'application' so that files such as 'application.properties' and
     * 'application.yaml' will be read.
     */
    public String getConfigurationFileName()
    {
        return configurationFileName;
    }

    public void setConfigurationFileName(String configurationFileName)
    {
        this.configurationFileName = configurationFileName;
    }

    @Override
    public int getOrder()
    {
        return order;
    }

    public void setOrder(int order)
    {
        this.order = order;
    }

    /**
     * Uses all registered property source loaders to construct property sources for files that are found in the application home directory.
     *
     * @param applicationHome the application home whose directory will be used for loading configurationf files.
     *
     * @return a property source for each loadable configuration file in the application home directory.
     */
    private Collection<? extends PropertySource<?>> readApplicationHomeConfig(ApplicationHome applicationHome)
    {
        Path applicationConfigurationDirectory = applicationHome.getApplicationHomeDirectory();

        List<PropertySource<?>> propertySources = new ArrayList<>();

        List<PropertySourceLoader> propertySourceLoaders = SpringFactoriesLoader.loadFactories(PropertySourceLoader.class, getClass().getClassLoader());
        for (PropertySourceLoader propertySourceLoader : propertySourceLoaders)
        {
            for (String extension : propertySourceLoader.getFileExtensions())
            {
                //Attempt to find resource
                String fileName = getConfigurationFileName() + "." + extension;
                Path configFile = applicationConfigurationDirectory.resolve(fileName);
                if (Files.isRegularFile(configFile))
                {
                    Resource resource = new PathResource(configFile);
                    try
                    {
                        List<PropertySource<?>> sources = propertySourceLoader.load("apphome" + extension, resource);
                        propertySources.addAll(sources);
                    }
                    catch (IOException e)
                    {
                        throw new RuntimeException("Could not read " + configFile + ": " + e.getMessage(), e);
                    }
                }
            }
        }

        return propertySources;
    }
}
