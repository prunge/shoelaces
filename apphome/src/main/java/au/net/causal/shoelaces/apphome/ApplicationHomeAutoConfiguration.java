package au.net.causal.shoelaces.apphome;

import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.ConfigurableEnvironment;

/**
 * Configures an {@code ApplicationHome} bean to be available for injection.
 *
 * @since 2.2
 */
@AutoConfiguration
@EnableConfigurationProperties(AppHomeProperties.class)
@ConditionalOnMissingBean(ApplicationHome.class)
public class ApplicationHomeAutoConfiguration
{
    @Bean
    public ApplicationHome applicationHome(ConfigurableEnvironment environment)
    {
        return ApplicationHome.fromEnvironment(environment);
    }
}
