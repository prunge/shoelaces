package au.net.causal.shoelaces.apphome;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.nio.file.Path;

/**
 * Application home module properties.  This class is mostly used for hinting properties editors - the {@link ApplicationHome} bean should be used
 * to reading these values in applications.
 */
@ConfigurationProperties(prefix="application")
public class AppHomeProperties
{
    /**
     * The application name is used for determining the application home directory.  It is derived from either the WAR context if running under a container, or
     * the name of the application class if not specified.
     */
    private String name;
    
    private Home home = new Home();

    public Home getHome()
    {
        return home;
    }

    public void setHome(Home home)
    {
        this.home = home;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public static class Home
    {
        /**
         * The directory where application configuration and data is stored.  By default, this is derived from the application name, is named 
         * .[applicationName] and sits under the user's home directory.  This is not typically overridden in application.properties but users might override this
         * value from the command line.
         */
        private Path directory;

        public Path getDirectory()
        {
            return directory;
        }

        public void setDirectory(Path directory)
        {
            this.directory = directory;
        }
  }
}
