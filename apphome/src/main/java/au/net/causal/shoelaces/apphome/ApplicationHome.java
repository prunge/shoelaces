package au.net.causal.shoelaces.apphome;

import org.springframework.boot.context.properties.bind.Binder;
import org.springframework.core.env.Environment;

import java.nio.file.Path;
import java.util.Objects;

/**
 * Used to access the application home directory and the application name.  These may be either configured
 * by the user or generated.  To use, inject a bean of this type into your components.
 * <p>
 *
 * The application home directory can be used to load and store files that are persisted between application
 * restarts.
 */
public class ApplicationHome
{
    private final String applicationName;
    private final Path applicationHomeDirectory;
    
    public ApplicationHome(String applicationName, Path applicationHomeDirectory)
    {
        this.applicationName = Objects.requireNonNull(applicationName);
        this.applicationHomeDirectory = Objects.requireNonNull(applicationHomeDirectory);
    }

    public ApplicationHome(AppHomeProperties appHomeProperties)
    {
        this(appHomeProperties.getName(), appHomeProperties.getHome().getDirectory());
    }

    /**
     * Gets an application home from a Spring environment.  This is only needed in special cases, such as when
     * application home needs to be acquired before Spring Boot has fully initialized.
     * <p>
     *
     * For normal usage, don't use this method but inject an <code>ApplicationHome</code> instance using standard
     * Spring dependency injection instead.
     *
     * @param environment Spring environment.
     *
     * @return application home.
     */
    public static ApplicationHome fromEnvironment(Environment environment)
    {
        AppHomeProperties appHomeProperties = Binder.get(environment)
                                                    .bind("application", AppHomeProperties.class)
                                                    .orElseThrow(() -> new IllegalStateException("application home properties not configured, did the initializer run?"));

        return new ApplicationHome(appHomeProperties);
    }

    /**
     * @return the name of the application.  This may be derived from the web context path or
     *         from the main Spring Boot application class or package name.
     */
    public String getApplicationName()
    {
        return applicationName;
    }

    /**
     * @return the application home directory.
     */
    public Path getApplicationHomeDirectory()
    {
        return applicationHomeDirectory;
    }
}
