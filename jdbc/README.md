# Shoelaces JDBC module

Configures a disk-based persistent embedded database for your 
application.  

The Shoelaces JDBC module is handy for use at development time and for 
small deployments where a larger dedicated database is unneeded.
You get a persistent disk-based database with zero configuration!
When it comes time to use another non-embedded dedicated database such
as Postgres, a few properties can be configured in 
`application.properties` under the application's home directory
(which is external to the application).  

In summary, users of your application get persistent database storage
from an embedded database until they want to switch to something else, 
which they can do by setting a few properties in a file.

### Basic usage

- Add the `shoelaces-jdbc` dependency to your project.  For Maven
users, that will involve adding an appropriate `<dependency>` element
to your POM.
- Add an embedded database dependency to your project.  This will
be either:
    - H2 ([com.h2database:h2](https://search.maven.org/search?q=g:com.h2database%20AND%20a:h2&core=gav)) 
    - Apache Derby ([org.apache.derby:derby](https://search.maven.org/search?q=g:org.apache.derby%20AND%20a:derby&core=gav)) 
    - HSQL-DB ([org.hsqldb:hsqldb](https://search.maven.org/search?q=g:org.hsqldb%20AND%20a:hsqldb&core=gav))

### Configuration

#### Embedded database type

Shoelaces supports these embedded database types:

- [H2](http://www.h2database.com)
- [Apache Derby](https://db.apache.org/derby/)
- [HSQL-DB](http://hsqldb.org/)

The first of these databases found on the classpath will be used.

#### Location of database data

By default, the database data is stored in a directory (defaults to `db`
under the [application home](../apphome/README.md) directory.  The name 
of the database directory can be customized by setting the 
`shoelaces.datasource.database-directory-name` application property 
(which can be done through command line by setting a system property,
or overriding it in an `application.properties` file on the 
application's classpath or in the application home).

#### Using an in-memory database instead of a disk-based one

This can be done by setting the `shoelaces.datasource.use-disk-database`
application property to `false`.  

Essentially, this turns off the _Shoelaces_ JDBC module and uses the 
Spring Boot default one instead.  This can be useful when turned on 
temporarily when doing testing.

#### Using a custom/external database instead of Shoelaces' default

- Ensure any required JDBC drivers are on the classpath.  So if you 
want your application to support connecting to Postgres databases, 
add the Postgres JDBC driver JAR as a dependency to your project.
As a developer, you'll need to do this when you build your project.
- Configure the following properties in `application.properties`:
  - `spring.datasource.url`: the JDBC URI of the database
  - `spring.datasource.username`: database username
  - `spring.datasource.password`: database password

  This is typically done by the user of the application by configuring
  the above properties in an `application.properties` file under
  the application home directory (which defaults to 
  `(user.home)/(application name)`).  See the 
  [Application Home module documentation](../apphome/README.md) for
  more details on application home directories.
- Or instead of the previous step, configure an alternative data
source using JDNI or a custom data source bean.  See the 
[Spring Boot JDBC documentation](https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-sql.html#boot-features-connect-to-production-database)
for more information on doing this.

For example, to use a Postgres database named `mydatabase` running on 
`localhost` with username 'finch' and password 'abc123', add the 
[org.postgresql:postgresql](https://search.maven.org/search?q=g:org.postgresql%20AND%20a:postgresql&core=gav)
dependency to your project.  Then add the following configuration in
the `application.properties` file in the application home:

```
spring.datasource.url=jdbc:postgresql://localhost:5432/mydatabase
spring.datasource.username=finch
spring.datasource.password=abc123
```

On a Linux machine running under user `john` and a Spring Boot 
application with a main class named `com.myapp.GalahApplication`, the 
application properties file will be 
`/home/john/.galah/application.properties`.
