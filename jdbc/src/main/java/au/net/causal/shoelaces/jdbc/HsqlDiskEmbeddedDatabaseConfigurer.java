package au.net.causal.shoelaces.jdbc;

import org.hsqldb.jdbc.JDBCDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.datasource.embedded.ConnectionProperties;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseConfigurer;

import javax.sql.DataSource;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Objects;
import java.util.Properties;

/**
 * An alternate implementation to Spring's standard HSQL-DB configurer that stores the database on disk instead of in
 * memory.
 * <p>
 *
 * These are the changes from the standard <code>HsqlEmbeddedDatabaseConfigurer</code> implementation:
 *
 * <ul>
 *     <li>Uses a filesystem directory for storing the database instead of an in-memory H2 database</li>
 *     <li>Shuts down the database cleanly when the application is shutting down</li>
 *     <li>Database user name and password are configurable</li>
 * </ul>
 */
public class HsqlDiskEmbeddedDatabaseConfigurer implements EmbeddedDatabaseConfigurer
{
    private static final Logger log = LoggerFactory.getLogger(HsqlDiskEmbeddedDatabaseConfigurer.class);

    private final Path databaseFile;
    private final String username;
    private final String password;

    /**
     * Constructs an <code>HsqlDiskEmbeddedDatabaseConfigurer</code>.
     *
     * @param databaseDirectory directory to store the database data under.
     * @param username the database username.
     * @param password the database password.
     */
    public HsqlDiskEmbeddedDatabaseConfigurer(Path databaseDirectory, String username, String password)
    {
        this.databaseFile = Objects.requireNonNull(databaseDirectory).resolve(databaseDirectory.getFileName());
        this.username = username;
        this.password = password;
    }

    /**
     * @return the HSQL-DB database driver class.
     */
    public static Class<? extends Driver> driverClass()
    {
        return JDBCDriver.class;
    }

    /**
     * @return the database URI for use when creating HSQL-DB database connections.
     */
    protected String databaseUri()
    {
        return "jdbc:hsqldb:file:" + databaseFile.toString().replace(databaseFile.getFileSystem().getSeparator(), "/");
    }
    
    @Override
    public void configureConnectionProperties(ConnectionProperties properties, String databaseName)
    {
        properties.setDriverClass(driverClass());
        properties.setUrl(databaseUri());
        properties.setUsername(username);
        properties.setPassword(password);
    }

    @Override
    public void shutdown(DataSource dataSource, String databaseName)
    {
        try
        {
            Properties props = new Properties();
            props.setProperty("user", username);
            props.setProperty("password", password);
            try (Connection con = new JDBCDriver().connect(databaseUri(), props);
                 Statement stat = con.createStatement())
            {
                stat.execute("SHUTDOWN");
                log.info("HSQL-DB database " + databaseName + " has been shut down");
            }
        }
        catch (SQLException ex)
        {
            log.warn("Could not shut down embedded HSQL-DB database", ex);
        }
    }
}
