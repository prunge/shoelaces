package au.net.causal.shoelaces.jdbc;

import au.net.causal.shoelaces.apphome.ApplicationHome;
import org.springframework.beans.factory.BeanClassLoaderAware;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionMessage;
import org.springframework.boot.autoconfigure.condition.ConditionOutcome;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.condition.SpringBootCondition;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.autoconfigure.jdbc.EmbeddedDataSourceConfiguration;
import org.springframework.boot.autoconfigure.jdbc.JndiDataSourceAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.context.annotation.Conditional;
import org.springframework.core.type.AnnotatedTypeMetadata;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseFactory;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import javax.sql.DataSource;
import javax.sql.XADataSource;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.SQLException;

/**
 * Configures a database data source.  The following will be attempted to be used to configure the application
 * data source, in order:
 *
 * <ol>
 *     <li>JNDI</li>
 *     <li>Application properties (DataSourceProperties)</li>
 *     <li>Default derby database based on config dir in filesystem (or in-memory if configured)</li>
 * </ol>
 */
@AutoConfiguration
@ConditionalOnClass({DataSource.class, EmbeddedDatabaseType.class })
@AutoConfigureBefore({DataSourceAutoConfiguration.class, EmbeddedDataSourceConfiguration.class})
@AutoConfigureAfter(JndiDataSourceAutoConfiguration.class)
@EnableConfigurationProperties(ShoelacesDataSourceProperties.class)
public class ShoelacesDataSourceAutoConfiguration implements BeanClassLoaderAware, DisposableBean
{
    private ClassLoader classLoader;
    private EmbeddedDatabase database;

    private final DiskEmbeddedDatabaseFactory diskEmbeddedDatabaseFactory = new DiskEmbeddedDatabaseFactory();
    
    @Override
    public void setBeanClassLoader(ClassLoader classLoader)
    {
        this.classLoader = classLoader;
    }

    @Conditional(EmbeddedDatabaseCondition.class)
    @ConditionalOnBean(ApplicationHome.class) //Can't configure disk-based embedded data source if there is no application home to put the files
    @ConditionalOnMissingBean({DataSource.class, XADataSource.class }) //Don't configure if the user has explicitly configured their own data source beans
    @ConditionalOnProperty(prefix = ShoelacesDataSourceProperties.PREFIX, name = "use-disk-database", havingValue = "true", matchIfMissing = true) //Use the default Spring-boot in-memory database if this property is set to false
    @Bean
    public EmbeddedDatabase bootDataSource(ApplicationHome applicationHome, DataSourceProperties dataSourceProperties,
                                           ShoelacesDataSourceProperties shoelacesDataSourceProperties)
    throws IOException, SQLException
    {
        Path databaseDirectory = applicationHome.getApplicationHomeDirectory().resolve(shoelacesDataSourceProperties.getDatabaseDirectoryName());
        Files.createDirectories(databaseDirectory.getParent());

        EmbeddedDatabaseFactory builder = new EmbeddedDatabaseFactory();
        EmbeddedDatabaseConnection embeddedDatabaseConnection = EmbeddedDatabaseConnection.get(this.classLoader);
        builder.setDatabaseType(embeddedDatabaseConnection.getType());
        
        String username = dataSourceProperties.determineUsername();
        String password = dataSourceProperties.determinePassword();
        
        builder.setDatabaseConfigurer(diskEmbeddedDatabaseFactory.create(embeddedDatabaseConnection.getType(), 
                                                                         databaseDirectory, 
                                                                         applicationHome, 
                                                                         username, password));

        this.database = builder.getDatabase();
        return this.database;
    }

    @Override
    public void destroy()
    {
        if (this.database != null)
            this.database.shutdown();
    }

    /**
     * Use an embedded database if spring.datasource.url has not been set and an embedded database is available
     * on the classpath.
     */
    static class EmbeddedDatabaseCondition extends SpringBootCondition
    {
        @Override
        public ConditionOutcome getMatchOutcome(ConditionContext context, AnnotatedTypeMetadata metadata)
        {
            //Data source URL must not be set
            String explicitlySetUrl = context.getEnvironment().getProperty("spring.datasource.url");
            if (explicitlySetUrl != null && !explicitlySetUrl.isEmpty())
                return ConditionOutcome.noMatch("datasource.url has been set");

            //Embedded database must be available
            ConditionMessage.Builder message = ConditionMessage.forCondition("EmbeddedDataSource");
            EmbeddedDatabaseType type = EmbeddedDatabaseConnection.get(context.getClassLoader()).getType();
            if (type == null)
                return ConditionOutcome.noMatch(message.didNotFind("embedded database").atAll());

            return ConditionOutcome.match(message.found("embedded database").items(type));
        }
    }
}
