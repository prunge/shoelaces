package au.net.causal.shoelaces.jdbc;

import au.net.causal.shoelaces.apphome.ApplicationHome;
import org.apache.derby.jdbc.EmbeddedDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.datasource.embedded.ConnectionProperties;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseConfigurer;

import javax.sql.DataSource;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.SQLException;
import java.util.Objects;
import java.util.Properties;

/**
 * An alternate implementation to Spring's standard Derby configurer that stores the database on disk instead of in
 * memory.
 * <p>
 *
 * These are the changes from the standard <code>DerbyEmbeddedDatabaseConfigurer</code> implementation:
 *
 * <ul>
 *     <li>Uses a filesystem directory for storing the database instead of an in-memory Derby database</li>
 *     <li>Creates the database with ;create=true if needed on startup, but then restarts the database so that the eventual database URL does not have this parameter</li>
 *     <li>Shuts down the database cleanly when the application is shutting down</li>
 *     <li>Database log messages are sent to a <code>derby.log</code> file underneath application home directory rather than being discarded</li>
 *     <li>Database user name and password are configurable</li>
 * </ul>
 */
public class DerbyDiskEmbeddedDatabaseConfigurer implements EmbeddedDatabaseConfigurer
{
    private static final Logger log = LoggerFactory.getLogger(DerbyDiskEmbeddedDatabaseConfigurer.class);

    private static final String DERBY_STREAM_ERROR_FILE_PROPERTY = "derby.stream.error.file";

    private final Path databaseDirectory;
    private final String username;
    private final String password;

    /**
     * Constructs a <code>DerbyDiskEmbeddedDatabaseConfigurer</code>.
     *
     * @param databaseDirectory directory to store the database data under.
     * @param applicationHome application home, which is used for the derby.log file that contains derby logs.
     * @param username the database username.
     * @param password the database password.
     *
     * @throws IOException if an I/O error occurs.
     * @throws SQLException if a database error occurs.
     */
    public DerbyDiskEmbeddedDatabaseConfigurer(Path databaseDirectory, ApplicationHome applicationHome, String username, String password)
    throws IOException, SQLException
    {
        this.databaseDirectory = Objects.requireNonNull(databaseDirectory);
        this.username = username;
        this.password = password;
        configureDerbyLog(applicationHome);

        if (Files.notExists(databaseDirectory))
            preCreateDatabase();
    }

    /**
     * To avoid making a database URI with ';create=true' which results in SQL warnings later on if the DB actually exists,
     * create the database here first.
     *
     * @throws IOException if an error occurs creating database directories.
     * @throws SQLException if an error occurs creating the database.
     */
    private void preCreateDatabase() 
    throws IOException, SQLException
    {
        Files.createDirectories(databaseDirectory.getParent());
        
        Properties props = new Properties();
        props.setProperty("user", username);
        props.setProperty("password", password);
        try (Connection con = new EmbeddedDriver().connect(databaseUri() + ";create=true", props))
        {
            log.info("Pre-created derby database: " + con.getMetaData().getURL());
        }
        shutdownDerby();
    }

    /**
     * @return the Derby embedded driver class.
     */
    public static Class<? extends Driver> driverClass()
    {
        return EmbeddedDriver.class;
    }

    /**
     * @return the database URI for use when creating Derby database connections.
     */
    protected String databaseUri()
    {
        return "jdbc:derby:" + databaseDirectory.toString().replace(databaseDirectory.getFileSystem().getSeparator(), "/");
    }
    
    @Override
    public void configureConnectionProperties(ConnectionProperties properties, String databaseName)
    {
        properties.setDriverClass(driverClass());
        properties.setUrl(databaseUri());
        properties.setUsername(username);
        properties.setPassword(password);
    }

    private void shutdownDerby()
    {
        try
        {
            new EmbeddedDriver().connect(databaseUri() + ";shutdown=true", new Properties());
        }
        catch (SQLException ex)
        {
            // Error code that indicates successful shutdown
            if (!"08006".equals(ex.getSQLState()))
                log.warn("Could not shut down embedded Derby database", ex);
        }
    }

    @Override
    public void shutdown(DataSource dataSource, String databaseName)
    {
        shutdownDerby();
        log.info("Derby database " + databaseName + " has been shut down");
    }

    /**
     * Sets up Derby to log messages to the specified log file.  This is a JVM-wide configuration.
     * Creates parent directories of the file if they do not yet exist.
     *
     * @param derbyLogFile the log file that all Derby connections and instances will log messages to.
     *
     * @throws IOException if an I/O error occurs.
     */
    public static void configureDerbyLogSystemProperty(Path derbyLogFile)
    throws IOException
    {
        Files.createDirectories(derbyLogFile.getParent());
        System.setProperty(DERBY_STREAM_ERROR_FILE_PROPERTY, derbyLogFile.toAbsolutePath().toString());
    }

    /**
     * Configures Derby to log messages to a <code>derby.log</code> file under the application home directory.
     * This is a JVM-wide configuration for all Derby instances.
     *
     * @param applicationHome application home.
     */
    private void configureDerbyLog(ApplicationHome applicationHome)
    {
        //Only do this if derby error file not explicitly configured
        if (System.getProperty(DERBY_STREAM_ERROR_FILE_PROPERTY) != null)
            return;

        //Don't derby.log to current directory, do it to known writable directory instead
        Path derbyLogLocation = applicationHome.getApplicationHomeDirectory().resolve("derby.log");

        try
        {
            configureDerbyLogSystemProperty(derbyLogLocation);
        }
        catch (IOException e)
        {
            log.error("Error creating derby log directory: " + e.getMessage(), e);
        }
    }
}
