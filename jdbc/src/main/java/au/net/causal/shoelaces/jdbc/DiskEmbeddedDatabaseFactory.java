package au.net.causal.shoelaces.jdbc;

import au.net.causal.shoelaces.apphome.ApplicationHome;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseConfigurer;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import java.io.IOException;
import java.nio.file.Path;
import java.sql.SQLException;
import java.util.EnumSet;
import java.util.Set;

/**
 * Factory for creating configurers for local file-based embedded databases.
 */
public class DiskEmbeddedDatabaseFactory
{
    /**
     * Creates a database configurer for the given configuration.
     *
     * @param databaseType the database type.
     * @param databaseDirectory directory to store database data in.
     * @param applicationHome application home.  May be used for database log files, etc
     * @param username the database username.
     * @param password the database password.
     *
     * @return the created configurer.
     *
     * @throws IOException if an I/O error occurs.
     * @throws SQLException if a database error occurs.
     */
    public EmbeddedDatabaseConfigurer create(EmbeddedDatabaseType databaseType, Path databaseDirectory, ApplicationHome applicationHome, 
                                             String username, String password)
    throws IOException, SQLException
    {
        switch (databaseType)
        {
            case DERBY:
                return new DerbyDiskEmbeddedDatabaseConfigurer(databaseDirectory, applicationHome, username, password);
            case H2:
                return new H2DiskEmbeddedDatabaseConfigurer(databaseDirectory, username, password);
            case HSQL:
                return new HsqlDiskEmbeddedDatabaseConfigurer(databaseDirectory, username, password);
            default:
                throw new Error("Unsupported embedded database type: " + databaseType);
        }
    }

    /**
     * Determines which of the embedded database types are available on the classpath.
     *
     * @param loader the classloader to search the classpath of.
     *
     * @return a set of embedded database types that may be used with the given classpath.
     */
    public Set<? extends EmbeddedDatabaseType> getAvailableDatabasesOnClasspath(ClassLoader loader)
    {
        EnumSet<EmbeddedDatabaseType> dbTypes = EnumSet.noneOf(EmbeddedDatabaseType.class);

        //We know these classes exist on our classpath, but we're checking whether they also are present on loader
        if (classExistsInLoader(DerbyDiskEmbeddedDatabaseConfigurer.driverClass(), loader))
            dbTypes.add(EmbeddedDatabaseType.DERBY);
        if (classExistsInLoader(H2DiskEmbeddedDatabaseConfigurer.driverClass(), loader))
            dbTypes.add(EmbeddedDatabaseType.H2);
        if (classExistsInLoader(HsqlDiskEmbeddedDatabaseConfigurer.driverClass(), loader))
            dbTypes.add(EmbeddedDatabaseType.HSQL);

        return dbTypes;
    }

    /**
     * Finds whether a class exists under the given classloader.
     *
     * @param clazz the class to search for.
     * @param loader the classloader to search.
     *
     * @return true if the class exists, false if not.
     */
    private boolean classExistsInLoader(Class<?> clazz, ClassLoader loader)
    {
        try
        {
            Class.forName(clazz.getName(), false, loader);
            return true;
        }
        catch (ClassNotFoundException e)
        {
            return false;
        }
    }
}
