package au.net.causal.shoelaces.jdbc;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = ShoelacesDataSourceProperties.PREFIX)
public class ShoelacesDataSourceProperties
{
    public static final String PREFIX = "shoelaces.datasource";

    /**
     * Use a disk-based database.  If false, an in-memory database that is not persisted across application restarts is used.
     */
    private boolean useDiskDatabase = true;

    /**
     * The name of the directory underneath application home to contain the database.
     */
    private String databaseDirectoryName = "db";

    public boolean isUseDiskDatabase()
    {
        return useDiskDatabase;
    }

    public void setUseDiskDatabase(boolean useDiskDatabase)
    {
        this.useDiskDatabase = useDiskDatabase;
    }

    public String getDatabaseDirectoryName()
    {
        return databaseDirectoryName;
    }

    public void setDatabaseDirectoryName(String databaseDirectoryName)
    {
        this.databaseDirectoryName = databaseDirectoryName;
    }
}
