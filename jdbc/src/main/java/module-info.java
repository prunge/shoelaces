open module au.net.causal.shoelaces.jdbc
{
    exports au.net.causal.shoelaces.jdbc;

    requires au.net.causal.shoelaces.apphome;
    requires spring.core;
    requires spring.beans;
    requires spring.boot;
    requires spring.context;
    requires spring.web;
    requires spring.boot.autoconfigure;
    requires spring.jdbc;
    requires java.sql;

    requires org.slf4j;

    requires static com.h2database;
    requires static org.hsqldb;
    requires static org.apache.derby.tools;
}
