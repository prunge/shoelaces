package au.net.causal.shoelaces.jersey.common;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.*;

class TestAbstractJacksonDateTimeParamConverterProvider
{
    @Test
    void findFirstInstanceNormal()
    {
        Number[] values = {Integer.valueOf(7), Double.valueOf(-1.0), Integer.valueOf(3)};
        Integer firstInteger = AbstractJacksonDateTimeParamConverterProvider.findFirstInstance(values, Integer.class);
        Double firstDouble = AbstractJacksonDateTimeParamConverterProvider.findFirstInstance(values, Double.class);
        assertThat(firstInteger).isEqualTo(7);
        assertThat(firstDouble).isEqualTo(-1.0);
    }

    @Test
    void findFirstInstanceNotFound()
    {
        Number[] values = {Integer.valueOf(7), Double.valueOf(-1.0), Integer.valueOf(3)};
        Byte firstByte = AbstractJacksonDateTimeParamConverterProvider.findFirstInstance(values, Byte.class);
        assertThat(firstByte).isNull();
    }
}
