package au.net.causal.shoelaces.jersey.common;

import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Response;

/**
 * Thrown when an error occurs parsing a request parameter.
 */
public class WebApplicationParamParsingException extends WebApplicationException
{
    private final String rawParameterValue;

    public WebApplicationParamParsingException(String rawParameterValue)
    {
        this.rawParameterValue = rawParameterValue;
    }

    public WebApplicationParamParsingException(String message, String rawParameterValue)
    {
        super(message);
        this.rawParameterValue = rawParameterValue;
    }

    public WebApplicationParamParsingException(Response response, String rawParameterValue)
    {
        super(response);
        this.rawParameterValue = rawParameterValue;
    }

    public WebApplicationParamParsingException(String message, Response response, String rawParameterValue)
    {
        super(message, response);
        this.rawParameterValue = rawParameterValue;
    }

    public WebApplicationParamParsingException(int status, String rawParameterValue)
    {
        super(status);
        this.rawParameterValue = rawParameterValue;
    }

    public WebApplicationParamParsingException(String message, int status, String rawParameterValue)
    {
        super(message, status);
        this.rawParameterValue = rawParameterValue;
    }

    public WebApplicationParamParsingException(Response.Status status, String rawParameterValue)
    {
        super(status);
        this.rawParameterValue = rawParameterValue;
    }

    public WebApplicationParamParsingException(String message, Response.Status status, String rawParameterValue)
    {
        super(message, status);
        this.rawParameterValue = rawParameterValue;
    }

    public WebApplicationParamParsingException(Throwable cause, String rawParameterValue)
    {
        super(cause);
        this.rawParameterValue = rawParameterValue;
    }

    public WebApplicationParamParsingException(String message, Throwable cause, String rawParameterValue)
    {
        super(message, cause);
        this.rawParameterValue = rawParameterValue;
    }

    public WebApplicationParamParsingException(Throwable cause, Response response, String rawParameterValue)
    {
        super(cause, response);
        this.rawParameterValue = rawParameterValue;
    }

    public WebApplicationParamParsingException(String message, Throwable cause, Response response, String rawParameterValue)
    {
        super(message, cause, response);
        this.rawParameterValue = rawParameterValue;
    }

    public WebApplicationParamParsingException(Throwable cause, int status, String rawParameterValue)
    {
        super(cause, status);
        this.rawParameterValue = rawParameterValue;
    }

    public WebApplicationParamParsingException(String message, Throwable cause, int status, String rawParameterValue)
    {
        super(message, cause, status);
        this.rawParameterValue = rawParameterValue;
    }

    public WebApplicationParamParsingException(Throwable cause, Response.Status status, String rawParameterValue) throws IllegalArgumentException
    {
        super(cause, status);
        this.rawParameterValue = rawParameterValue;
    }

    public WebApplicationParamParsingException(String message, Throwable cause, Response.Status status, String rawParameterValue) throws IllegalArgumentException
    {
        super(message, cause, status);
        this.rawParameterValue = rawParameterValue;
    }

    /**
     * @return the raw parameter value that failed to parse.
     */
    public String getRawParameterValue()
    {
        return rawParameterValue;
    }
}
