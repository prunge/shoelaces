package au.net.causal.shoelaces.jersey.common;

import jakarta.ws.rs.WebApplicationException;

/**
 * Thrown when a JSON response is valid JSON but has an unexpected type.  This may be thrown, for example,
 * when a client requires a String type but the JSON returned from the server is a JSON array.
 */
public class UnexpectedJsonTypeException extends WebApplicationException
{
    private final Object jsonValue;

    /**
     * Creates an unexpected JSON exception.
     *
     * @param jsonValue the JSON value that did not have the expected type.
     * @param message an exception detail message.
     */
    public UnexpectedJsonTypeException(Object jsonValue, String message)
    {
        super(message);
        this.jsonValue = jsonValue;
    }

    /**
     * @return the read JSON value that not of the expected type.
     */
    public Object getJsonValue()
    {
        return jsonValue;
    }
}
