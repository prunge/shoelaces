package au.net.causal.shoelaces.testing;

import au.net.causal.shoelaces.testing.TestJerseyServerEndpointExtension.ReallySimpleApp;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import jakarta.ws.rs.core.MediaType;
import org.glassfish.jersey.client.JerseyClient;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.stereotype.Controller;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.temporal.ChronoField;

import static org.assertj.core.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = ReallySimpleApp.class, webEnvironment = WebEnvironment.RANDOM_PORT)
public class TestJerseyServerEndpointExtension
{
    @LocalServerPort
    private int serverPort;
    
    @RegisterExtension
    static JerseyServerEndpointExtension server = new JerseyServerEndpointExtension().usePath("/test")
                                                        .useClientConfigurer(TestJerseyServerEndpointExtension::makeJerseyDoSomething)
                                                        .useObjectMapperConfigurer(TestJerseyServerEndpointExtension::configureObjectMapper);

    private static JerseyClient clientSeenByConfigurer;

    private static void configureObjectMapper(ObjectMapper om)
    {
        om.registerModule(new JavaTimeModule());
        //When the feature is disabled, the last number when an array is parsed as a date on the client is interpreted as
        //milliseconds instead of nanoseconds
        om.disable(DeserializationFeature.READ_DATE_TIMESTAMPS_AS_NANOSECONDS);
    }

    private static void makeJerseyDoSomething(JerseyClient client)
    {
        //Anything can happen here
        clientSeenByConfigurer = client;
    }

    @Test
    void checkServerUri()
    throws MalformedURLException
    {
        //Check the URL is using Spring's dynamic port
        assertThat(serverPort).isNotZero();
        assertThat(server.getUri()).isEqualTo(URI.create("http://localhost:" + serverPort + "/test"));
        assertThat(server.getUrl()).isEqualTo(new URL("http://localhost:" + serverPort + "/test"));
        assertThat(server.getHost()).isEqualTo("localhost");
        assertThat(server.getPort()).isEqualTo(serverPort);
        assertThat(server.target().getUri()).isEqualTo(URI.create("http://localhost:" + serverPort + "/test"));
    }

    @Test
    void simpleStringResponse()
    {
        //Let's verify we can actually hit the endpoint with Jersey
        String response = server.target("simple").request(MediaType.APPLICATION_JSON_TYPE).get(String.class);
        assertThat(response).isEqualTo("Galah");
    }

    /**
     * Verify that object mapper configurers have effect.
     */
    @Test
    public void objectMapperConfigurationIsUsed()
    {
        //Let's verify we can actually hit the endpoint with Jersey
        MyStructure response = server.target("timestamp").request(MediaType.APPLICATION_JSON_TYPE).get(MyStructure.class);
        //Because we explicitly turn off reading last number of date as nanos (which is on by default), we shoulld
        //get milliseconds instead
        assertThat(response.date).isEqualToIgnoringNanos(LocalDateTime.of(1977, 1, 9, 15, 16, 17));

        //That's 18 milliseconds as opposed to 18 nanoseconds because of our configured setting
        //If the object mapper config is not in effect, we'll get 18 nanos instead
        assertThat(response.date.get(ChronoField.MILLI_OF_SECOND)).isEqualTo(18);
    }

    @Test
    public void clientConfigurerIsCalled()
    {
        assertThat(server.client()).isEqualTo(clientSeenByConfigurer);
    }

    @SpringBootApplication
    public static class ReallySimpleApp
    {
        @Controller
        @RequestMapping("/test/simple")
        public static class MyController
        {
            @GetMapping
            @ResponseBody
            public String reallySimpleEndpoint()
            {
                return "Galah";
            }
        }

        @Controller
        @RequestMapping("/test/timestamp")
        public static class Timestamp
        {
            @GetMapping
            @ResponseBody
            public String timestampEndpoint()
            {
                //Just use Strings in Spring, let Jersey client do fancy deserialization
                return "{\"date\":[1977,1,9,15,16,17,18]}";
            }
        }
    }

    /**
     * Used for Jackson deserialization only on the client.
     */
    public static class MyStructure
    {
        public LocalDateTime date;
    }
}
