package au.net.causal.shoelaces.testing;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;

import static org.assertj.core.api.Assertions.*;

/**
 * No Spring, default port.
 */
public class TestServerEndpointExtension
{
    @RegisterExtension
    static ServerEndpointExtension server = new ServerEndpointExtension().usePath("/api");

    @Test
    public void checkServerUrl()
    throws MalformedURLException
    {
        assertThat(server.getUri()).isEqualTo(URI.create("http://localhost:8080/api"));
        assertThat(server.getUrl()).isEqualTo(new URL("http://localhost:8080/api"));
        assertThat(server.getHost()).isEqualTo("localhost");
        assertThat(server.getPort()).isEqualTo(8080);
    }
}
