package au.net.causal.shoelaces.testing;

import au.net.causal.shoelaces.testing.TestServerEndpointExtensionWithSpring.ReallySimpleApp;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;

import static org.assertj.core.api.Assertions.*;

/**
 * Use Spring to bring up its own webserver running on a random port and check that the server URL picks this information up automatically.
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = ReallySimpleApp.class, webEnvironment = WebEnvironment.RANDOM_PORT)
public class TestServerEndpointExtensionWithSpring
{
    @LocalServerPort
    private int serverPortFromSpring;
    
    @RegisterExtension
    static ServerEndpointExtension server = new ServerEndpointExtension().usePath("/myServer");
    
    @Test
    public void checkServerUrl()
    throws MalformedURLException
    {
        //Check that Spring web server has come up and has configured its own port which will be random
        assertThat(serverPortFromSpring).isNotZero();

        //Should be using the random port from Spring
        assertThat(server.getUri()).isEqualTo(URI.create("http://localhost:" + serverPortFromSpring + "/myServer"));
        assertThat(server.getUrl()).isEqualTo(new URL("http://localhost:" + serverPortFromSpring + "/myServer"));
        assertThat(server.getHost()).isEqualTo("localhost");
        assertThat(server.getPort()).isEqualTo(serverPortFromSpring);
    }

    /**
     * A nothing Spring app just to allow SpringExtension to bring a webserver up.
     */
    @SpringBootApplication
    public static class ReallySimpleApp
    {
    }
}
