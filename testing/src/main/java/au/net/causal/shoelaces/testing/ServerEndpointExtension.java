package au.net.causal.shoelaces.testing;

import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ExtensionContext.Namespace;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.IOException;
import java.net.ConnectException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * An extension to configure a server URL to test against.
 * <p>
 *
 * Server configuration can be set using system properties, but defaults are chosen so that
 * tests can run against typical server execution through IDEs, etc.
 */
public class ServerEndpointExtension implements BeforeAllCallback, BeforeEachCallback
{
    private static final List<Integer> DEFAULT_PORTS = List.of(8080);

    private static final String SERVER_HOST_PROPERTY = "test.server.host";
    private static final String SERVER_PORT_PROPERTY = "test.server.port";
    private static final String SERVER_TIMEOUT_PROPERTY = "test.server.wait";

    //This is defined in SpringExtension but private so need to copy
    private static final ExtensionContext.Namespace SPRING_EXTENSION_NAMESPACE = Namespace.create(SpringExtension.class);

    private String host = "localhost";
    private final List<Integer> ports = new ArrayList<>(DEFAULT_PORTS);
    private boolean portExplicitlyConfigured;
    private String path = "";
    private Duration waitForStartupDuration;

    private URI resolvedUri;

    private final Clock clock = Clock.systemDefaultZone();

    /**
     * Configures the host to use when accessing web services through this extension.  By
     * default, the host is <code>localhost</code>.
     *
     * @param host the host to use.
     *
     * @return this extension.
     */
    public ServerEndpointExtension useHost(String host)
    {
        this.host = host;
        return this;
    }

    /**
     * Configures the port to use when accessing web services through this extension as a string.
     * <p>
     * This is meant to be used by subclasses as it performs string parsing.  Use {@link #usePort(int)} or
     * {@link #usePorts(int, int...)} in normal cases.
     *
     * @param portStr the HTTP port to use as a string.
     *
     * @return this extension
     *
     * @see #usePort(int)
     * @see #usePorts(int, int...)
     */
    protected ServerEndpointExtension usePortString(String portStr)
    {
        try
        {
            return usePort(Integer.parseInt(portStr));
        }
        catch (NumberFormatException e)
        {
            throw new IllegalArgumentException(SERVER_PORT_PROPERTY + " value of '" + portStr + "' must be a number", e);
        }
    }

    /**
     * Configures the port to use when accessing web services through this extension.
     *
     * @param port the HTTP port.
     *
     * @return this extension.
     *
     * @see #usePorts(int, int...)
     */
    public ServerEndpointExtension usePort(int port)
    {
        return usePorts(port);
    }

    /**
     * Configures multiple potential ports to use when accessing web services through this extension.
     * <p>
     *
     * When the extension is setting up during test initialization, each port specified in the arguments
     * will be checked to see if a server is running on it by performing an HTTP request.  If the request
     * makes a connection successfully then a server is deemed to be running on the port.  The response is ignored -
     * it could be an HTTP error, as long as any response comes through then that port will be selected.
     *
     * @param first the first port to check.  At least one port must be specified.
     * @param others more ports to check.
     *
     * @return this extension.
     */
    public ServerEndpointExtension usePorts(int first, int... others)
    {
        portExplicitlyConfigured = true;
        ports.clear();
        ports.add(first);
        for (int other : others)
        {
            ports.add(other);
        }
        return this;
    }

    /**
     * Sets the default path to use when accessing web services through this extension.
     *
     * @param path the path part of a URI.
     *
     * @return this extension.
     */
    public ServerEndpointExtension usePath(String path)
    {
        this.path = path;
        return this;
    }

    /**
     * Configures the server startup wait duration as a string in ISO-8601 duration
     * format.
     * <p>
     *
     * Protected since it is designed to be called by subclasses only, normal use should be done through
     * {@link #useServerStartupWait(Duration)}.
     *
     * @param durationString the duration string in ISO-8601 duration format.
     *
     * @return this extension.
     *
     * @see #useServerStartupWait(Duration)
     * @see Duration#parse(CharSequence)
     */
    protected ServerEndpointExtension useServerStartupWaitDurationString(String durationString)
    {
        try
        {
            useServerStartupWait(Duration.parse(durationString));
            return this;
        }
        catch (DateTimeParseException e)
        {
            //Give a more helpful error message
            throw new DateTimeParseException("Invalid duration '" + durationString + "' given for " + SERVER_TIMEOUT_PROPERTY +
                                                     " system property - must be a parseable duration. See " + Duration.class.getName() +
                                                     " documentation for details on duration strings.", e.getParsedString(), e.getErrorIndex(), e);
        }
    }

    /**
     * If configured, the extension will check that a server is running, and if not, will wait at most this amount of
     * time for the server to complete starting up.
     * <p>
     *
     * This is useful in situations when a server might not be fully started up by the time the tests start running.
     * The server is deemed to have started when an HTTP request can be made to the server (it does not matter if the
     * response is an HTTP error response).
     *
     * @param duration the maximum amount of time to wait for the server to start up.
     *
     * @return this extension.
     */
    public ServerEndpointExtension useServerStartupWait(Duration duration)
    {
        this.waitForStartupDuration = duration;
        return this;
    }

    @Override
    public void beforeAll(ExtensionContext context)
    throws Exception
    {
        setup(context);
    }

    @Override
    public void beforeEach(ExtensionContext context)
    throws Exception
    {
        setup(context);
    }

    /**
     * Converts empty string to Optional.empty(), else returns a value.
     *
     * @param s potentially empty string.
     *
     * @return an optional.
     */
    private Optional<String> emptyStringToNothing(String s)
    {
        if (s.isEmpty())
            return Optional.empty();

        return Optional.of(s);
    }

    /**
     * If port has not been explicitly configured by the user, the test is using the Spring extension and its using a Spring-created web server for
     * testing, use the port from that.
     */
    private void setupPortFromSpring(ExtensionContext context)
    {
        //If the user explicitly specified port, don't fill this in using Spring
        if (!portExplicitlyConfigured)
        {
            //Detect if we are actually using a Spring extension - if not then don't use it to configure ports
            ExtensionContext.Store springExtensionStore = context.getRoot().getStore(SPRING_EXTENSION_NAMESPACE);
            Object storeValue = springExtensionStore.get(context.getRequiredTestClass());
            if (storeValue != null)
            {
                ApplicationContext applicationContext = SpringExtension.getApplicationContext(context);
                Integer springPort = applicationContext.getEnvironment().getProperty("local.server.port", Integer.class);
                if (springPort != null)
                    usePort(springPort);
            }
        }
    }

    protected void setup(ExtensionContext context)
    throws Exception
    {
        //If already setup, don't do it again
        if (resolvedUri != null)
            return;

        setupPortFromSpring(context);
        
        //System properties take prescedence over configured defaults
        context.getConfigurationParameter(SERVER_HOST_PROPERTY).flatMap(this::emptyStringToNothing).ifPresent(this::useHost);
        context.getConfigurationParameter(SERVER_PORT_PROPERTY).flatMap(this::emptyStringToNothing).ifPresent(this::usePortString);
        context.getConfigurationParameter(SERVER_TIMEOUT_PROPERTY).flatMap(this::emptyStringToNothing).ifPresent(this::useServerStartupWaitDurationString);

        //If there are multiple ports we need to detect which one is up
        List<URI> potentialUris = this.ports.stream()
                                            .map(port -> URI.create("http://" + host + ":" + port + "/").resolve(path))
                                            .collect(Collectors.toList());

        if (waitForStartupDuration != null)
            resolvedUri = performServerWait(potentialUris, waitForStartupDuration, true);
        else if (potentialUris.size() == 1) //No need to hit the server if there's only one choice
            resolvedUri = potentialUris.get(0);
        else //Multiple potentials but we were told not to wait, so do one hit just to find the right URL, using first one if nothing is up
            resolvedUri = performServerWait(potentialUris, Duration.ZERO, false);
    }

    /**
     * Checks each potential URL which is constructed from potentially multiple ports configured by the user,
     * in a loop.  Returns when an HTTP request is successfully made to a server and an HTTP response is
     * received (though the HTTP response might be an error response).
     *
     * @param potentialUris potential URLs to try.
     * @param maxTimeToWait maximum amount of time to wait until giving up for the server to start.
     * @param failOnTimeout true to fail with an exception when timing out, false to just return the first
     *                      URI in the list when timing out.
     *
     * @return the URI that should be used, either because a server exists there or there was a timeout.
     *
     * @throws TimeoutException if no server came up within the waiting time.
     */
    private URI performServerWait(Collection<URI> potentialUris, Duration maxTimeToWait, boolean failOnTimeout)
    {
        Instant timeoutTime = clock.instant().plus(maxTimeToWait);

        ConnectException lastError = null;
        try
        {
            do
            {
                for (URI curPotentialUri : potentialUris)
                {
                    try
                    {
                        curPotentialUri.toURL().getContent();

                        //If we get here, the server is up
                        return curPotentialUri;
                    }
                    catch (MalformedURLException e)
                    {
                        //There's something wrong with the URL, let's abort and notify the user
                        throw new Error("Invalid URL: " + curPotentialUri + ": " + e.getMessage(), e);
                    }
                    catch (ConnectException e)
                    {
                        //Failed to connect, try again a bit later
                        lastError = e;
                    }
                    catch (IOException e)
                    {
                        //If we get here it's probably an auth error, in any case it connected OK so waiting is done
                        return curPotentialUri;
                    }
                }
                Thread.sleep(200L);
            }
            while (clock.instant().isBefore(timeoutTime));
        }
        catch (InterruptedException e)
        {
            throw new RuntimeException("Interrupted waiting for server startup.", e);
        }

        if (failOnTimeout)
            throw new TimeoutException("Timeout (" + maxTimeToWait + ") waiting for server at " + potentialUris + " to start up.", lastError);

        //If nothing worked and we were told not to fail, just pick the first URI in the list
        return potentialUris.iterator().next();
    }

    /**
     * Verify the testing lifecycle methods have been called before reading the resolved URI.  Throwing an error like this
     * should help the user with the common mistake of forgetting to use <code>@RegisterExtension</code>.
     */
    protected void verifySetup()
    {
        if (resolvedUri == null)
            throw new Error(getClass().getName() + " has not been set up.  Ensure its field is annoated with @RegisterExtension and is not private.");
    }

    /**
     * @return the actual port of the server to test against, which might have been chosen from a list of
     *         potential ports.
     */
    public int getPort()
    {
        verifySetup();
        return resolvedUri.getPort();
    }

    /**
     * @return the host the server to test against.
     */
    public String getHost()
    {
        verifySetup();
        return resolvedUri.getHost();
    }

    /*
     * @return the URI of the server to test against.
     */
    public URI getUri()
    {
        verifySetup();
        return resolvedUri;
    }

    /**
     * @return the URL of the server to test against.
     */
    public URL getUrl()
    {
        verifySetup();
        try
        {
            return resolvedUri.toURL();
        }
        catch (MalformedURLException e)
        {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String toString()
    {
        if (resolvedUri == null)
            return "<unresolved>";
        else
            return resolvedUri.toString();
    }

    /**
     * Thrown when a server could not be detected within a timeout period.
     */
    public static class TimeoutException extends RuntimeException
    {
        public TimeoutException(String message, Throwable cause)
        {
            super(message, cause);
        }
    }
}
