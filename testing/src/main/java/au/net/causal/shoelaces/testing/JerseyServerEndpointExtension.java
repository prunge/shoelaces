package au.net.causal.shoelaces.testing;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.ws.rs.ext.ContextResolver;
import org.glassfish.jersey.client.JerseyClient;
import org.glassfish.jersey.client.JerseyClientBuilder;
import org.glassfish.jersey.client.JerseyWebTarget;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

/**
 * An extension to aid testing servers using a Jersey client.  To use, configure the extension in your tests, then
 * grab a client using {@link #client()} or a target using {@link #target()} or {@link #target(String)}.
 * The Jersey client will be preconfigured with the server URL.
 * <p>
 *
 * Server configuration can be set using system properties, but defaults are chosen so that
 * tests can run against typical server execution through IDEs, etc.
 */
public class JerseyServerEndpointExtension extends ServerEndpointExtension
implements AfterEachCallback, AfterAllCallback
{
    private JerseyClient jerseyClient;
    private JerseyWebTarget rootTarget;

    private List<JerseyClientBuilderTestConfigurer> jerseyClientBuilderConfigurers = new ArrayList<>();
    private List<JerseyClientTestConfigurer> jerseyClientConfigurers = new ArrayList<>();
    private List<ObjectMapperTestConfigurer> objectMapperConfigurers = new ArrayList<>();
    private LoginCredentials loginCredentials;

    private boolean beforeAllCalled;
    
    @Override
    public void beforeAll(ExtensionContext context)
    throws Exception
    {
        beforeAllCalled = true;
        super.beforeAll(context);
    }

    @Override
    public void beforeEach(ExtensionContext context) 
    throws Exception
    {
        super.beforeEach(context);
    }

    @Override
    public void afterAll(ExtensionContext context)
    throws Exception
    {
        if (jerseyClient != null)
        {
            jerseyClient.close();
            jerseyClient = null;
        }
    }

    @Override
    public void afterEach(ExtensionContext context)
    throws Exception
    {
        //If the client was created in beforeAll, we'll get an afterAll callback so wait till then to clean up
        if (beforeAllCalled)
            return;

        if (jerseyClient != null)
        {
            jerseyClient.close();
            jerseyClient = null;
        }
    }

    @Override
    public JerseyServerEndpointExtension useHost(String host)
    {
        super.useHost(host);
        return this;
    }

    @Override
    protected JerseyServerEndpointExtension usePortString(String portStr)
    {
        super.usePortString(portStr);
        return this;
    }

    @Override
    public JerseyServerEndpointExtension usePort(int port)
    {
        super.usePort(port);
        return this;
    }

    @Override
    public JerseyServerEndpointExtension usePorts(int first, int... others)
    {
        super.usePorts(first, others);
        return this;
    }

    @Override
    public JerseyServerEndpointExtension usePath(String path)
    {
        super.usePath(path);
        return this;
    }

    @Override
    protected JerseyServerEndpointExtension useServerStartupWaitDurationString(String durationString)
    {
        super.useServerStartupWaitDurationString(durationString);
        return this;
    }

    @Override
    public JerseyServerEndpointExtension useServerStartupWait(Duration duration)
    {
        super.useServerStartupWait(duration);
        return this;
    }

    /**
     * Configures the extension to use HTTP basic authentication using the specified login credentials.
     *
     * @param username login user name.
     * @param password login password.
     *
     * @return this extension.
     */
    public JerseyServerEndpointExtension useLogin(String username, String password)
    {
        if (username == null)
            this.loginCredentials = null;
        else
            this.loginCredentials = new LoginCredentials(username, password);

        //This might be called after setup to change credentials, so re-create client in this case
        if (jerseyClient != null)
        {
            try
            {
                setupJerseyClient();
            }
            catch (RuntimeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new RuntimeException("Error setting up Jersey: " + e, e);
            }
        }

        return this;
    }

    /**
     * Clears login credentials from the extension.  Use this in test methods if login credentials have been configured
     * in test setup but a few special tests should not use them.
     *
     * @return this extension.
     */
    public JerseyServerEndpointExtension useNoLogin()
    {
        return useLogin(null, null);
    }

    /**
     * Configures Jersey client builders for all clients produced from this extension.
     *
     * @param configurer some code that configures the Jersey client builder.  Typically a lambda or method reference.
     *
     * @return this extension.
     *
     * @see #useClientBuilderConfigurers(JerseyClientBuilderTestConfigurer...)
     */
    public JerseyServerEndpointExtension useClientBuilderConfigurer(JerseyClientBuilderTestConfigurer configurer)
    {
        return useClientBuilderConfigurers(configurer);
    }

    /**
     * Configures Jersey client builders for all clients produced from this extension.
     *
     * @param configurers configurers that configure the Jersey client builder.
     *
     * @return this extension.
     *
     * @see #useClientBuilderConfigurer(JerseyClientBuilderTestConfigurer)
     */
    public JerseyServerEndpointExtension useClientBuilderConfigurers(JerseyClientBuilderTestConfigurer... configurers)
    {
        this.jerseyClientBuilderConfigurers.addAll(List.of(configurers));
        return this;
    }

    /**
     * Configures Jersey clients produced from this extension.
     *
     * @param configurer some code that configures the Jersey client.  Typically a lambda or method reference.
     *
     * @return this extension.
     *
     * @see #useClientConfigurers(JerseyClientTestConfigurer...)
     */
    public JerseyServerEndpointExtension useClientConfigurer(JerseyClientTestConfigurer configurer)
    {
        return useClientConfigurers(configurer);
    }

    /**
     * Configures Jersey clients produced from this extension.
     *
     * @param configurers configurers that configure the Jersey client.
     *
     * @return this extension.
     *
     * @see #useClientConfigurer(JerseyClientTestConfigurer)
     */
    public JerseyServerEndpointExtension useClientConfigurers(JerseyClientTestConfigurer... configurers)
    {
        this.jerseyClientConfigurers.addAll(List.of(configurers));
        return this;
    }

    /**
     * Configures the Jackson object mapper on Jersey clients produced from this extension.
     *
     * @param configurer some code that configures object mappers.  Typically a lambda or method reference.
     *
     * @return this extension.
     *
     * @see #useObjectMapperConfigurers(ObjectMapperTestConfigurer...)
     */
    public JerseyServerEndpointExtension useObjectMapperConfigurer(ObjectMapperTestConfigurer configurer)
    {
        return useObjectMapperConfigurers(configurer);
    }

    /**
     * Configures the Jackson object mapper on Jersey clients produced from this extension.
     *
     * @param configurers configurers that configure object mappers.
     *
     * @return this extension.
     *
     * @see #useObjectMapperConfigurer(ObjectMapperTestConfigurer)
     */
    public JerseyServerEndpointExtension useObjectMapperConfigurers(ObjectMapperTestConfigurer... configurers)
    {
        this.objectMapperConfigurers.addAll(List.of(configurers));
        return this;
    }

    @Override
    protected void setup(ExtensionContext context)
    throws Exception
    {
        super.setup(context);
        setupJerseyClient();
    }

    /**
     * Creates the Jersey client, configuring it with any configurers registered on the extension.
     * Jackson is preconfigured.
     *
     * @throws Exception if an error occurs.
     */
    private void setupJerseyClient()
    throws Exception
    {
        //Set up Jersey client
        JerseyClientBuilder builder = new JerseyClientBuilder();
        if (!objectMapperConfigurers.isEmpty())
        {
            ObjectMapper objectMapper = new ObjectMapper();
            for (ObjectMapperTestConfigurer configurer : objectMapperConfigurers)
            {
                configurer.configure(objectMapper);
            }
            builder.register(JacksonFeature.class);
            builder.register(new ObjectMapperContextResolver(objectMapper), ContextResolver.class);
        }
        for (JerseyClientBuilderTestConfigurer configurer : jerseyClientBuilderConfigurers)
        {
            configurer.configure(builder);
        }
        jerseyClient = builder.build();
        if (loginCredentials != null)
        {
            HttpAuthenticationFeature feature = HttpAuthenticationFeature.basicBuilder()
                                                                         .credentials(loginCredentials.getUsername(), loginCredentials.getPassword())
                                                                         .build();
            jerseyClient.register(feature);
        }
        for (JerseyClientTestConfigurer jerseyConfigurer : jerseyClientConfigurers)
        {
            jerseyConfigurer.configure(jerseyClient);
        }
        rootTarget = jerseyClient.target(getUri());
    }

    /**
     * @return the configured login credentials.
     */
    public LoginCredentials getLoginCredentials()
    {
        return loginCredentials;
    }

    /**
     * @return a Jersey client for accessing the server.
     */
    public JerseyClient client()
    {
        verifySetup();
        return jerseyClient;
    }

    /**
     * @return a Jersey target for accessing the server's base URL.
     */
    public JerseyWebTarget target()
    {
        verifySetup();
        return rootTarget;
    }

    /**
     * Creates a target for accessing a URL relative to the server's base URL.
     *
     * @param path the path relative to the server's base URL.
     *
     * @return a Jersey target.
     */
    public JerseyWebTarget target(String path)
    {
        return target().path(path);
    }

    @FunctionalInterface
    public static interface JerseyClientBuilderTestConfigurer
    {
        public void configure(JerseyClientBuilder builder)
        throws Exception;
    }

    @FunctionalInterface
    public static interface JerseyClientTestConfigurer
    {
        public void configure(JerseyClient client)
        throws Exception;
    }

    @FunctionalInterface
    public static interface ObjectMapperTestConfigurer
    {
        public void configure(ObjectMapper objectMapper)
        throws Exception;
    }

    public static class LoginCredentials
    {
        private final String username;
        private final String password;

        public LoginCredentials(String username, String password)
        {
            this.username = username;
            this.password = password;
        }

        public String getUsername()
        {
            return username;
        }

        public String getPassword()
        {
            return password;
        }

        @Override
        public String toString()
        {
            return getUsername();
        }
    }

    /**
     * Context resolver for configuring an object mapper on Jersey.
     */
    private static final class ObjectMapperContextResolver implements ContextResolver<ObjectMapper>
    {
        private final ObjectMapper objectMapper;

        private ObjectMapperContextResolver(ObjectMapper objectMapper)
        {
            this.objectMapper = objectMapper;
        }

        @Override
        public ObjectMapper getContext(Class<?> type)
        {
            return this.objectMapper;
        }

    }
}
