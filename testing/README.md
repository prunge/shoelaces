# Shoelaces Testing module

A library that makes it easier to perform integration testing on 
Spring Boot applications, especially those that use Jersey to 
provide REST endpoints.

A JUnit 5 extension `JerseyServerEndpointExtension` is provided that 
allows tests to configure things like the default path, host/port and 
login credentials.  Jersey  clients can then be created from this 
extension.  The basic non-Jersey `ServerEndpointExtension` can also
be used for creating basic URL-based connections.

### Usage

Add the Shoelaces Testing module as a test-scoped dependency to your
project, then register the extension in your JUnit 5 tests.

#### Jersey extension

Register the `JerseyServerEndpointExtension` in your JUnit 5 test:

```
@RegisterExtension
static JerseyServerEndpointExtension server = new JerseyServerEndpointExtension()
                                                    .useLogin("admin", "admin")
                                                    .usePath("/api");
``` 

In this example, some default login credentials and a default URI path
is set up.  You can also configure URLs, ports, etc. however usually 
you will leave the default mechanism to set these up.

Once this setup is done, use the extension's `target()` method to create
Jersey client targets for accessing your endpoints:

```
private WebTarget getEmployeeNamesTarget()
{
    return server.target("getEmployeeNames");
}
```

These targets are appended to the base URL of the server just as it 
is normally when using the Jersey client API.  

Then, in your actual test methods, use the Jersey targets like normal:

```
List<String> results = getEmployeeNamesTarget().request(MediaType.APPLICATION_JSON_TYPE)
                                               .get(new GenericType<List<String>>(){});
assertThat(results).containsExactly("John Galah", "Jenny Cockatoo");
```

Here was have an AssertJ assertion checking the results of a remote call 
to the `http://localhost:8080/api/getEmployeeNames` endpoint using
an HTTP GET.

### Using as part of a Spring Boot integration test

If not explicitly configured, when running as part of a Spring Boot 
integration test that dynamically allocates the server port, Shoelaces
will automatically pick up that port and use it.

Example:

```
/**
 * Use Spring to bring up its own webserver running on a random port and check that the server URL picks this information up automatically.
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = ReallySimpleApp.class, webEnvironment = WebEnvironment.RANDOM_PORT)
public class TestServerEndpointExtensionWithSpring
{
    @LocalServerPort
    private int serverPortFromSpring;
    
    @RegisterExtension
    static ServerEndpointExtension server = new ServerEndpointExtension().usePath("/myServer");
    
    @Test
    public void doSomeTesting()
    throws MalformedURLException
    {
        URI serverUri = server.getUri(); //Port automatically configured here
        // ... and use that server URI for connecting to the server
    }
}
```

### Customizing the Jersey client

When configuring `JerseyServerEndpointExtension`, use the methods 
`useClientConfigurer()`, `useClientBuilderConfigurer()` and 
`useObjectMapperConfigurer()`:

```
class TestJersey 
{
    @RegisterExtension
    static JerseyServerEndpointExtension server = new JerseyServerEndpointExtension().usePath("/test")
                                                        .useClientConfigurer(TestJersey::configureClient)
                                                        .useObjectMapperConfigurer(TestJersey::configureObjectMapper);
                                                        
    private static void configureObjectMapper(ObjectMapper om)
    {
        om.registerModule(new JavaTimeModule());
        om.disable(DeserializationFeature.READ_DATE_TIMESTAMPS_AS_NANOSECONDS);
    }
    
    private static void configureClient(JerseyClient client)
    {
        client.register(HttpAuthenticationFeature.basicBuilder()
                                                 .credentials("admin", "admin")
                                                 .build());
    }
    
    @Test
    void testSomething()
    {
        String response = server.target("simple")
                                .request(MediaType.APPLICATION_JSON_TYPE)
                                .get(String.class);
        assertThat(response).isEqualTo("expected response");
    }
}
```

This is an example of customizing the JSON format for timestamps and 
adding default login credentials for the Jersey client when used
from your tests.

This is a bit of a long-winded way of configuring login credentials 
though for the sake of demonstrating the configurer - there is 
a simpler API you can use for that.

### Specifying login credentials for Jersey clients

When configuring the extension, use the `useLogin()` method:

```
@RegisterExtension
static JerseyServerEndpointExtension server = new JerseyServerEndpointExtension().usePath("/test")
                                                    .useLogin("admin", "pass");                   
```

### Making tests wait for the server to start up

Sometimes a server might not have fully started up when integration
tests start executing, such as when spawning Spring Boot applications
using `java:exec` from Maven.  The Shoelaces testing extension has the
ability to wait for a server to be responding (with a configurable 
timeout) before actually executing the tests.

This can be configured programmatically using:

```
@RegisterExtension
static JerseyServerEndpointExtension server = new JerseyServerEndpointExtension().usePath("/test")
                                                    .useServerStartupWait(60, TimeUnit.SECONDS);                   
```

or can be set using the system property `test.server.wait` to a 
string duration value:

```
<plugin>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-failsafe-plugin</artifactId>
    <configuration>
        <systemPropertyVariables>
            <test.server.wait>PT60S</test.server.wait>
        </systemPropertyVariables>
    </configuration>
</plugin>
```

### Configuration using system properties

Values configured by system properties override those configured using
the Shoelaces extension API.

The following system properties are used by the Shoelaces testing
module and can be used to configure various settings instead of the 
extension API:

#### `test.server.wait` 

The maximum amount of time to wait for a server to respond to HTTP
requests before failing the tests.  Use this if, when the tests 
start executing, the server might not yet be fully started up.

Values are formatted duration values, e.g. `PT60S` for 60 seconds.

Equivalent in the extension API: `useServerStartupWait()`

#### `test.server.host` and `test.server.port`

Configure the host and port the server is running on.  This is useful
to configure when Maven integration tests spin up a server on a random
port (such as with Build Helper's 
[reserve network port](https://www.mojohaus.org/build-helper-maven-plugin/reserve-network-port-mojo.html) 
goal), or inside a Docker container.
