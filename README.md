Shoelaces is a collection of modules and libraries that can be used with 
Spring Boot.

### Modules

| Module                                      | Description                                                                                                                                                                                  |
|---------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [AppHome](apphome/README.md)                | Sets up a directory for your application at runtime on the file system that can be used by the application for persistent storage and by users to customize configuration of the application |
| [Assorted](assorted/README.md)              | Various beans and a request timing logging filter                                                                                                                                            |
| [JDBC](jdbc/README.md)                      | Configures a default persistent disk-based embedded database for your application                                                                                                            |
| [Jersey](jersey/README.md)                  | Allows easy configuration of Jersey and its Jackson-based JSON serialization                                                                                                                 |
| [Jersey Client](jersey-client/README.md)    | Provides bean that can be used to acquire pre-configured Jersey client for accessing web services                                                                                            |
| [Logging Configurator](logconfig/README.md) | Allows Logback logging to be custom-configured by a configuration file under the application home directory by users                                                                         |
| [Selenium](selenium/README.md)              | Library that makes it easier to test your projects with Selenium.                                                                                                                            | 
| [Testing](testing/README.md)                | Library that makes it easier to perform integration testing on Spring Boot applications                                                                                                      |

### Using the Bill of Materials artifact to configure versions

Using the BOM artifact helps by configuring versions of all Shoelaces
artifacts in your project in one place.

To use it in Maven projects, configure the BOM artifact in the
`<dependencyManagement>` section of your project POM:

```
<dependencyManagement>
    <dependencies>
        <dependency>
            <groupId>au.net.causal.shoelaces</groupId>
            <artifactId>shoelaces-bom</artifactId>
            <version>${shoelaces.version}</version>
            <type>pom</type>
            <scope>import</scope>
        </dependency>
    </dependencies>
</dependencyManagement>
``` 

where `${shoelaces.version}` is the version of Shoelaces you want to
use.

When this is used, throughout your POM you don't need to explicitly 
configure the versions of all the Shoelaces modules you want to use.
 