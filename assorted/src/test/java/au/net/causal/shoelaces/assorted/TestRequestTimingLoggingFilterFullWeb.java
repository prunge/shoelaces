package au.net.causal.shoelaces.assorted;

import au.net.causal.shoelaces.assorted.TestRequestTimingLoggingFilterFullWeb.ReallySimpleApp;
import jakarta.servlet.http.HttpServletRequest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.time.Clock;
import java.time.Instant;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

/**
 * A kind of integration test for the request timing logging filter that uses a Spring mock web environment.
 */
@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = ReallySimpleApp.class, webEnvironment = WebEnvironment.MOCK)
@AutoConfigureMockMvc
class TestRequestTimingLoggingFilterFullWeb
{
    @Mock
    private static Logger mockLogger;

    @MockBean
    private Clock mockClock;

    @Captor
    private static ArgumentCaptor<String> logMessageCaptor;

    @Autowired
    private MockMvc mockMvc;

    /**
     * Verify the filter is invoked and has the right message.
     */
    @Test
    void testFilter() throws Exception
    {
        when(mockLogger.isDebugEnabled()).thenReturn(true);
        when(mockClock.instant()).thenReturn(Instant.ofEpochMilli(100L), Instant.ofEpochMilli(400L)); //Request will take 300 ms
        mockMvc.perform(get("/anything/else")); //Will fail with a 404, but the filter will still be invoked and that's all we care about
        verify(mockLogger).debug(logMessageCaptor.capture());
        assertThat(logMessageCaptor.getValue()).isEqualTo("http://localhost/anything/else: 0.3s");
    }

    /**
     * A nothing Spring app just to allow SpringExtension to bring a webserver up.
     */
    @SpringBootApplication
    public static class ReallySimpleApp
    {
        @Bean
        public RequestTimingLoggingFilter requestTimingLoggingFilter(Clock clock)
        {
            return new RequestTimingLoggingFilterForTesting(clock);
        }
    }

    private static class RequestTimingLoggingFilterForTesting extends RequestTimingLoggingFilter
    {
        RequestTimingLoggingFilterForTesting(Clock clock)
        {
            super(clock);
        }

        @Override
        Logger loggerForRequest(HttpServletRequest request)
        {
            return mockLogger;
        }
    }
}
