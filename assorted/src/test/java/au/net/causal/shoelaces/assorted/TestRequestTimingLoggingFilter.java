package au.net.causal.shoelaces.assorted;

import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockHttpServletRequest;

import static org.assertj.core.api.Assertions.assertThat;

class TestRequestTimingLoggingFilter
{
    @Test
    void generateLoggerNameNoServletPathOrExtraPath()
    {
        String baseLoggerName = "base.name";
        RequestTimingLoggingFilter filter = new RequestTimingLoggingFilter(baseLoggerName);
        MockHttpServletRequest request = new MockHttpServletRequest();
        String loggerName = filter.generateLoggerName(request);

        assertThat(loggerName).isEqualTo("base.name");
    }

    @Test
    void generateLoggerNameServletPath()
    {
        String baseLoggerName = "base.name";
        RequestTimingLoggingFilter filter = new RequestTimingLoggingFilter(baseLoggerName);
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setServletPath("/myservlet");
        String loggerName = filter.generateLoggerName(request);

        assertThat(loggerName).isEqualTo("base.name.myservlet");
    }

    @Test
    void generateLoggerNameServletPathAndInfo()
    {
        String baseLoggerName = "base.name";
        RequestTimingLoggingFilter filter = new RequestTimingLoggingFilter(baseLoggerName);
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setServletPath("/myservlet");
        request.setPathInfo("/galah/cockatoo");
        String loggerName = filter.generateLoggerName(request);

        assertThat(loggerName).isEqualTo("base.name.myservlet.galah.cockatoo");
    }

    @Test
    void generateLoggerNameInfoOnly()
    {
        String baseLoggerName = "base.name";
        RequestTimingLoggingFilter filter = new RequestTimingLoggingFilter(baseLoggerName);
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setPathInfo("/galah/cockatoo");
        String loggerName = filter.generateLoggerName(request);

        assertThat(loggerName).isEqualTo("base.name.galah.cockatoo");
    }

    @Test
    void generateLoggerNameEndWithSlash()
    {
        String baseLoggerName = "base.name";
        RequestTimingLoggingFilter filter = new RequestTimingLoggingFilter(baseLoggerName);
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setServletPath("/myservlet/");
        request.setPathInfo("/galah/cockatoo/");
        String loggerName = filter.generateLoggerName(request);

        assertThat(loggerName).isEqualTo("base.name.myservlet.galah.cockatoo");
    }
}
