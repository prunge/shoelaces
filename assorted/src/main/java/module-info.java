open module au.net.causal.shoelaces.assorted
{
    exports au.net.causal.shoelaces.assorted;

    requires spring.core;
    requires spring.beans;
    requires spring.boot;
    requires spring.context;
    requires spring.web;
    requires spring.boot.autoconfigure;

    requires org.slf4j;

    requires static jakarta.servlet;
}
