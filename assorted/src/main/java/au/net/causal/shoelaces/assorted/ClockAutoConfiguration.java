package au.net.causal.shoelaces.assorted;

import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;

import java.time.Clock;

/**
 * Makes a clock bean available for injection.  The clock will use the {@linkplain Clock#systemDefaultZone() system's default time zone}.
 *
 * @see Clock
 */
@AutoConfiguration
@ConditionalOnMissingBean(Clock.class)
public class ClockAutoConfiguration
{
    @Bean
    public Clock clock()
    {
        return Clock.systemDefaultZone();
    }
}
