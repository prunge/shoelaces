package au.net.causal.shoelaces.assorted;

import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Bean;

import java.time.Clock;

/**
 * Configures a servlet request filter that logs request / response timing information if
 * its application property is configured.
 *
 * @see RequestTimingLoggingFilter
 */
@AutoConfiguration
@ConditionalOnWebApplication
public class RequestTimingLoggingFilterAutoConfiguration
{
    @ConditionalOnProperty(prefix = ShoelacesAssortedProperties.PREFIX + ".request-timing", name = "enabled", havingValue = "true")
    @ConditionalOnMissingBean(RequestTimingLoggingFilter.class)
    @Bean
    public RequestTimingLoggingFilter requestTimingLoggingFilter(Clock clock)
    {
        return new RequestTimingLoggingFilter(clock);
    }
}
