package au.net.causal.shoelaces.assorted;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.util.Objects;

/**
 * A servlet filter that logs request / response timing information.
 * <p>
 *
 * Logger name is chosen based on the path of the request:
 * <ul>
 *     <li>A chosen prefix, which defaults to {@value DEFAULT_BASE_LOGGER_NAME}</li>
 *     <li>The path of the request is modified by replacing '/' with '.'</li>
 * </ul>
 *
 * For example, for a request to <code>/rest/cats/add</code> the logger name will be
 * <code>shoelaces.request.timing.rest.cats.add</code>.  All timing information is logged at
 * <code>DEBUG</code> log level - configuring your logging system appropriately will allow certain requests,
 * based on path, to be logged and others not.  Additionally, the start of each request is logged at the <code>TRACE</code>
 * level.
 */
public class RequestTimingLoggingFilter extends OncePerRequestFilter
{
    private static final String DEFAULT_BASE_LOGGER_NAME = "shoelaces.request.timing";

    private final Clock clock;
    private final String baseLoggerName;

    /**
     * Constructs a <code>RequestTimingLoggingFilter</code> with a custom clock and base logger name.
     *
     * @param clock clock for timing.
     * @param baseLoggerName base logger name used for the prefix for logger names.
     */
    public RequestTimingLoggingFilter(Clock clock, String baseLoggerName)
    {
        this.clock = Objects.requireNonNull(clock);
        this.baseLoggerName = Objects.requireNonNull(baseLoggerName);
    }

    /**
     * Constructs a <code>RequestTimingLoggingFilter</code> with a custom clock and the default base logger name.
     *
     * @param clock clock for timing.
     */
    public RequestTimingLoggingFilter(Clock clock)
    {
        this(clock, DEFAULT_BASE_LOGGER_NAME);
    }

    /**
     * Constructs a <code>RequestTimingLoggingFilter</code> with the system default clock and a custom base logger name.
     *
     * @param baseLoggerName base logger name used for the prefix for logger names.
     */
    public RequestTimingLoggingFilter(String baseLoggerName)
    {
        this(Clock.systemDefaultZone(), baseLoggerName);
    }

    /**
     * Constructs a <code>RequestTimingLoggingFilter</code> with the system default clock and base logger name.
     */
    public RequestTimingLoggingFilter()
    {
        this(Clock.systemDefaultZone());
    }

    /**
     * Logger name uses the path, replacing '/' with '.' and prepending the base logger name.
     * e.g. /rest/cats/add -&gt; shoelaces.request.timing.rest.cats.add
     */
    protected String generateLoggerName(HttpServletRequest request)
    {
        StringBuilder buf = new StringBuilder();
        buf.append(baseLoggerName);

        String servletPath = request.getServletPath().replace('/', '.');
        while (servletPath.endsWith("."))
        {
            servletPath = servletPath.substring(0, servletPath.length() - 1);
        }
        buf.append(servletPath);

        String pathInfo = request.getPathInfo();
        if (pathInfo != null && !pathInfo.isEmpty())
        {
            pathInfo = pathInfo.replace('/', '.');
            if (!pathInfo.startsWith("."))
                pathInfo = "." + pathInfo;
            while (pathInfo.endsWith("."))
            {
                pathInfo = pathInfo.substring(0, pathInfo.length() - 1);
            }
            buf.append(pathInfo);
        }

        return buf.toString();
    }

    /**
     * Creates a logger to use for timing information given an HTTP request.
     *
     * @param request the request.
     *
     * @return the logger to use for the request.
     */
    Logger loggerForRequest(HttpServletRequest request)
    {
        return LoggerFactory.getLogger(generateLoggerName(request));
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
    throws ServletException, IOException
    {
        Logger log = loggerForRequest(request);

        if (!log.isDebugEnabled())
        {
            filterChain.doFilter(request, response);
            return;
        }

        log.trace("Start request: " + request.getRequestURL());

        Instant startTime = clock.instant();
        filterChain.doFilter(request, response);
        Instant endTime = clock.instant();
        Duration timeTaken = Duration.between(startTime, endTime);

        log.debug(request.getRequestURL() + ": " + timeTaken.toNanos() / 1.0E9 + "s");
    }
}
