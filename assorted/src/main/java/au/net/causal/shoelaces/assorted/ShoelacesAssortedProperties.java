package au.net.causal.shoelaces.assorted;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix=ShoelacesAssortedProperties.PREFIX)
public class ShoelacesAssortedProperties
{
    public static final String PREFIX = "shoelaces.assorted";
    
    private final RequestTiming requestTiming = new RequestTiming();

    public RequestTiming getRequestTiming()
    {
        return requestTiming;
    }

    public static class RequestTiming
    {
        /**
         * When request timing is enabled, every HTTP request will be timed and the amount of time it takes will be logged.  The logger name will be 
         * based on the request path.
         */
        private boolean enabled;

        public boolean isEnabled()
        {
            return enabled;
        }

        public void setEnabled(boolean enabled)
        {
            this.enabled = enabled;
        }
    }
}
