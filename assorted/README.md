# Shoelaces Assorted module

Contains assorted beans and utilities for use with Spring Boot
applications.

### Clock bean

When the assorted module is present, a `java.time.Clock` bean configured
to use the system clock in the system default timezone is made available
the the rest of the application.

Example use:

```
@Autowired
private Clock clock;
```

```
LocalDateTime now = LocalDateTime.now(clock);
```

### Request timing logging filter

When activated, the filter logs timing information for each
request/response to a logger with a name derived from the 
request path.

#### Logger naming

Loggers are named based on the request path:

- the default prefix is `shoelaces.request.timing` (can be reconfigured
by creating your own `RequestTimingLoggingFilter` bean instead of 
using the default one)
- the request path, minus the application context path (which is 
typically only used when running as a WAR in a container), is translated
by replacing all '/' characters with '.'s to make the remainder of the 
logger name

For example, a request to `/myservice/records?name=John` will log 
request timing information to a logger named 
`shoelaces.request.timing.myservice.records`. 

#### Usage

The request timing logging filter is not turned on by default.  To turn
on, set the `shoelaces.assorted.request-timing.enabled` application 
property to `true`.  An easy way to do this is to add this property
to the `application.properties` file (create it if you don't already 
have one):

src/main/resources/application.properties:
```
shoelaces.assorted.request-timing.enabled=true
```

Once turned on, configure your logging system appropriately.
For example, if using Logback:

src/main/resources/logback.xml
```
...
<logger name="shoelaces.request.timing" level="DEBUG" />
...
```
This will log timing information for all requests to the default appender.

To log only particular requests, such as to `/myservice/records`, 
the logger can instead be configured with:

```
...
<logger name="shoelaces.request.timing.myservice.records" level="DEBUG" />
...

```

Typical hierarchy of loggers apply here, so configuring the logger named
`shoelaces.request.timing.myservice` will cover the records endpoint
as well.  Your logging system will be able to more fancy things with these loggers,
such as logging certain requests to different files, etc.