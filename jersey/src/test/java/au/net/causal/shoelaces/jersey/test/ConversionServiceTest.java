package au.net.causal.shoelaces.jersey.test;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.ResolvableType;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.format.FormatterRegistry;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.datetime.standard.DateTimeFormatterRegistrar;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import java.lang.annotation.Annotation;
import java.time.LocalDate;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = ConversionServiceTest.ReallySimpleApp.class, webEnvironment = SpringBootTest.WebEnvironment.MOCK)
class ConversionServiceTest
{
    @Test
    void test(@Autowired ConversionService conversionService)
    throws Exception
    {
        System.out.println(conversionService);

        System.out.println("Can convert: " + conversionService.canConvert(String.class, LocalDate.class));


        System.out.println("Converting: " + conversionService.convert("1977-01-09", LocalDate.class));

        Annotation[] annotations = ConversionServiceTest.class.getDeclaredField("formatHolder").getDeclaredAnnotations();
        LocalDate result = (LocalDate)conversionService.convert("1977-01-09", TypeDescriptor.valueOf(String.class), new TypeDescriptor(ResolvableType.forClass(LocalDate.class), LocalDate.class, annotations));
        System.out.println("Conversion result for annotation: " + result);
    }

    //Just for having an annotation
    @DateTimeFormat(pattern = "yyyy-dd-MM")
    private static final Void formatHolder = null;

    /**
     * A nothing Spring app just to allow SpringExtension to bring a webserver up.
     */
    @SpringBootApplication
    public static class ReallySimpleApp
    {
        @Configuration
        public static class WebConfig extends WebMvcConfigurationSupport
        {
            @Override
            public void addFormatters(FormatterRegistry registry)
            {
                DateTimeFormatterRegistrar r = new DateTimeFormatterRegistrar();
                r.setUseIsoFormat(true);
                r.registerFormatters(registry);
            }
        }
    }
}
