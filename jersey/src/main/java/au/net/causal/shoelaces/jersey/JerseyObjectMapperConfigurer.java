package au.net.causal.shoelaces.jersey;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Hook for customizing the Jackson object mapper used by Jersey.
 * <p>
 *
 * Beans that implement this interface will be picked up and their <code>configure()</code> methods will be run
 * when Jersey is initialized.  This allows object mapper features to be easily configured from Spring Boot
 * applications.
 * <p>
 *
 * Configurers only affect <i>Jersey's</i> object mapper, which is actually copied from the 'standard' object mapper in
 * Spring Boot.  This allows the standard object mapper to be used for other purposes while Jersey's one can be
 * customized specifically.
 */
@FunctionalInterface
public interface JerseyObjectMapperConfigurer
{
    /**
     * Customize an object mapper that is used for Jersey.
     *
     * @param objectMapper the object mapper to configure.
     */
    public void configure(ObjectMapper objectMapper);
}
