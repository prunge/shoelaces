package au.net.causal.shoelaces.jersey;

import au.net.causal.shoelaces.jersey.common.AbstractJacksonDateTimeParamConverterProvider;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.Set;

public class JerseyJacksonDateTimeParamConverterProvider extends AbstractJacksonDateTimeParamConverterProvider
{
    @JerseyConfigured @Autowired
    private ObjectMapper objectMapper;

    protected JerseyJacksonDateTimeParamConverterProvider(Set<Class<?>> handledTypes)
    {
        super(handledTypes);
    }

    public JerseyJacksonDateTimeParamConverterProvider()
    {
        super();
    }

    @Override
    protected ObjectMapper objectMapper(Class<?> rawType, Type genericType, Annotation[] annotations)
    {
        return objectMapper;
    }
}
