open module au.net.causal.shoelaces.jersey
{
    exports au.net.causal.shoelaces.jersey;

    requires spring.core;
    requires spring.beans;
    requires spring.boot;
    requires spring.context;
    requires spring.boot.autoconfigure;
    requires jakarta.ws.rs;
    requires com.fasterxml.jackson.databind;
    requires com.fasterxml.jackson.module.jakarta.xmlbind;

    //These dependencies have autogenerated module names
    requires jersey.media.json.jackson;
    requires jersey.server;
    requires jersey.common;
}
