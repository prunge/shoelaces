# Shoelaces Jersey module

Allows applications using Jersey on the server-side to be customized
more easily, such as configuring Jackson object mappers and 
Jackson's JSON serialization.

### Customizing JSON serialization and deserialization

To customize Jersey's object mapper, add the Shoelaces Jersey module as 
a dependency to your application, and then register one or more beans of 
type `JerseyObjectMapperConfigurer`:

```
@Bean
public JerseyObjectMapperConfigurer customizeJerseyObjectMapper()
{
    return om -> om.enable(SerializationFeature.WRITE_ENUMS_USING_INDEX);
}
```

The configurer is a functional interface that accepts a Jackson
object mapper as a parameter.  Customize this by calling methods, 
enabling features, etc.

### Jersey's object mapper

The Shoelaces Jersey module creates and makes Jersey use a 
Jersey-specific custom object mapper.  This differs from the default
Spring Boot behaviour, where Jersey uses the global object mapper 
bean.

The advantage of this approach is that multiple object mappers can be 
used for different parts of the system and customized individually.
